<?php
namespace Com\Hunchfree\Plugins\CommonUtils;

defined('\\ABSPATH') or die('No script kiddies please!');

class Common_Admin_Utils extends Common_Utils {

	protected $_role_names = null;
	protected $_testable_post_types = null;
	protected $_saved_meta_boxes = false;
	protected $_saved_sitemap_meta_boxes = false;

	public function __construct( $plugin_file_path ) {
		parent::__construct( $plugin_file_path );
	}

	public function init() {
		$this->do_common_init();

		# add_action('admin_init', array($this, 'setup_options_panels'));
		add_action('admin_menu', array( $this, 'setup_admin_panel') );
		add_action('admin_init', array( $this, 'maybe_register_settings' ) );

		add_filter("plugin_action_links_{$this->_base_name}", array($this, 'add_action_links') );

		$bh_on_admin = $this->get_option('bugherd_use_on_admin', false);
		if ( $bh_on_admin ) {
			add_action('admin_footer', array($this, 'add_bug_herd') );
		}
		$an_on_admin = $this->get_option('analytics_use_on_admin', self::ANALYTICS_NO);
		if ( false === $an_on_admin ) {
			$an_on_admin = self::ANALYTICS_NO;
		} else if ( true === $an_on_admin ) {
			$an_on_admin = self::ANALYTICS_NORMAL;
		}
		switch ( "{$an_on_admin}" ) {
			case '1':
				add_action('admin_footer', array($this, 'add_analytics') );
				break;
			case '2':
				add_action('admin_footer', array($this, 'add_analytics_tag_manager') );
				break;
			default:
				break;
		}
		#if ( $an_on_admin ) {
		#	add_action('admin_footer', array($this, 'add_analytics') );
		#}

		$split_test_on = $this->get_option('split_test_use', false);
		if ( $split_test_on ) {
			$defaults = $this->get_testable_post_type_values();
			$allowed_types = $this->get_option('split_test_post_types', $defaults);
			foreach ( $allowed_types as $post_type_name ) {
				add_action('add_meta_boxes_' . $post_type_name, array( $this, 'setup_meta_boxes' ) );
				add_action('manage_edit-' . $post_type_name . '_columns', array( $this, 'add_split_test_to_columns') );
				add_action('manage_' . $post_type_name . '_posts_custom_column', array( $this, 'add_split_test_column_data') );
				# add_filter('manage_edit-' . $post_type_name . '_sortable_columns', array( $this, 'alter_split_test_sortable_columns') );
			}
			add_filter('posts_clauses', array($this, 'maybe_modify_admin_queries'), 10, 2);
			add_action('save_post', array( $this, 'save_meta_boxes' ), 10, 2);
			# add_filter('posts_orderby', array( $this, 'alter_split_test_order_by') );
		}

		$sitemap_on = $this->get_option('sitemap_use', false);
		if ( $sitemap_on ) {

			# Deal with Post Types
			$allowed_types = $this->get_site_mapped_post_types();
			foreach ( $allowed_types as $post_type_name ) {
				add_action('add_meta_boxes_' . $post_type_name, array( $this, 'setup_meta_sitemap_boxes' ) );
			}
			add_action('save_post', array( $this, 'save_sitemap_meta_boxes' ), 10, 2);

			# TODO - Deal with taxonomy terms
		}
	}

	protected function get_site_mapped_post_types() {
		$types = array();
		$choices = $this->get_option('sitemap_display_settings', array() );
		if ( is_array($choices) && 0 < count($choices) ) {
			foreach ( $choices as $choice ) {
				if ( array_key_exists('type', $choice) && 'post' == "{$choice['type']}" ) {
					if ( array_key_exists('name', $choice) ) {
						$types[] = $choice['name'];
					}
				}
			}
		}
		return $types;
	}

	public function setup_admin_panel() {
		try {
			if ( current_user_can('activate_plugins') ) {

				# Add a utilities section to the Settings menu
				$settings_page = add_options_page(
					__( 'HunchFree Utilities Dashboard', $this->_prefix . 'language'),
					__( 'HF Utilities', $this->_prefix . 'language' ),
					'manage_options',
					$this->_prefix . 'settings',
					array( $this, 'draw_settings_panel' )
				);

				if ( $settings_page ) {
					# add_action("load-{$options_page}", array( $this, 'do_enqueue_admin_js'));
					# add_action("admin_print_styles-{$options_page}", array( $this, 'do_enqueue_admin_css'));
				} else {
					throw new Common_Utils_Exception( "Failed to setup dashboard menu page", 10001 );
				}
			}
		} catch ( \Exception $e ) {
			$this->handle_exception( $e );
		}
	}

	public function draw_settings_panel() {
		$screen_icon = get_screen_icon();
		$meta = $this->get_plugin_info();

		$plugin_name = $meta['Name'];
		$plugin_version = $meta['Version'];

		echo <<<HTML
<div class="wrap">
	{$screen_icon}
	<h2>{$plugin_name} v{$plugin_version} : Settings</h2>

HTML;

		try {
			settings_errors();
			echo <<<HTML
	<form action="options.php" method="post" enctype="multipart/form-data">
		<input type="hidden" name="wanted_page" value="{$this->_prefix}settings" />

HTML;

			settings_fields( "{$this->_prefix}settings_tab" );
			do_settings_sections( "{$this->_prefix}settings" );

			submit_button();

			echo <<<HTML
	</form>

HTML;

		} catch ( \Exception $e ) {
			$this->handle_exception( $e );
		}

		echo <<<HTML
</div>
HTML;

	}

	public function maybe_register_settings() {
		try {
			$wanted_page = ( isset($_REQUEST['page']) && !empty($_REQUEST['page']) )? stripcslashes($_REQUEST['page']) : '';
			if ( empty( $wanted_page ) ) {
				# This is used because we need to register our settings for the options processor
				$wanted_page = ( isset($_REQUEST['wanted_page']) && !empty($_REQUEST['wanted_page']) )? stripcslashes( $_REQUEST['wanted_page'] ) : '';
			}
			if ( $this->_prefix . 'settings' == "{$wanted_page}" ) {
				$this->do_register_settings();
			}
		} catch ( \Exception $e ) {
			$this->handle_exception( $e );
		}
	}

	public function add_split_test_to_columns( $columns ) {
		$revised = array();
		foreach ( $columns as $id => $data ) {
			if ( 'date' == "{$id}" ) {
				$revised['split_test'] = __('Split Testing');
				$revised["{$id}"] = $data;
			} else {
				$revised["{$id}"] = $data;
			}
		}
		return $revised;
	}

	public function add_split_test_column_data( $column, $post_id = 0 ) {
		#global $wp_query;
		#echo "<pre>" . print_r($wp_query, true) . "</pre>";
		#exit;
		global $post;

		if ( $post_id ) {
			# Nothing to do with this - TODO - shorten to just receive 1 value
		}
		switch ( $column ) {
			case 'split_test':
				$test_type = ( isset( $post->split_test_type) )? (int)$post->split_test_type : 3;
				# $test_type = ( isset( $post->split_test_type) && !empty( $post->split_test_type ) )? (int)$post->split_test_type : 3;
				switch ( $test_type ) {
					case 0:
						# Control
						echo '<em>Control</em>';
						break;
					case 1:
						# Test
						$control_id = ( isset($post->control_id) && !empty($post->control_id) ) ? (int)$post->control_id : 0;
						$control_title = ( isset($post->control_title) && !empty($post->control_title) )? $post->control_title : 'No Title';
						$control_view_link = get_permalink( $control_id );
						echo '<em>Test</em> (<a href="' . $control_view_link . '" title="' . $control_title . '" target="_blank">View Control</a>)';
						if ( current_user_can('edit_posts') ) {
							$control_edit_link = get_edit_post_link( $control_id );
							echo ' (<a href="' . $control_edit_link . '">Edit Control</a>)';
						}
						break;
					default:
						echo '<span title="' . $test_type . '">-</span>';
						# Something else
						break;
				}
				break;
			default:
				break;
		}
	}

	public function alter_split_test_sortable_columns( $columns ) {
		$columns['split_test'] = 'split_test';
		return $columns;
	}

	public function alter_split_test_order_by( $order_by ) {
		/** @var \WP_Query $wp_query */
		global $wp_query;
		if ( $wp_query->is_main_query() && $wp_query->is_admin && is_array( $wp_query->query ) && array_key_exists( 'post_type', $wp_query->query ) ) {
			$defaults = $this->get_testable_post_type_values();
			$filtered = $this->get_option('split_test_post_types', $defaults);
			if ( in_array( $wp_query->query['post_type'], $filtered ) ) {
				if ( array_key_exists( 'orderby', $_REQUEST ) && 'split_test' == "{$_REQUEST['orderby']}" ) {
					$asc_desc = ( array_key_exists( 'order', $_REQUEST ) ) ? $_REQUEST['order'] : 'asc';
					$asc_desc = strtoupper( $asc_desc );
					$order_by = "split_test_type {$asc_desc}, title ASC";
				}
			}
		}
		return $order_by;
	}

	public function maybe_modify_admin_queries( $pieces, \WP_Query $wp_query ) {
		$prefix = $this->_prefix;
		if ( $wp_query->is_admin ) {
			if ( $wp_query->is_main_query() ) {
				if ( is_array($wp_query->query) && array_key_exists('post_type', $wp_query->query) ) {
					$defaults = $this->get_testable_post_type_values();
					$allowed_post_types = $this->get_option('split_test_post_types', $defaults);
					if ( in_array($wp_query->query['post_type'], $allowed_post_types) ) {
						global $wpdb;
						$control_key = "_{$prefix}control_id";
						$code_key = "_{$prefix}experiment_code";

						$pieces['join'] .= "LEFT JOIN {$wpdb->prefix}postmeta as hf_con ON  {$wpdb->prefix}posts.ID = hf_con.post_id AND hf_con.meta_key = '{$control_key}' ";
						$pieces['join'] .= "LEFT JOIN {$wpdb->prefix}posts as hf_con_p ON hf_con_p.ID = hf_con.meta_value ";
						$pieces['join'] .= "LEFT JOIN {$wpdb->prefix}postmeta as hf_code ON {$wpdb->prefix}posts.ID = hf_code.post_id AND hf_code.meta_key = '{$code_key}' ";

						$pieces['fields'] .= ", IFNULL(hf_con.meta_value, 0) as control_id, IFNULL(hf_con_p.post_title, '') as control_title ";
						$pieces['fields'] .= ", ( CASE WHEN hf_con.meta_value > 0 THEN 1 WHEN hf_code.meta_value IS NOT NULL THEN 0 ELSE 2 END ) AS split_test_type ";

						#$pieces['fields'] .= ", IF(hf_code.meta_value IS NULL, 0, 1) AS has_experiment ";
						#$pieces['fields'] .= ", IFNULL(hf_code.meta_value, '') as experiment_code ";

						# TODO - maybe use CASE to get a sortable column
					}
				}
			}
		}
		return $pieces;
	}

	public function setup_meta_sitemap_boxes( \WP_Post $post ) {
		add_meta_box(
			$this->_prefix . 'sitemap_box',
			__('Sitemap Settings', $this->_prefix . 'language'),
			array( $this, 'draw_sitemap_meta_box' ),
			$post->post_type,
			'advanced',
			'default'
		);
	}

	public function setup_meta_boxes( \WP_Post $post ) {
		add_meta_box(
			$this->_prefix . 'split_test_box',
			__('Split Test Settings', $this->_prefix . 'language'),
			array( $this, 'draw_split_test_meta_box' ),
			$post->post_type,
			'advanced',
			'default'
		);
	}

	protected function get_available_controls( $post_type, $post_id ) {
		$available = array();

		$args = array(
			'post_type' => "{$post_type}",
			'order' => 'ASC',
			'orderby' => 'title',
			'post_status' => array( 'publish' ),
			'posts_per_page' => -1
		);
		if ( !empty( $post_id ) ) {
			$args['post__not_in'] = array( (int)$post_id );
		}
		$found = new \WP_Query( $args );
		if ( $found->have_posts() ) {
			while ( $found->have_posts() ) {
				$item = $found->next_post();
				$item_id = $item->ID;
				$available["{$item_id}"] = $item;
			}
		}
		return $available;
	}

	public function draw_sitemap_meta_box( \WP_Post $post, $args ) {
		$prefix = $this->_prefix;
		if ( $args ) {
			# not currently used
		}

		wp_nonce_field('updating_sitemap_settings', $prefix . 'sitemap_nonce');

		$hidden_choice = (int)get_post_meta( $post->ID, "_{$prefix}sitemap_hide", true);

		$choice_yes = ( 1 === $hidden_choice )? ' checked="checked"' : '';
		$choice_no = ( empty($choice_yes) )? ' checked="checked"' : '';

		echo <<<HTML
<p>
	<b>Hide on site map?</b>
	<br /><label for="{$prefix}sitemap_hide_n"><input type="radio" name="{$prefix}sitemap_hide" id="{$prefix}sitemap_hide_n" value="0" {$choice_no} /> No</label>
	<br /><label for="{$prefix}sitemap_hide_y"><input type="radio" name="{$prefix}sitemap_hide" id="{$prefix}sitemap_hide_y" value="1" {$choice_yes} /> Yes</label>
</p>
HTML;

	}

	public function draw_split_test_meta_box( \WP_Post $post, $args ) {
		$prefix = $this->_prefix;
		if ( $args ) {
			# Not currently used
		}

		wp_nonce_field('updating_split_test_settings', $prefix . 'split_test_nonce');

		$control_id = get_post_meta( $post->ID, "_{$prefix}control_id", true );
		if( empty( $control_id ) ) {
			$control_id = '';
			$control_blurb = '';
		} else {
			$c = get_post( $control_id );
			$control_link = get_permalink( $control_id );
			$control_edit_link = get_edit_post_link( $control_id );
			$control_blurb = '<br /><b>Current Control:</b> ' . $c->post_title . ' (<a href="' . $control_link . '" target="_blank">view</a> | <a href="' . $control_edit_link . '">edit</a>';
		}

		$controls = $this->get_available_controls( $post->post_type, $post->ID );
		$selected = ( empty($control_id) )? ' selected="selected"' : '';
		$control_options = <<<HTML
<option value="" {$selected}> -- None -- </option>

HTML;

		if ( 0 < count($controls) ) {
			foreach ( $controls as $controller ) {
				$selected = ( "{$controller->ID}" == "{$control_id}" )? ' selected="selected"' : '';
				$control_options .= <<<HTML
<option value="{$controller->ID}" {$selected}>{$controller->post_title}</option>

HTML;

			}
		}

		$code = get_post_meta( $post->ID, "_{$prefix}experiment_code", true );
		$test_links = '';
		if ( !empty( $code ) ) {
			$code = htmlentities( $code );
			$test_items = array();
			$test_args = array(
				'post_type' => $post->post_type,
				'post_status' => 'any',
				'posts_per_page' => -1,
				'meta_key' => "_{$prefix}control_id",
				'meta_value_num' => $post->ID,
				'order' => 'ASC',
				'orderby' => 'title'
			);
			$tests = new \WP_Query( $test_args );
			if( $tests->have_posts() ) {
				while ( $tests->have_posts() ) {
					$test = $tests->next_post();
					$test_link = get_permalink( $test );
					$test_edit_link = get_edit_post_link( $test->ID );
					$test_items[] = $test->post_title . ' (<a href="' . $test_link . '" target="_blank">view</a> | <a href="' . $test_edit_link . '">edit</a>)';
				}
			}
			if ( 0 < count($test_items) ) {
				$test_links = '<p><b>Current Tests:</b></p><ul style="list-style-type:disc;padding-left:1em;"><li>' . implode('</li><li>', $test_items) . '</li></ul>';
			}
		}

		echo <<<HTML
<p>
	<label for="{$prefix}control_id">Control:</label>
	<select id="{$prefix}control_id" name="{$prefix}control_id">{$control_options}</select>
	<span>Only for Tests!{$control_blurb}</span>
</p>
<p>
	<label for="{$prefix}experiment_code" style="display:block;">Experiment Code:</label>
	<textarea id="{$prefix}experiment_code" name="{$prefix}experiment_code" style="width: 100%;">{$code}</textarea>
	<span style="display:block;">Only for Controls!</span>
</p>{$test_links}
HTML;

	}

	public function save_sitemap_meta_boxes( $post_id, \WP_Post $post = null ) {

		$do_save = true;

		$allowed_post_types = $this->get_site_mapped_post_types();
		if ( empty($post_id) || empty( $post ) || $this->_saved_sitemap_meta_boxes ) {
			$do_save = false;
		} else if ( ! in_array( $post->post_type, $allowed_post_types ) ) {
			$do_save = false;
		}

		# Do not save for revisions or autosaves
		if ( defined('DOING_AUTOSAVE') && is_int( wp_is_post_revision( $post ) ) || is_int( wp_is_post_autosave( $post ) ) ) {
			$do_save = false;
		}

		# Make sure proper post is being worked on
		if ( !array_key_exists('post_ID', $_POST) || $post_id != $_POST['post_ID'] ) {
			$do_save = false;
		}

		# Make sure we have the needed permissions to save
		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			$do_save = false;
		}

		$nonce_field_name = $this->_prefix . 'sitemap_nonce';
		if ( ! array_key_exists( $nonce_field_name, $_POST) ) {
			$do_save = false;
		} else if ( ! wp_verify_nonce( $_POST["{$nonce_field_name}"], 'updating_sitemap_settings' ) ) {
			$do_save = false;
		} else if ( ! check_admin_referer( 'updating_sitemap_settings', $nonce_field_name ) ) {
			$do_save = false;
		}
		if ( $do_save ) {
			$field_id = $this->_prefix . 'sitemap_hide';
			$hide_sitemap = ( array_key_exists($field_id, $_POST) )? (int)$_POST["{$field_id}"] : 0;
			$current_hide_sitemap = (int)get_post_meta( $post_id, "_{$this->_prefix}sitemap_hide", true);
			if ( $hide_sitemap != $current_hide_sitemap ) {
				if ( 0 == $hide_sitemap ) {
					delete_post_meta( $post_id, "_{$this->_prefix}sitemap_hide" );
				} else {
					update_post_meta( $post_id, "_{$this->_prefix}sitemap_hide", $hide_sitemap );
				}
			}
			$this->_saved_sitemap_meta_boxes = true;
		}
		return;

	}

	public function save_meta_boxes( $post_id, \WP_Post $post = null ) {

		$do_save = true;

		$default_allowed = $this->get_testable_post_type_values();
		$allowed_post_types = $this->get_option('split_test_post_types', $default_allowed);
		# Do not save if there is no post id or post
		if ( empty($post_id) || empty( $post ) || $this->_saved_meta_boxes ) {
			# echo "<p>Missing post id or post or already saved</p>";
			$do_save = false;
		} else if ( ! in_array( $post->post_type, $allowed_post_types ) ) {
			$do_save = false;
		}

		# Do not save for revisions or autosaves
		if ( defined('DOING_AUTOSAVE') && is_int( wp_is_post_revision( $post ) ) || is_int( wp_is_post_autosave( $post ) ) ) {
			$do_save = false;
		}

		# Make sure proper post is being worked on
		if ( !array_key_exists('post_ID', $_POST) || $post_id != $_POST['post_ID'] ) {
			$do_save = false;
		}

		# Make sure we have the needed permissions to save
		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			$do_save = false;
		}

		$nonce_field_name = $this->_prefix . 'split_test_nonce';
		if ( ! array_key_exists( $nonce_field_name, $_POST) ) {
			$do_save = false;
		} else if ( ! wp_verify_nonce( $_POST["{$nonce_field_name}"], 'updating_split_test_settings' ) ) {
			$do_save = false;
		} else if ( ! check_admin_referer( 'updating_split_test_settings', $nonce_field_name ) ) {
			$do_save = false;
		}
		if ( $do_save ) {
			$field_id = $this->_prefix . 'control_id';
			$control_id = ( array_key_exists($field_id, $_POST) )? (int)$_POST["{$field_id}"] : '';
			$current_control_id = get_post_meta( $post_id, "_{$this->_prefix}control_id", true );
			if ( "{$control_id}" != "{$current_control_id}" ) {
				if ( empty( $control_id ) ) {
					delete_post_meta( $post_id, "_{$this->_prefix}control_id" );
				} else {
					update_post_meta( $post_id, "_{$this->_prefix}control_id", $control_id );
				}
			}
			$field_id = $this->_prefix . 'experiment_code';
			$code = ( array_key_exists($field_id, $_POST) )? stripcslashes($_POST["{$field_id}"]) : '';
			$current_code = get_post_meta( $post_id, "_{$this->_prefix}experiment_code", true );
			if ( "{$code}" != "{$current_code}" ) {
				if ( empty($code) ) {
					delete_post_meta( $post_id, "_{$this->_prefix}experiment_code" );
				} else {
					update_post_meta( $post_id, "_{$this->_prefix}experiment_code", "{$code}" );
				}
			}
			$this->_saved_meta_boxes = true;
		}
		return;
	}

	public function add_action_links( $links ) {

		$link = '<a href="' . get_admin_url( null, "options-general.php#{$this->_prefix}bh" ) . '">Settings</a>';
		array_unshift( $links, $link );

		return $links;
	}

	protected function validate_string( $value ) {
		return ltrim(rtrim(stripcslashes("{$value}")));
	}

	protected function validate_int( $value ) {
		return (int)$this->validate_string( $value );
	}

	protected function validate_check_box( $field_name = '', $data = null ) {
		$value = false;
		if ( !empty($field_name) && is_array($data) && array_key_exists($field_name, $data) ) {
			$x = $data["{$field_name}"];
			$value = ( '1' == "{$x}" )? true : false;
		}
		return $value;
	}

	protected function validate_radio_buttons( $field_name = '', $data = null, $choices = array(), $default = null ) {
		$value = $default;
		if ( !empty( $field_name) && is_array($data) && array_key_exists($field_name, $data) ) {
			$x = $data["{$field_name}"];
			if ( in_array( $x, $choices ) ) {
				$value = $x;
			}
		}
		return $value;
	}

	protected function validate_form_ids( $field_name = '', $data = null ) {
		$values = array();
		if ( !empty($field_name) && is_array($data) && array_key_exists($field_name, $data) ) {
			$choices = $data[$field_name];
			if ( is_array($choices) && 0 < count($choices) ) {
				foreach ( $choices as $form_id ) {
					$values[] = (int)$form_id;
				}
			}
		}
		return $values;
	}

	protected function validate_role( $value ) {
		if ( !empty($value) ) {
			$allowed = $this->get_role_names();
			if ( !array_key_exists($value, $allowed) ) {
				$value = '';
			}
		}
		return $value;
	}

	protected function validate_split_test_post_types( $values ) {
		$chosen = array();
		$allowed = $this->get_testable_post_types();
		if ( is_array($values) && 0 < count($values) ) {
			foreach ( $values as $post_type_name ) {
				if ( array_key_exists($post_type_name, $allowed) ) {
					$chosen[] = $post_type_name;
				}
			}
		} else {
			foreach ( $allowed as $name => $display_name ) {
				$chosen[] = $name;
			}
		}
		return $chosen;
	}

	protected function get_testable_post_type_values() {
		$types = $this->get_testable_post_types();
		return array_keys( $types );
	}

	protected function get_testable_post_types() {
		# if ( is_null( $this->_testable_post_types ) ) {
			$testable = array();
			$args = array(
				'public' => true
			);
			$available = get_post_types( $args, 'objects', 'and' );
			# echo "<p>Found post types:</p><pre>" . print_r($available, true) . "</pre>";
			if ( is_array( $available ) && 0 < count( $available ) ) {
				foreach ( $available as $post_type_name => $pt ) {
					if ( post_type_supports( $post_type_name, 'editor') ) {
						# echo "<p>Chekcing post type:</p><pre>" . print_r($pt, true) . "</pre>";
						if ( isset( $pt->labels ) && is_array( $pt->labels ) && array_key_exists( 'name', $pt->labels ) && ! empty( $pt->labels['name'] ) ) {
							$display_name = $pt->labels['name'];
						} else {
							if ( isset( $pt->label ) && ! empty( $pt->label ) ) {
								$display_name = $pt->label;
							} else {
								$display_name = $post_type_name;
							}
						}
						# echo "<p>Display Name is {$display_name}</p>";
						$testable["{$post_type_name}"] = $display_name;
					} else {
						# echo "<p>{$post_type_name} is not testable</p>";
					}
				}
			}
			#$this->_testable_post_types = $testable;
		#}
		#return $this->_testable_post_types;
		return $testable;
	}

	protected function validate_sitemap_search_settings( $received ) {
		$defaults = $this->get_default_sitemap_search_settings();
		if ( !is_array($received) ) {
			$received = $defaults;
		} else if ( 0 == count($received) ) {
			$received = $defaults;
		} else {
			foreach ( $defaults as $k => $v ) {
				if ( ! array_key_exists($k, $received) ) {
					$received[$k] = $v;
				}
			}
		}
		return $received;
	}

	protected function get_sitemap_allowed_query_order_bys() {
		return array(
			'title'
		);
	}

	protected function validate_sitemap_display_settings( $received ) {
		$items = array();
		if ( is_array( $received ) && 0 < count($received) ) {
			$allowed_post_types = $this->get_sitemap_allowed_post_types();
			$allowed_taxonomies = $this->get_sitemap_allowed_taxonomies();
			$i = count($received);
			foreach ( $received as $item_type_name => $settings ) {
				if ( in_array( $item_type_name, $allowed_post_types ) ) {
					$defaults = $this->get_default_sitemap_post_type_settings( $item_type_name, 0 );
					$order = array_key_exists('order', $settings)? (int)$settings['order'] : $i;
					$query_order = array_key_exists('query_order', $settings)? $this->validate_string($settings['query_order']) : $defaults['query_order'];
					$query_order = strtoupper($query_order);
					if ( $query_order != 'ASC' && $query_order != 'DESC' ) {
						$query_order = 'ASC';
					}
					$query_order_by = array_key_exists('query_order_by', $settings)? $this->validate_string($settings['query_order_by']) : $defaults['query_order_by'];
					$allowed_orders = $this->get_sitemap_allowed_query_order_bys();
					if ( ! in_array($query_order_by, $allowed_orders) ) {
						$query_order_by = reset( $allowed_orders );
					}
					$use = ( array_key_exists('use', $settings) && '1' == "{$settings['use']}" )? 1 : 0;
					if ( ! $use ) {
						$order = $i;
						$i++;
					}
					$items["{$order}"] = array(
						'type' => 'post',
						'name' => $item_type_name,
						'title' => array_key_exists('title', $settings)? $this->validate_string($settings['title']) : $defaults['title'],
						'link' => array_key_exists('link', $settings) ? $this->validate_string($settings['link']) : $defaults['link'],
						'intro' => array_key_exists('intro', $settings) ? $this->validate_string($settings['intro']) : $defaults['intro'],
						'query_order' => $query_order,
						'query_order_by' => $query_order_by,
						'use' => $use
					);
				} else if ( in_array( $item_type_name, $allowed_taxonomies ) ) {
					$defaults = $this->get_default_sitemap_post_type_settings( $item_type_name, 0 );
					$order = array_key_exists('order', $settings)? (int)$settings['order'] : $i;
					$query_order = array_key_exists('query_order', $settings)? $this->validate_string($settings['query_order']) : $defaults['query_order'];
					$query_order = strtoupper($query_order);
					if ( $query_order != 'ASC' && $query_order != 'DESC' ) {
						$query_order = 'ASC';
					}
					$query_order_by = array_key_exists('query_order_by', $settings)? $this->validate_string($settings['query_order_by']) : $defaults['query_order_by'];
					$allowed_orders = $this->get_sitemap_allowed_query_order_bys();
					if ( ! in_array($query_order_by, $allowed_orders) ) {
						$query_order_by = reset( $allowed_orders );
					}
					$use = ( array_key_exists('use', $settings) && '1' == "{$settings['use']}" )? 1 : 0;
					if ( ! $use ) {
						$order = $i;
						$i++;
					}
					$items["{$order}"] = array(
						'type' => 'taxonomy',
						'name' => $item_type_name,
						'title' => array_key_exists('title', $settings)? $this->validate_string($settings['title']) : $defaults['title'],
						'link' => array_key_exists('link', $settings) ? $this->validate_string($settings['link']) : $defaults['link'],
						'intro' => array_key_exists('intro', $settings) ? $this->validate_string($settings['intro']) : $defaults['intro'],
						'query_order' => $query_order,
						'query_order_by' => $query_order_by,
						'use' => $use
					);
				}
			}
			ksort( $items );
		}
		return $items;
	}

	public function validate_options( $in ) {
		$out = $this->get_options();

		# echo "<p>Received:</p><pre>" . print_r($in, true) . "</pre>"; exit;
		# Bugherd
		if ( array_key_exists('bugherd_api_key', $in ) ) {
			$out['bugherd_api_key'] = $this->validate_string( $in['bugherd_api_key'] );
		}
		$out['bugherd_use'] = $this->validate_check_box('bugherd_use', $in);
		if ( array_key_exists('bugherd_minimum_role', $in ) ) {
			$out['bugherd_minimum_role'] = $this->validate_role( $in['bugherd_minimum_role'] );
		}
		$out['bugherd_use_on_admin'] = $this->validate_check_box('bugherd_use_on_admin', $in);

		# Analytics
		if ( array_key_exists('analytics_api_key', $in ) ) {
			$out['analytics_api_key'] = $this->validate_string( $in['analytics_api_key'] );
		}
		if ( array_key_exists('analytics_tag_manager_key', $in ) ) {
			$out['analytics_tag_manager_key'] = $this->validate_string( $in['analytics_tag_manager_key'] );
		}
		$analytics_use_choices = array( self::ANALYTICS_NO, self::ANALYTICS_NORMAL, self::ANALYTICS_TAG_MANAGER );
		$out['analytics_use'] = $this->validate_radio_buttons('analytics_use', $in, $analytics_use_choices, self::ANALYTICS_NO);
		# $out['analytics_use'] = $this->validate_check_box('analytics_use', $in);
		if ( array_key_exists('analytics_maximum_role', $in ) ) {
			$out['analytics_maximum_role'] = $this->validate_role( $in['analytics_maximum_role'] );
		}
		$out['analytics_use_on_admin'] = $this->validate_radio_buttons('analytics_use_on_admin', $in, $analytics_use_choices, self::ANALYTICS_NO );
		# $out['analytics_use_on_admin'] = $this->validate_check_box('analytics_use_on_admin', $in);

		# Theme Unit Test
		$out['unit_test_use'] = $this->validate_check_box('unit_test_use', $in);

		# Disable Links
		$out['disable_links_use'] = $this->validate_check_box('disable_links_use', $in);
		$out['disable_links_alert'] = $this->validate_check_box('disable_links_alert', $in);
		if ( array_key_exists('disable_links_maximum_role', $in ) ) {
			$out['disable_links_maximum_role'] = $this->validate_role( $in['disable_links_maximum_role'] );
		}

		# Split Testing
		$out['split_test_use'] = $this->validate_check_box('split_test_use', $in);

		if ( array_key_exists('split_test_post_types', $in) ) {
			$out['split_test_post_types'] = $this->validate_split_test_post_types( $in['split_test_post_types'] );
		} else {
			$out['split_test_post_types'] = array();
		}

		# Custom Login
		$out['login_use'] = $this->validate_check_box('login_use', $in);
		if ( array_key_exists('login_head_title', $in ) ) {
			$out['login_head_title'] = $this->validate_string( $in['login_head_title'] );
		}
		if ( array_key_exists('login_head_link_url', $in ) ) {
			$out['login_head_link_url'] = $this->validate_string( $in['login_head_link_url'] );
		}
		if ( array_key_exists('login_image_url', $in ) ) {
			$out['login_image_url'] = $this->validate_string( $in['login_image_url'] );
		}
		if ( array_key_exists('login_image_h', $in ) ) {
			$out['login_image_h'] = $this->validate_int( $in['login_image_h'] );
		}
		if ( array_key_exists('login_image_w', $in ) ) {
			$out['login_image_w'] = $this->validate_int( $in['login_image_w'] );
		}

		# Mail Chimp
		$out['mailchimp_use'] = $this->validate_check_box('mailchimp_use', $in);
		if ( array_key_exists('mailchimp_api_key', $in ) ) {
			$out['mailchimp_api_key'] = $this->validate_string( $in['mailchimp_api_key'] );
		}
		if ( array_key_exists('mailchimp_list_id', $in ) ) {
			$out['mailchimp_list_id'] = $this->validate_string( $in['mailchimp_list_id'] );
		}
		if ( array_key_exists('mailchimp_email_field', $in ) ) {
			$out['mailchimp_email_field'] = $this->validate_string( $in['mailchimp_email_field'] );
		}
		$out['mailchimp_double_opt_in'] = $this->validate_check_box('mailchimp_double_opt_in', $in);
		$out['mailchimp_contact_form_ids'] = $this->validate_form_ids( 'mailchimp_contact_form_ids', $in);

		if ( array_key_exists('mailchimp_extra_fields', $in ) ) {
			$out['mailchimp_extra_fields'] = $this->validate_string( $in['mailchimp_extra_fields'] );
		}
		if ( array_key_exists('mailchimp_merge_tags', $in ) ) {
			$out['mailchimp_merge_tags'] = $this->validate_string( $in['mailchimp_merge_tags'] );
		}

		# sitemap settings
		$out['sitemap_use'] = $this->validate_check_box('sitemap_use', $in);
		# sitemap_cache_minutes (int)
		if ( array_key_exists('sitemap_cache_minutes', $in ) ) {
			$out['sitemap_cache_minutes'] = $this->validate_int( $in['sitemap_cache_minutes'] );
		}
		# sitemap_search_placement (int)
		if ( array_key_exists('sitemap_search_placement', $in ) ) {
			$x = $this->validate_int( $in['sitemap_search_placement'] );
			if ( $x < 0 || $x > 2 ) {
				$x = 0;
			}
			$out['sitemap_search_placement'] = $x;
		}
		# sitemap_search_settings array([label_text],[placeholder_text],[button_text])
		if ( array_key_exists('sitemap_search_settings', $in) ) {
			$out['sitemap_search_settings'] = $this->validate_sitemap_search_settings( $in['sitemap_search_settings'] );
		}
		# sitemap_display_settings array([type],[name],[title],[link],[query_order],[intro]
		if ( array_key_exists('sitemap_display_settings', $in) ) {
			$out['sitemap_display_settings'] = $this->validate_sitemap_display_settings( $in['sitemap_display_settings'] );
		}

		return $out;
	}

	#public function setup_options_panels() {
	public function do_register_settings() {

		register_setting(
			$this->_prefix . 'settings_tab',
			$this->_options_key,
			array($this,'validate_options')
		);
		add_settings_section(
			'top_panel',
			'Introduction',
			array($this, 'draw_introduction'),
			$this->_prefix . 'settings'
		);

		# Bugherd ----------------------------------------

		add_settings_section(
			'bugherd_settings',
			'<span style="display:block;margin-bottom:2em;" id="' . $this->_prefix . 'bh">&nbsp;</span>Bugherd Settings',
			array($this, 'draw_bugherd_header'),
			$this->_prefix . 'settings'
		);
		add_settings_field(
			'bugherd_use',
			'Enabled?',
			array($this, 'draw_bugherd_use'),
			$this->_prefix . 'settings',
			'bugherd_settings'
		);
		add_settings_field(
			'bugherd_minimum_role',
			'Minimum Role To Use',
			array($this, 'draw_bugherd_minimum_role'),
			$this->_prefix . 'settings',
			'bugherd_settings'
		);
		add_settings_field(
			'bugherd_api_key',
			'Bugherd API Key:',
			array($this, 'draw_bugherd_api_key'),
			$this->_prefix . 'settings',
			'bugherd_settings'
		);
		add_settings_field(
			'bugherd_use_on_admin',
			'Use on Admin Panels?',
			array($this, 'draw_bugherd_use_on_admin'),
			$this->_prefix . 'settings',
			'bugherd_settings'
		);

		# Google Analytics --------------------------------

		add_settings_section(
			'analytics_settings',
			'<span style="display:block;margin-bottom:2em;" id="' . $this->_prefix . 'ga">&nbsp;</span>Google Analytics Settings',
			array($this, 'draw_analytics_header'),
			$this->_prefix . 'settings'
		);
		add_settings_field(
			'analytics_use',
			'Enabled?',
			array($this, 'draw_analytics_use'),
			$this->_prefix . 'settings',
			'analytics_settings'
		);
		add_settings_field(
			'analytics_maximum_role',
			'Suppress for Role?',
			array($this, 'draw_analytics_maximum_role'),
			$this->_prefix . 'settings',
			'analytics_settings'
		);
		add_settings_field(
			'analytics_api_key',
			'Analytics UA Number:',
			array($this, 'draw_analytics_api_key'),
			$this->_prefix . 'settings',
			'analytics_settings'
		);
		add_settings_field(
			'analytics_tag_manager_key',
			'Analytics Tag Manager Key:',
			array($this, 'draw_analytics_tag_manager_key'),
			$this->_prefix . 'settings',
			'analytics_settings'
		);
		add_settings_field(
			'analytics_use_on_admin',
			'Use on Admin Panels?',
			array($this, 'draw_analytics_use_on_admin'),
			$this->_prefix . 'settings',
			'analytics_settings'
		);

		# Theme Unit Test -----------------------------------

		add_settings_section(
			'unit_test_settings',
			'<span style="display:block;margin-bottom:2em;" id="' . $this->_prefix . 'unit">&nbsp;</span>Unit Testing',
			array($this, 'draw_unit_test_settings_header'),
			$this->_prefix . 'settings'
		);
		add_settings_field(
			'unit_test_use',
			'Enable Unit Test Short Code?',
			array($this, 'draw_unit_test_use'),
			$this->_prefix . 'settings',
			'unit_test_settings'
		);

		# Disable Links --------------------------

		add_settings_section(
			'disable_links_settings',
			'<span style="display:block;margin-bottom:2em;" id="' . $this->_prefix . 'klink">&nbsp;</span>Disable Links',
			array($this, 'draw_disable_links_header'),
			$this->_prefix . 'settings'
		);
		add_settings_field(
			'disable_links_use',
			'Enable Disable Links?',
			array($this, 'draw_disable_links_use'),
			$this->_prefix . 'settings',
			'disable_links_settings'
		);
		add_settings_field(
			'disable_links_maximum_role',
			'Suppress for Roles',
			array($this, 'draw_disable_links_maximum_role'),
			$this->_prefix . 'settings',
			'disable_links_settings'
		);

		# Split Testing -----------------------------

		add_settings_section(
			'split_test_settings',
			'<span style="display:block;margin-bottom:2em;" id="' . $this->_prefix . 'split">&nbsp;</span>Split Testing',
			array($this, 'draw_split_test_settings_header'),
			$this->_prefix . 'settings'
		);
		add_settings_field(
			'split_test_use',
			'Enable Split Testing?',
			array($this, 'draw_split_test_use'),
			$this->_prefix . 'settings',
			'split_test_settings'
		);
		add_settings_field(
			'split_test_post_types',
			'Allowed Post Types:',
			array($this, 'draw_split_test_post_types'),
			$this->_prefix . 'settings',
			'split_test_settings'
		);

		# Custom Login Adjustments --------------------------------

		add_settings_section(
			'custom_login_settings',
			'<span style="display:block;margin-bottom:2em;" id="' . $this->_prefix . 'login">&nbsp;</span>Custom Login',
			array($this, 'draw_custom_login_settings_header'),
			$this->_prefix . 'settings'
		);
		add_settings_field(
			'login_use',
			'Enable Custom Login?',
			array($this, 'draw_login_use'),
			$this->_prefix . 'settings',
			'custom_login_settings'
		);
		add_settings_field(
			'login_head_title',
			'Login Header Title:',
			array($this, 'draw_login_head_title'),
			$this->_prefix . 'settings',
			'custom_login_settings'
		);
		add_settings_field(
			'login_head_link_url',
			'Login Header Link:',
			array($this, 'draw_login_head_link_url'),
			$this->_prefix . 'settings',
			'custom_login_settings'
		);
		add_settings_field(
			'login_image_url',
			'Login Image URL:',
			array($this, 'draw_login_image_url'),
			$this->_prefix . 'settings',
			'custom_login_settings'
		);
		add_settings_field(
			'login_image_h',
			'Login Image Height (pixels):',
			array($this, 'draw_login_image_h'),
			$this->_prefix . 'settings',
			'custom_login_settings'
		);
		add_settings_field(
			'login_image_w',
			'Login Image Width (pixels):',
			array($this, 'draw_login_image_w'),
			$this->_prefix . 'settings',
			'custom_login_settings'
		);

		# Mail Chimp Mailing List Adder
		add_settings_section(
			'mailchimp_settings',
			'<span style="display:block;margin-bottom:2em;" id="' . $this->_prefix . 'mchimp">&nbsp;</span>Mail Chimp',
			array($this, 'draw_mailchimp_settings_header'),
			$this->_prefix . 'settings'
		);
		add_settings_field(
			'mailchimp_use',
			'Use mail chimp adder?',
			array($this, 'draw_mailchimp_use'),
			$this->_prefix . 'settings',
			'mailchimp_settings'
		);
		add_settings_field(
			'mailchimp_api_key',
			'Mail Chimp API Key:',
			array($this, 'draw_mailchimp_api_key'),
			$this->_prefix . 'settings',
			'mailchimp_settings'
		);
		add_settings_field(
			'mailchimp_list_id',
			'Mailing List ID:',
			array($this, 'draw_mailchimp_list_id'),
			$this->_prefix . 'settings',
			'mailchimp_settings'
		);
		add_settings_field(
			'mailchimp_email_field',
			'Email Address Field Name:',
			array($this, 'draw_mailchimp_email_field'),
			$this->_prefix . 'settings',
			'mailchimp_settings'
		);
		add_settings_field(
			'mailchimp_name_field',
			'Merge Tags:',
			array($this, 'draw_mailchimp_merge_tags'),
			$this->_prefix . 'settings',
			'mailchimp_settings'
		);
		add_settings_field(
			'mailchimp_double_opt_in',
			'Double Opt In?',
			array($this, 'draw_mailchimp_double_opt_in'),
			$this->_prefix . 'settings',
			'mailchimp_settings'
		);
		add_settings_field(
			'mailchimp_contact_form_ids',
			'Contact Forms:',
			array($this, 'draw_mailchimp_contact_form_ids'),
			$this->_prefix . 'settings',
			'mailchimp_settings'
		);

		# Sitemap Shortcode Settings
		add_settings_section(
			'sitemap_settings',
			'<span style="display:block;margin-bottom:2em;" id="' . $this->_prefix . 'sitemap">&nbsp;</span>Site Map',
			array($this, 'draw_sitemap_settings_header'),
			$this->_prefix . 'settings'
		);
		add_settings_field(
			'sitemap_use',
			'Use visual sitemap?',
			array($this, 'draw_sitemap_use'),
			$this->_prefix . 'settings',
			'sitemap_settings'
		);
		add_settings_field(
			'sitemap_cache_settings',
			'Cache Settings:',
			array($this, 'draw_sitemap_cache_settings'),
			$this->_prefix . 'settings',
			'sitemap_settings'
		);
		add_settings_field(
			'sitemap_search_settings',
			'Search Settings:',
			array($this, 'draw_sitemap_search_settings'),
			$this->_prefix . 'settings',
			'sitemap_settings'
		);
		add_settings_field(
			'sitemap_display_settings',
			'Display Settings:',
			array($this, 'draw_sitemap_display_settings'),
			$this->_prefix . 'settings',
			'sitemap_settings'
		);

	}

	public function draw_introduction() {
		/** @noinspection HtmlUnknownAnchorTarget */
		echo <<<HTML
<p>Current Utilities:
	<a class="button secondary" href="#{$this->_prefix}bh">Bug Herd</a>
	| <a class="button secondary" href="#{$this->_prefix}ga">Google Analytics</a>
	| <a class="button secondary" href="#{$this->_prefix}unit">Theme Unit Test</a>
	| <a class="button secondary" href="#{$this->_prefix}klink">Disable Links</a>
	| <a class="button secondary" href="#{$this->_prefix}split">Split Testing</a>
	| <a class="button secondary" href="#{$this->_prefix}login">Custom Login</a>
	| <a class="button secondary" href="#{$this->_prefix}mchimp">Mail Chimp</a>
	| <a class="button secondary" href="#{$this->_prefix}sitemap">Site Map</a>
</p>
HTML;

	}

	public function draw_split_test_settings_header() {
		echo <<<HTML
<p id="{$this->_prefix}split">To use split testing, you need to enable split testing and select at least one post type to enable split testing for.</p>
<p>Once that is done, you can set up a split test by:</p>
<ol>
	<li>Create a page of the same type and selecting the control page for it.<br />The <a href="https://wordpress.org/plugins/duplicate-post/" target="_blank">Duplicate Post Plugin</a> makes it simple to copy an existing page as a starting point for an experiment.</li>
	<li>Set up your experiment in Google Experiments.</li>
	<li>Copy the code into the Experiments Code box on the control page.</li>
</ol>
<p>When you have finished your experiment, update the control (if it was not the winner) and then remove the experiments code from it and, if wanted, delete the tests.</p>
HTML;

	}

	public function draw_split_test_use() {
		$current = $this->get_option('split_test_use', false);
		$checked = ( $current )? ' checked="checked"':'';
		echo <<<HTML
<p><input id="hf_split_test_use" name="{$this->_options_key}[split_test_use]" type="checkbox" value="1" {$checked} /></p>
<p class="description">If checked, split testing via google experiments will be enabled.</p>
HTML;

	}

	public function draw_split_test_post_types() {
		$defaults = $this->get_testable_post_type_values();
		$current = $this->get_option('split_test_post_types', $defaults);

		$available = $this->get_testable_post_types();

		# echo "<p>Current:</p><pre>" . print_r($current, true) . "</pre>";
		# echo "<p>Available:</p><pre>" . print_r($available, true) . "</pre>";
		# echo "<p>Defaults:</p><pre>" . print_r($defaults, true) . "</pre>";

		$choices = '';
		foreach ( $available as $post_type => $display_name ) {
			$checked = in_array( $post_type, $current )? ' checked="checked"' : '';
			$choices .= <<<HTML
<p><input type="checkbox" id="{$this->_prefix}st_post_type_{$post_type}" name="{$this->_options_key}[split_test_post_types][]" value="{$post_type}" {$checked} /> <label for="{$this->_prefix}st_post_type_{$post_type}">{$display_name}</label></p>
HTML;

		}
		echo <<<HTML
{$choices}
<p class="description">Split testing fields will only show on the specified post types.</p>
HTML;

	}

	public function draw_bugherd_header() {
		echo <<<HTML
<p>Configure <a href="https://bugherd.com">Bugherd</a> settings here.</p>
HTML;

	}

	public function draw_bugherd_use() {
		$current = $this->get_option('bugherd_use', false);
		$checked = ( $current )? ' checked="checked"':'';
		echo <<<HTML
<p><input id="hf_bugherd_use" name="{$this->_options_key}[bugherd_use]" type="checkbox" value="1" {$checked} /></p>
<p class="description">If checked, bugherd will be used on the front end of the site.</p>
HTML;

	}

	public function draw_bugherd_minimum_role() {
		$current = $this->get_option('bugherd_minimum_role', '');
		$roles_available = $this->get_role_names();
		$selected = ( empty($current) )?' selected="selected"':'';
		$role_options = <<<HTML
<option value="" {$selected}>No Restriction</option>
HTML;

		if ( is_array($roles_available) ) {
			foreach ( $roles_available as $role_id => $role_name ) {
				$selected = ( "{$role_id}" == "{$current}" ) ? ' selected="selected"':'';
				$role_options .= <<<HTML
<option value="{$role_id}" {$selected}>{$role_name}</option>
HTML;

			}
		}
		echo <<<HTML
<p><select id="hf_bugherd_minimum_role" name="{$this->_options_key}[bugherd_minimum_role]">{$role_options}</select> </p>
<p class="description">You can use this to prevent bugherd from being loaded for users that lack a particular role.</p>
<p class="description">If used, only users of the specified role or higher will be able to see bugherd.</p>
<p class="description">To load bugherd for everyone, set this to No Restriction. See <a href="https://codex.wordpress.org/Roles_and_Capabilities" target="_blank">The WP Roles and Capabilities page</a> for more information.</p>
HTML;


	}

	public function draw_bugherd_api_key() {
		$current = $this->get_option('bugherd_api_key', '');
		echo <<<HTML
<p><input id="hf_bugherd_api_key" name="{$this->_options_key}[bugherd_api_key]" type="text" value="{$current}" /></p>
<p class="description">Enter your bugherd api key here. Without this, bugherd support will not work.</p>
HTML;

	}

	public function draw_bugherd_use_on_admin() {
		$current = $this->get_option('use_on_admin', false);
		$checked = ( $current )? ' checked="checked"':'';
		echo <<<HTML
<p><input id="hf_bugherd_use_on_admin" name="{$this->_options_key}[bugherd_use_on_admin]" type="checkbox" value="1" {$checked} /></p>
<p class="description">If checked, Bugherd will be used on the admin panels as well as on the front end. Useful for testing out back end custom functionality.</p>
<hr id="{$this->_prefix}an" style="margin-bottom:2em;" />
HTML;

	}

	public function draw_analytics_header() {
		echo <<<HTML
<p>Configure Google Analytics here.</p>
HTML;

	}

	public function draw_analytics_use() {
		$current = $this->get_option('analytics_use', self::ANALYTICS_NO);
		if ( false === $current ) {
			$current = self::ANALYTICS_NO;
		} else if ( true === $current ) {
			$current = self::ANALYTICS_NORMAL;
		}
		$allowed = array(
			'No' => self::ANALYTICS_NO,
			'Normal' => self::ANALYTICS_NORMAL,
			'Tag Manager' => self::ANALYTICS_TAG_MANAGER
		);
		if ( !in_array( $current, $allowed ) ) {
			$current = self::ANALYTICS_NO;
		}
		$choices = array();
		foreach ( $allowed as $k => $v ) {
			$checked = ( "{$current}" === "{$v}" )? ' checked="checked"' : '';
			$choices[] = <<<HTML
<label for="hf_analytics_use_{$v}"><input type="radio" id="hf_analytics_use_{$v}" name="{$this->_options_key}[analytics_use]" value="{$v}" {$checked} /> {$k}</label>
HTML;

		}
		if ( 0 < count($choices) ) {
			$choice_list = implode('<br />', $choices);
		} else {
			$choice_list = 'No choices available';
		}
		echo <<<HTML
<p>{$choice_list}</p>
<p class="description">Set to Normal to use normal analytics. Set to Tag Manager to use the GA Tag Manager.</p>
HTML;

#		$checked = ( $current )? ' checked="checked"':'';
#		echo <<<HTML
#<p><input id="hf_analytics_use" name="{$this->_options_key}[analytics_use]" type="checkbox" value="1" {$checked} /></p>
#<p class="description">Set to Normal to use normal analytics. Set to Tag Manager to use the GA Tag Manager.</p>
#HTML;

	}

	public function draw_analytics_api_key() {
		$current = $this->get_option('analytics_api_key', '');
		echo <<<HTML
<p><input id="hf_analytics_api_key" name="{$this->_options_key}[analytics_api_key]" type="text" value="{$current}" /></p>
<p class="description">Enter you Google Analytics UA Number here.</p>
HTML;

	}

	public function draw_analytics_tag_manager_key() {
		$current = $this->get_option('analytics_tag_manager_key', '');
		echo <<<HTML
<p><input id="hf_analytics_tag_key" name="{$this->_options_key}[analytics_tag_manager_key]" type="text" value="{$current}" /></p>
<p class="description">Enter you Google Analytics Tag Manager Key here. It should look like GTM-XXXXXX</p>
HTML;

	}

	public function draw_analytics_use_on_admin() {
		$current = $this->get_option('analytics_use_on_admin', self::ANALYTICS_NO);
		if ( false === $current ) {
			$current = self::ANALYTICS_NO;
		} else if ( true === $current ) {
			$current = self::ANALYTICS_NORMAL;
		}
		$allowed = array(
			'No' => self::ANALYTICS_NO,
			'Normal' => self::ANALYTICS_NORMAL,
			'Tag Manager' => self::ANALYTICS_TAG_MANAGER
		);
		if ( !in_array( $current, $allowed ) ) {
			$current = self::ANALYTICS_NO;
		}
		$choices = array();
		foreach ( $allowed as $k => $v ) {
			$checked = ( "{$current}" === "{$v}" )? ' checked="checked"' : '';
			$choices[] = <<<HTML
<label for="hf_analytics_use_on_admin_{$v}"><input type="radio" id="hf_analytics_use_on_admin{$v}" name="{$this->_options_key}[analytics_use_on_admin]" value="{$v}" {$checked} /> {$k}</label>
HTML;

		}
		if ( 0 < count($choices) ) {
			$choice_list = implode('<br />', $choices);
		} else {
			$choice_list = 'No choices available';
		}
		echo <<<HTML
<p>{$choice_list}</p>
<p class="description">If you want analytics to load on the wordpress admin panels, set the type of analytics to use here.</p>
HTML;

	}

	public function draw_analytics_maximum_role() {
		$current = $this->get_option('analytics_maximum_role', '');
		$roles_available = $this->get_role_names();
		$selected = ( empty($current) )?' selected="selected"':'';
		$role_options = <<<HTML
<option value="" {$selected}>No Restriction</option>
HTML;

		if ( is_array($roles_available) ) {
			foreach ( $roles_available as $role_id => $role_name ) {
				$selected = ( "{$role_id}" == "{$current}" ) ? ' selected="selected"':'';
				$role_options .= <<<HTML
<option value="{$role_id}" {$selected}>{$role_name}</option>
HTML;

			}
		}
		echo <<<HTML
<p><select id="hf_analytics_maximum_role" name="{$this->_options_key}[analytics_maximum_role]">{$role_options}</select> </p>
<p class="description">You can use this to prevent analytics from being used by logged in users having at least the specified role.</p>
<p class="description">If used, the specified role (and all higher roles) will not trigger analytics on the front end of the site.</p>
<p class="description">To suppress for all logged in users, set this to Subscriber. See <a href="https://codex.wordpress.org/Roles_and_Capabilities" target="_blank">The WP Roles and Capabilities page</a> for more information.</p>
HTML;

	}

	public function draw_unit_test_settings_header() {
		echo <<<HTML
<p id="{$this->_prefix}ut">The Theme Unit Test short code allows you to quickly
and easily make sure that all normal wordpress styless have been set up.</p>
HTML;

	}

	public function draw_unit_test_use() {
		$current = $this->get_option('unit_test_use', false);
		$checked = ( $current )? ' checked="checked"':'';
		echo <<<HTML
<input id="hf_unit_test_use" name="{$this->_options_key}[unit_test_use]" type="checkbox" value="1" {$checked} />
<p class="description">This allows you to use the <code>[unit_test]</code> shortcode.</p>
<p class="description">To use the short code, enter <code>[unit_test]</code> into the body of any page or post.</p>
<p class="description">If you want just a certain section use <code>[unit_test sections="x"]</code> where x can be all, markup, or images.</p>

HTML;

	}

	public function draw_disable_links_header() {
		echo <<<HTML
<p id="{$this->_prefix}ut">If you have pages that you don't want a client to look at
	yet, use this utility to be able to be able to add a no_click css class to links
	that are not yet ready for people to view.</p>
HTML;

	}

	public function draw_disable_links_use() {
		$current = $this->get_option('disable_links_use', false);
		$checked = ( $current )? ' checked="checked"':'';
		$current_alert = $this->get_option('disable_links_alert', false);
		$alert_checked = ( $current_alert )? ' checked="checked"':'';
		echo <<<HTML
<p>Allow Disable Links? <input id="hf_disable_links_use" name="{$this->_options_key}[disable_links_use]" type="checkbox" value="1" {$checked} />
<br />Pop up alert? <input id="hf_disable_links_alert" name="{$this->_options_key}[disable_links_alert]" type="checkbox" value="1" {$alert_checked} /></p>
<p class="description">This allows you to add a <code>no_click</code> css class to menu items and other links to disable them.</p>
<p class="description">To use the short code, enter <code>[unit_test]</code> into the body of any page or post.</p>
<p class="description">If you want just a certain section use <code>[unit_test sections="x"]</code> where x can be all, markup, or images.</p>
<p class="description">If you check the pop up alert box, in addition to preventing the link from working, an alert will note that it is not yet ready for use.</p>
HTML;

	}

	public function draw_disable_links_maximum_role() {
		$current = $this->get_option('disable_links_maximum_role', '');
		$roles_available = $this->get_role_names();
		$selected = ( empty($current) )?' selected="selected"':'';
		$role_options = <<<HTML
<option value="" {$selected}>No Restriction</option>
HTML;

		if ( is_array($roles_available) ) {
			foreach ( $roles_available as $role_id => $role_name ) {
				$selected = ( "{$role_id}" == "{$current}" ) ? ' selected="selected"':'';
				$role_options .= <<<HTML
<option value="{$role_id}" {$selected}>{$role_name}</option>
HTML;

			}
		}
		echo <<<HTML
<p><select id="hf_disable_links_maximum_role" name="{$this->_options_key}[disable_links_maximum_role]">{$role_options}</select> </p>
<p class="description">You can use this to avoid disabling links for users that have at least the specified role.</p>
<p class="description">If used, the specified role (and all higher roles) will not be prevented from clicking links with the <code>no_click</code> css class.</p>
<p class="description">To suppress for all logged in users, set this to Subscriber. See <a href="https://codex.wordpress.org/Roles_and_Capabilities" target="_blank">The WP Roles and Capabilities page</a> for more information.</p>
HTML;

	}
	protected function get_role_names() {
		if ( is_null( $this->_role_names ) ) {
			$r = new \WP_Roles();
			$this->_role_names = $r->get_names();
		}
		return $this->_role_names;
	}

	public function draw_custom_login_settings_header() {
		echo <<<HTML
<p id="{$this->_prefix}login">If you want to set a custom title, link, and image on the login page
	use this option and setup the settings you want below.</p>
HTML;

	}

	public function draw_login_use() {
		$current = $this->get_option('login_use', false);
		$checked = ( $current )? ' checked="checked"':'';
		echo <<<HTML
<p><input id="hf_login_use" name="{$this->_options_key}[login_use]" type="checkbox" value="1" {$checked} /></p>
<p class="description">If checked, overrides set below will be used on the login page.</p>
HTML;

	}

	public function draw_login_head_title() {
		$current = $this->get_option('login_head_title', '');
		$site_title = get_option('blogname');
		echo <<<HTML
<p><input id="hf_login_head_title" name="{$this->_options_key}[login_head_title]" type="text" value="{$current}" /></p>
<p class="description">Enter the displayed title for the login image when hovered over or missing
	<br /><code>default:</code> {$site_title}</p>
HTML;

	}

	public function draw_login_head_link_url() {
		$current = $this->get_option('login_head_link_url', '');
		$site_url = home_url();
		echo <<<HTML
<p><input id="hf_login_head_link_url" name="{$this->_options_key}[login_head_link_url]" type="text" value="{$current}" /></p>
<p class="description">Enter the link to use for the image when clicked.
	<br /><code>current:</code> {$current}
	<br /><code>default:</code> {$site_url}</p>
HTML;

	}

	public function draw_login_image_url() {
		$current = $this->get_option('login_image_url', '');
		$default_logo = plugins_url('includes/images/hunchfree_logo.png', dirname(__FILE__));
		echo <<<HTML
<p><input id="hf_login_image_url" name="{$this->_options_key}[login_image_url]" type="text" value="{$current}" /></p>
<p class="description">Enter the full url to the login image you want to use.
	<br /><code>current:</code> {$current}
	<br /><code>default:</code> {$default_logo}</p>
HTML;

	}

	public function draw_login_image_h() {
		$current = $this->get_option('login_image_h', '');
		echo <<<HTML
<p><input id="hf_login_image_h" name="{$this->_options_key}[login_image_h]" type="text" value="{$current}" /> pixels</p>
<p class="description">Enter the height of the image you want to use in pixels.
	<br /><code>default:</code> 300</p>
HTML;

	}

	public function draw_login_image_w() {
		$current = $this->get_option('login_image_w', '');
		echo <<<HTML
<p><input id="hf_login_image_w" name="{$this->_options_key}[login_image_w]" type="text" value="{$current}" /> pixels</p>
<p class="description">Enter the width of the image you want to use in pixels.
	<br /><code>default:</code> 99</p>
HTML;

	}

	public function draw_mailchimp_settings_header() {
		echo <<<HTML
<p id="{$this->_prefix}mchimp">Automatically push Contact Form 7 email addresses to Mail Chimp.</p>
<p>What you need:</p>
<ol>
	<li><a href="http://mailchimp.com/" target="_blank">A Mail Chimp Account</a></li>
	<li><a href="http://kb.mailchimp.com/lists/managing-subscribers/find-your-list-id" target="_blank">A Mail Chimp List ID</a></li>
	<li><a href="https://wordpress.org/plugins/contact-form-7/">Contact Form 7</a></li>
	<li><a href="/wp-admin/admin.php?page=wpcf7" target="_blank">At least one Contact Form</a></li>
	<li>The field name of the email address in your contact forms. (keep it the same!)</li>
	<li>Optionally, the names of fields you want to use with merge tags to add extra data for people.</li>
</ol>
<p>Once you have the above, you can set up your options below. Don't forget to specify which forms should have
	any detected email field pushed into mail chimp.</p>
HTML;

	}

	public function draw_mailchimp_use() {
		$current = $this->get_option('mailchimp_use', false);
		$checked = ( $current )? ' checked="checked"':'';
		echo <<<HTML
<p><input id="hf_mailchimp_use" name="{$this->_options_key}[mailchimp_use]" type="checkbox" value="1" {$checked} /></p>
<p class="description">If checked, email addresses provided in contact form 7 forms will be pulled into mailchimp.</p>
HTML;

	}

	public function draw_mailchimp_api_key() {
		$current = $this->get_option('mailchimp_api_key', '');
		echo <<<HTML
<p><input id="hf_mailchimp_api_key" name="{$this->_options_key}[mailchimp_api_key]" type="text" value="{$current}" /></p>
<p class="description">Enter your <a href="http://kb.mailchimp.com/accounts/management/about-api-keys" target="_blank">mail chimp api key</a> here.</p>
HTML;

	}

	public function draw_mailchimp_list_id() {
		$current = $this->get_option('mailchimp_list_id', '');
		echo <<<HTML
<p><input id="hf_mailchimp_list_id" name="{$this->_options_key}[mailchimp_list_id]" type="text" value="{$current}" /></p>
<p class="description">Enter the ID or the mailing list you want people to be added to here.</p>
<p class="description">See <a href="http://kb.mailchimp.com/lists/managing-subscribers/find-your-list-id" target="_blank">how to find your list id</a> if needed.</p>
HTML;

	}

	public function draw_mailchimp_email_field() {
		$current = $this->get_option('mailchimp_email_field', '');
		echo <<<HTML
<p><input id="hf_mailchimp_email_field" name="{$this->_options_key}[mailchimp_email_field]" type="text" value="{$current}" /></p>
<p class="description">Enter name of the field that holds peoples email addresses on your contact forms.</p>
<p class="description">There is no way to use different field names for different forms currently so be sure to use the same
	email field name on all forms that you want to pull in new list subscribers with.</p>
HTML;

	}

	public function draw_mailchimp_merge_tags() {
		$current_fields = $this->get_option('mailchimp_extra_fields', '');
		$current_tags = $this->get_option('mailchimp_merge_tags', '');

		echo <<<HTML
<p><label for="{$this->_options_key}_mc_ef"><b>Extra Fields:</b></label>
	<br /><input id="{$this->_options_key}_mc_ef" class="large-text" name="{$this->_options_key}[mailchimp_extra_fields]" type="text" value="{$current_fields}" /></p>
<p class="description">If you want to use merge tags, enter the list of field names that hold the data you want to pull
	into mailchimp into the above box, <em>separated by commas</em>.

<p><label for="{$this->_options_key}_mc_mf"><b>Merge Tags:</b></label>
	<br /><input id="{$this->_options_key}_mc_mf" class="large-text" name="{$this->_options_key}[mailchimp_merge_tags]" type="text" value="{$current_tags}" /></p>
<p class="description">Name the merge tags you want to use here, <em>comma separated</em>.</p>
<p class="description">If you have a field that holds data for multiple merge tags separated by spaces, enter them like
	shown below:
	<br />Fields: <code>your-name,company</code>
	<br />Merge Tags: <code>FNAME LNAME,COMPANY</code></p>
<p class="description">The above will split the your-name field into first and last names and load the first name
	into the FNAME merge tag and the last name into the LNAME merge tag.</p>


HTML;

	}

	# Need a way to
	# (a) set up name pulling
	# (b) specify merge tags

	public function draw_mailchimp_double_opt_in() {
		$current = $this->get_option('mailchimp_double_opt_in', false);
		$checked = ( $current )? ' checked="checked"':'';
		echo <<<HTML
<p><input id="hf_mailchimp_double_opt_in" name="{$this->_options_key}[mailchimp_double_opt_in]" type="checkbox" value="1" {$checked} /></p>
<p class="description">If checked, people who you add to your mailing list will receive a confirmation email and will need
	to confirm that they want to subscribe before being added to the list.</p>
HTML;

	}

	public function draw_mailchimp_contact_form_ids() {
		$current = $this->get_option('mailchimp_contact_form_ids', array());

		$available = $this->get_contact_forms();

		$choices = '';
		foreach ( $available as $form_id => $display_name ) {
			$checked = in_array( $form_id, $current )? ' checked="checked"' : '';
			$choices .= <<<HTML
<p><input type="checkbox" id="{$this->_prefix}st_cf_{$form_id}" name="{$this->_options_key}[mailchimp_contact_form_ids][]" value="{$form_id}" {$checked} /> <label for="{$this->_prefix}st_cf_{$form_id}">{$display_name}</label></p>
HTML;

		}
		echo <<<HTML
{$choices}
<p class="description">Choose the contact form(s) that you want to pull email addresses from.</p>
HTML;

	}

	protected function get_contact_forms() {
		$available = array();
		if ( class_exists('WPCF7_ContactForm') ) {
			$args = array(
				'post_type' => \WPCF7_ContactForm::post_type,
				'posts_per_page' => -1,
				'order' => 'ASC',
				'orderby' => 'title'
			);
			$results = new \WP_Query( $args );
			if ( $results->have_posts() ) {
				while ( $results->have_posts() ) {
					$form = $results->next_post();
					$available[$form->ID] = $form->post_title;
				}
			}
		}
		return $available;
	}

	public function draw_sitemap_settings_header() {
		echo <<<HTML
<p id="{$this->_prefix}sitemap">Using the sitemap shortcode</p>
<p>The sitemap shortcode can be used to quickly and easily draw out a human readable sitemap.</p>
<p>To use it, set up the below configuration settings and then place the following shortcode where you want
	your sitemap to be drawn (e.g. your 404 template, sitemap page, etc.):</p>
<p><code>[sitemap]</code></p>
HTML;

	}


	public function draw_sitemap_use() {
		$current = $this->get_option('sitemap_use', false);
		$checked = ( $current )? ' checked="checked"':'';
		echo <<<HTML
<p><input id="{$this->_prefix}sitemap_use" name="{$this->_options_key}[sitemap_use]" type="checkbox" value="1" {$checked} /></p>
<p class="description">If checked, the <code>[sitemap]</code> shortcode can be used to display a visual sitemap.</p>
HTML;

	}

	public function draw_sitemap_cache_settings() {
		$current = $this->get_option('sitemap_cache_minutes', 0);

		echo <<<HTML
<p>
	<label for="{$this->_prefix}smap_cache">Cache Refresh Time (minutes)</label>
	<input type="text" id="{$this->_prefix}smap_cache" name="{$this->_options_key}[sitemap_cache_minutes]" value="{$current}" />
</p>
<p class="description">Set this to <code>0</code> to not cache the sitemap.</p>
<p class="description">Generating the sitemap is fairly intensive, so using the cache is recommended.</p>
HTML;

	}

	public function draw_sitemap_search_settings() {
		$cur_placement = $this->get_option('sitemap_search_placement', 0);

		$default_settings = $this->get_default_sitemap_search_settings();
		$cur_settings = $this->get_option('sitemap_search_settings', $default_settings);
		foreach ( $default_settings as $k => $v ) {
			if ( ! array_key_exists($k, $cur_settings) ) {
				$cur_settings[$k] = $v;
			}
		}

		$placement_options = array(
			0 => 'Do not display search',
			1 => 'Show search above sitemap',
			2 => 'Show search below sitemap'
		);
		$placement_choices = '';
		foreach ( $placement_options as $k => $v ) {
			$checked = ( "{$k}" == "{$cur_placement}" )? ' checked="checked"':'';
			$placement_choices .= <<<HTML
<div class="radio-item-wrap"><label for="{$this->_prefix}smap_place_{$k}"><input type="radio" name="{$this->_options_key}[sitemap_search_placement]" id="{$this->_prefix}smap_place_{$k}" value="{$k}" {$checked} />{$v}</label></div>
HTML;

		}

		echo <<<HTML
<p class="description">You can optionally place a search box above or below the generated sitemap.</p>
<p><b>Search Box Display and Placement</b></p>
<div class="radio-list-wrap">{$placement_choices}</div>
<p><b>Search Component Settings</b></p>
<p>The search box contains a label, a search terms input field, and a form submit button.</p>
<p>
	<label for="{$this->_prefix}smap_label_text">Label Text:</label>
	<input type="text" id="{$this->_prefix}smap_label_text" name="{$this->_options_key}[sitemap_search_settings][label_text]" value="{$cur_settings['label_text']}" />
	<span>If left empty, no label is drawn</span>
</p>
<p>
	<label for="{$this->_prefix}smap_placeholder_text">Placeholder Text:</label>
	<input type="text" id="{$this->_prefix}smap_placeholder_text" name="{$this->_options_key}[sitemap_search_settings][placeholder_text]" value="{$cur_settings['placeholder_text']}" />
	<span>If left empty, no placeholder text is used.</span>
</p>
<p>
	<label for="{$this->_prefix}smap_button_text">Button Text:</label>
	<input type="text" id="{$this->_prefix}smap_button_text" name="{$this->_options_key}[sitemap_search_settings][button_text]" value="{$cur_settings['button_text']}" />
	<span>If left empty, button text will default to Go</span>
</p>
HTML;

	}

	public function draw_sitemap_display_settings() {
		$current = $this->get_option('sitemap_display_settings', array());
		$taxes = $this->get_sitemap_allowed_taxonomies();
		$post_types = $this->get_sitemap_allowed_post_types();
		$used_taxes = array();
		$used_post_types = array();

		echo <<<HTML
<p>To create your sitemap, look through the available post types and taxonomies shown below and set them up.</p>
<p>For each item type, you can:</p>
<ul>
<li>Choose whether to include the group in the sitemap</li>
<li>Choose a title to show above the group and, optionally, a url to link it to.</li>
<li>Create a short, optional, introduction for the group</li>
<li>Adjust the display order of the group</li>
</ul>
HTML;

		$group_types = array('post','taxonomy');
		$i = 0;
		if ( 0 < count($current) ) {
			# We have some valid settings. Draw them out in the users specified order
			foreach ( $current as $k => $group ) {
				$group_type = ( array_key_exists('type', $group) )? $group['type'] : '';
				if ( in_array($group_type, $group_types) ) {
					$group_name = ( array_key_exists('name', $group) )? $group['name'] : '';
					if ( 'post' == "{$group_type}" && in_array($group_name, $post_types) ) {
						$this->draw_sitemap_post_type_settings( $group_name, $group, $i );
						$used_post_types[] = $group_name;
						$i++;
					} else if ( 'taxonomy' == "{$group_type}" && in_array( $group_name, $taxes ) ) {
						$this->draw_sitemap_taxonomy_settings( $group_name, $group, $i );
						$used_taxes[] = $group_name;
						$i++;
					}
				}
			}
		}

		foreach ( $post_types as $post_type ) {
			if ( !in_array( $post_type, $used_post_types ) ) {
				$this->draw_sitemap_post_type_settings( $post_type, null, $i );
				$i++;
			}
		}

		foreach ( $taxes as $taxonomy_name ) {
			if ( ! in_array($taxonomy_name, $used_taxes ) ) {
				$this->draw_sitemap_taxonomy_settings( $taxonomy_name, null, $i );
			}
		}
	}

	protected function get_sitemap_allowed_taxonomies() {
		$tax_args = array(
			'public' => true
		);
		return get_taxonomies( $tax_args, 'names' );

	}

	protected function get_sitemap_allowed_post_types() {
		$post_type_args = array(
			'public' => true
		);
		return get_post_types( $post_type_args, 'names' );
	}

	protected function draw_sitemap_post_type_settings( $post_type, $settings = null, $order = 0) {
		$defaults = $this->get_default_sitemap_post_type_settings( $post_type, $order );
		if ( !is_array( $settings ) ) {
			$settings = $defaults;
		} else if ( 0 == count($settings) ) {
			$settings = $defaults;
		} else {
			foreach ( $defaults as $k => $v ) {
				if ( ! array_key_exists($k, $settings) ) {
					$settings[$k] = $v;
				}
			}
		}
		$checked_use = ( '1' == "{$settings['use']}" ) ? ' checked="checked"' : '';
		$introduction = ( empty($settings['intro']) )? '' : htmlentities($settings['intro']);
		echo <<<HTML
<hr /><h4>{$post_type} settings</h4>
<input type="hidden" name="{$this->_options_key}[sitemap_display_settings][{$post_type}][query_order]" value="ASC" />
<input type="hidden" name="{$this->_options_key}[sitemap_display_settings][{$post_type}][query_order_by]" value="title" />
<p>
	<label for="{$this->_prefix}sm_{$post_type}_use">Show on Sitemap?</label>
	<input type="checkbox" id="{$this->_prefix}sm_{$post_type}_use" name="{$this->_options_key}[sitemap_display_settings][{$post_type}][use]" value="1" {$checked_use} />
</p>
<p>
	<label for="{$this->_prefix}sm_{$post_type}_ord">Section Order: </label>
	<input type="text" id="{$this->_prefix}sm_{$post_type}_ord" name="{$this->_options_key}[sitemap_display_settings][{$post_type}][order]" value="{$order}" />
</p>
<p>
	<label for="{$this->_prefix}sm_{$post_type}_title">Section Title: </label>
	<input type="text" id="{$this->_prefix}sm_{$post_type}_title" name="{$this->_options_key}[sitemap_display_settings][{$post_type}][title]" value="{$settings['title']}" />
	<span>If set, this title will be shown before the list of {$post_type} items.</span>
</p>
<p>
	<label for="{$this->_prefix}sm_{$post_type}_link">Section Title Link: </label>
	<input type="text" id="{$this->_prefix}sm_{$post_type}_link" name="{$this->_options_key}[sitemap_display_settings][{$post_type}][link]" value="{$settings['link']}" />
	<span>If specified, any title entered will be linked to the url set here.</span>
</p>
<p>
	<label for="{$this->_prefix}sm_{$post_type}_intro">Section Intro: </label>
	<textarea id="{$this->_prefix}sm_{$post_type}_intro" name="{$this->_options_key}[sitemap_display_settings][{$post_type}][intro]">{$introduction}</textarea>
	<span>If specified, any title entered will be linked to the url set here.</span>
</p>
HTML;

	}

	protected function draw_sitemap_taxonomy_settings( $taxonomy, $settings = null, $order = 0) {
		$defaults = $this->get_default_sitemap_taxonomy_settings( $taxonomy, $order );
		if ( !is_array( $settings ) ) {
			$settings = $defaults;
		} else if ( 0 == count($settings) ) {
			$settings = $defaults;
		} else {
			foreach ( $defaults as $k => $v ) {
				if ( ! array_key_exists($k, $settings) ) {
					$settings[$k] = $v;
				}
			}
		}
		$checked_use = ( '1' == "{$settings['use']}" ) ? ' checked="checked"' : '';
		$introduction = ( empty($settings['intro']) )? '' : htmlentities($settings['intro']);
		echo <<<HTML
<hr /><h4>{$taxonomy} settings</h4>
<input type="hidden" name="{$this->_options_key}[sitemap_display_settings][{$taxonomy}][query_order]" value="ASC" />
<input type="hidden" name="{$this->_options_key}[sitemap_display_settings][{$taxonomy}][query_order_by]" value="title" />
<p>
	<label for="{$this->_prefix}sm_{$taxonomy}_use">Show on Sitemap?</label>
	<input type="checkbox" id="{$this->_prefix}sm_{$taxonomy}_use" name="{$this->_options_key}[sitemap_display_settings][{$taxonomy}][use]" value="1" {$checked_use} />
</p>
<p>
	<label for="{$this->_prefix}sm_{$taxonomy}_ord">Section Order: </label>
	<input type="text" id="{$this->_prefix}sm_{$taxonomy}_ord" name="{$this->_options_key}[sitemap_display_settings][{$taxonomy}][order]" value="{$order}" />
</p>
<p>
	<label for="{$this->_prefix}sm_{$taxonomy}_title">Section Title: </label>
	<input type="text" id="{$this->_prefix}sm_{$taxonomy}_title" name="{$this->_options_key}[sitemap_display_settings][{$taxonomy}][title]" value="{$settings['title']}" />
	<span>If set, this title will be shown before the list of {$taxonomy} items.</span>
</p>
<p>
	<label for="{$this->_prefix}sm_{$taxonomy}_link">Section Title Link: </label>
	<input type="text" id="{$this->_prefix}sm_{$taxonomy}_link" name="{$this->_options_key}[sitemap_display_settings][{$taxonomy}][link]" value="{$settings['link']}" />
	<span>If specified, any title entered will be linked to the url set here.</span>
</p>
<p>
	<label for="{$this->_prefix}sm_{$taxonomy}_intro">Section Intro: </label>
	<textarea id="{$this->_prefix}sm_{$taxonomy}_intro" name="{$this->_options_key}[sitemap_display_settings][{$taxonomy}][intro]">{$introduction}</textarea>
	<span>If specified, any title entered will be linked to the url set here.</span>
</p>
HTML;

	}

	protected function get_default_sitemap_search_settings() {
		return array(
			'label_text' => 'Search',
			'placeholder_text' => 'Enter your search...',
			'button_text' => 'Go'
		);
	}

	protected function get_default_sitemap_post_type_settings( $post_type, $order ) {
		return array(
			'use' => 1,
			'order' => $order,
			'type' => 'post',
			'name' => $post_type,
			'title' => '',
			'link' => '',
			'intro' => '',
			'query_order' => 'ASC',
			'query_order_by' => 'title'
		);
	}

	protected function get_default_sitemap_taxonomy_settings( $taxonomy, $order ) {
		return array(
			'use' => 1,
			'order' => $order,
			'type' => 'taxonomy',
			'name' => $taxonomy,
			'title' => '',
			'link' => '',
			'intro' => '',
			'query_order' => 'ASC',
			'query_order_by' => 'title'
		);
	}
}