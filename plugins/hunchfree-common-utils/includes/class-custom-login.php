<?php
namespace Com\Hunchfree\Plugins\CommonUtils;

defined('\\ABSPATH') or die('No script kiddies please!');

if ( !class_exists('Custom_Login') ) {
	class Custom_Login {

		protected $_options;

		public function __construct($args = array()) {
			$defaults = $this->get_default_args();
			if ( !is_array($args) || 0 == count($args) ) {
				$args = $defaults;
			} else {
				foreach ( $defaults as $k => $v ) {
					if ( !array_key_exists( "{$k}", $args ) || empty($args["{$k}"]) ) {
						$args["{$k}"] = $v;
					}
				}
			}
			$this->_options = $args;
		}

		protected function get_default_args() {
			$plugin_dir = plugins_url('includes/images/', dirname(__FILE__));
			return array(
				'login_head_title' => get_option('blogname'),
				'login_head_link_url' => home_url(),
				'login_image_url' => "{$plugin_dir}hunchfree_logo.png",
				'login_image_h' => '99',
				'login_image_w' => '300'
			);
		}

		public function do_alter_login_head() {
			$image_url = $this->_options['login_image_url'];
			if ( !empty($image_url) ) {
				$image_height = ( array_key_exists('login_image_h', $this->_options) && !empty($this->_options['login_image_h']) )? "{$this->_options['login_image_h']}px" : 'auto';
				$image_width = ( array_key_exists('login_image_w', $this->_options) && !empty($this->_options['login_image_w']) )? "{$this->_options['login_image_w']}px" : 'auto';

				echo <<<HTML
<style>
body.login #login h1 a {
	background: url('{$image_url}') no-repeat scroll center top transparent;
	width: {$image_width};
	height: {$image_height};
	max-width: 100%;
	margin: 0 auto;
}
</style>

HTML;

			}
		}

		public function override_login_header_title($title) {
			if ( array_key_exists('login_head_title', $this->_options) && !empty($this->_options['login_head_title']) ) {
				$title = $this->_options['login_head_title'];
			}
			return $title;
		}

		public function override_login_header_url($url) {
			if ( array_key_exists('login_head_link_url', $this->_options) && !empty($this->_options['login_head_link_url']) ) {
				$url = $this->_options['login_head_link_url'];
			}
			return $url;
		}
	}
}