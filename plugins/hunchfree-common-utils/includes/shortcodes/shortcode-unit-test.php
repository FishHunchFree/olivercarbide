<?php
namespace Com\Hunchfree\Plugins\CommonUtils;

defined('\\ABSPATH') or die('Permission denied - class-theme-base.php');

class Unit_Test {

	protected static $_notices;

	public static function init() {
		add_shortcode('unit_test',array( __CLASS__, 'do_process_short_code') );
	}

	public static function do_process_short_code( $attributes, $content = null ) {
		$a = shortcode_atts( array(
			'sections' => 'all'
		), $attributes );

		$output = '';
		self::$_notices = array();

		if ( 'all' == $a['sections'] || empty($a['sections']) ) {
			$sections = array(
				'markup',
				'images'
			);
		} else if ( false !== strpos($a['sections'], ',') ) {
			$sections = implode(',', $a['sections']);
		} else {
			$sections = array( $a['sections'] );
		}

		if ( in_array('markup', $sections) ) {
			$output .= self::get_markup();
		}
		if ( in_array('images', $sections) ) {
			$output .= self::get_images();
		}
		if ( empty($output) ) {
			self::$_notices[] = "No valid sections found. Please use 'all' (show all), 'markup' (show tag markup), or 'images' (show images).";
			self::$_notices[] = "You can also combine multiple sections with commas.";
		}
		if ( 0 < count( self::$_notices ) ) {
			$output .= "\r\n<!--\r\n" . implode("\r\n", self::$_notices) . "\r\n-->\r\n";
		}
		return $output;
	}

	protected static function get_markup() {

		return <<<HTML
<hr />
<h1>Unit Test Markup Review</h1>
<p>On this page, you will see the general styling for a lot of different html tags. Use this page to make sure that things look the way you want them to.</p>
<p><strong>Headings</strong></p>
<h1>Header one &lt;h1&gt;</h1>
<h2>Header two &lt;h2&gt;</h2>
<h3>Header three &lt;h3&gt;</h3>
<h4>Header four &lt;h4&gt;</h4>
<h5>Header five &lt;h5&gt;</h5>
<h6>Header six &lt;h6&gt;</h6>
<h2>Hyperlinks</h2>
<p>A single (non functional) <a href="#">link &lt;a&gt;</a> within a paragraph.</p>
<h2>Blockquotes</h2>
<p>Single line blockquote &lt;blockquote&gt;:</p>
<blockquote><p>Stay hungry. Stay foolish.</p></blockquote>
<p>Multi line blockquote with a cite reference:</p>
<blockquote><p>People think focus means saying yes to the thing you&#8217;ve got to focus on. But that&#8217;s not what it means at all. It means saying no to the hundred other good ideas that there are. You have to pick carefully. I&#8217;m actually as proud of the things we haven&#8217;t done as the things I have done. Innovation is saying no to 1,000 things. <cite>Steve Jobs &#8211; Apple Worldwide Developers&#8217; Conference, 1997</cite></p></blockquote>
<h2>Tables &lt;table&gt;, &lt;thead&gt;, &lt;tfoot&gt;, &lt;tbody&gt;, &lt;tr&gt;, &lt;th&gt;, and &lt;hd&gt;</h2>
<table>
<tbody>
<tr>
<th>Employee &lt;th&gt;</th>
<th class="views">Salary</th>
<th></th>
</tr>
<tr class="odd">
<td><a href="http://example.com/">Jane</a></td>
<td>$1</td>
<td>Because that&#8217;s all Steve Job&#8217; needed for a salary.</td>
</tr>
<tr class="even">
<td><a href="http://example.com">John</a></td>
<td>$100K</td>
<td>For all the blogging he does.</td>
</tr>
<tr class="odd">
<td><a href="http://example.com/">Jane</a></td>
<td>$100M</td>
<td>Pictures are worth a thousand words, right? So Tom x 1,000.</td>
</tr>
<tr class="even">
<td><a href="http://example.com/">Jane</a></td>
<td>$100B</td>
<td>With hair like that?! Enough said&#8230;</td>
</tr>
</tbody>
</table>
<h2>Definition Lists &lt;dl&gt;, &lt;dt&gt;, and &lt;dd&gt;</h2>
<dl>
<dt>Definition List Title &lt;dt&gt; in &lt;dd&gt;</dt>
<dd>Definition list division. &lt;dd&gt; in &lt;dd&gt;</dd>
<dt>Startup</dt>
<dd>A startup company or startup is a company or temporary organization designed to search for a repeatable and scalable business model.</dd>
<dt>#dowork</dt>
<dd>Coined by Rob Dyrdek and his personal body guard Christopher &#8220;Big Black&#8221; Boykins, &#8220;Do Work&#8221; works as a self motivator, to motivating your friends.</dd>
<dt>Do It Live</dt>
<dd>I&#8217;ll let Bill O&#8217;Reilly will <a title="We'll Do It Live" href="https://www.youtube.com/watch?v=O_HyZ5aW76c">explain</a> this one.</dd>
</dl>
<h2>Unordered Lists (Nested) &lt;ul&gt;</h2>
<ul>
	<li>List item one
		<ul>
			<li>List item one
				<ul>
					<li>List item one</li>
					<li>List item two</li>
					<li>List item three</li>
					<li>List item four</li>
				</ul>
			</li>
			<li>List item two</li>
			<li>List item three</li>
			<li>List item four</li>
		</ul>
	</li>
	<li>List item two</li>
	<li>List item three</li>
	<li>List item four</li>
</ul>
<h2>Ordered List (Nested) &lt;ol&gt;</h2>
<ol>
	<li>List item one
		<ol>
			<li>List item one
				<ol>
					<li>List item one</li>
					<li>List item two</li>
					<li>List item three</li>
					<li>List item four</li>
				</ol>
			</li>
			<li>List item two</li>
			<li>List item three</li>
			<li>List item four</li>
		</ol>
	</li>
	<li>List item two</li>
	<li>List item three</li>
	<li>List item four</li>
</ol>
<h2>HTML Tags</h2>
<p>These supported tags come from the WordPress.com code <a title="Code" href="http://en.support.wordpress.com/code/">FAQ</a>.</p>
<p><strong>Address Tag</strong> &lt;address&gt;</p>
<address>1 Infinite Loop<br />
Cupertino, CA 95014<br />
United States</address>
<p><strong>Anchor Tag (aka. Link)</strong> &lt;a&gt;</p>
<p>This is an example of a <a title="Apple" href="http://apple.com">link</a>.</p>
<p><strong>Abbreviation Tag</strong> &lt;abbr&gt;</p>
<p>The abbreviation <abbr title="Seriously">srsly</abbr> stands for &#8220;seriously&#8221;.</p>
<p><strong>Acronym Tag</strong> &lt;acronym&gt;</p>
<p>The acronym <acronym title="For The Win">ftw</acronym> stands for &#8220;for the win&#8221;.</p>
<p><strong>Big Tag</strong> &lt;big&gt;</p>
<p>These tests are a <big>big</big> deal, but this tag is no longer supported in HTML5.</p>
<p><strong>Cite Tag</strong> &lt;cite&gt;</p>
<p>&#8220;Code is poetry.&#8221; &#8212;<cite>Automattic</cite></p>
<p><strong>Code Tag</strong> &lt;code&gt;</p>
<p>You will learn later on in these tests that <code>word-wrap: break-word;</code> will be your best friend.</p>
<p><strong>Delete Tag</strong> &lt;del&gt;</p>
<p>This tag will let you <del>strikeout text</del>, but this tag is no longer supported in HTML5 (use the <code>&lt;strike&gt;</code> instead).</p>
<p><strong>Emphasize Tag</strong> &lt;em&gt;</p>
<p>The emphasize tag should <em>italicize</em> text.</p>
<p><strong>Insert Tag</strong> &lt;ins&gt;</p>
<p>This tag should denote <ins>inserted</ins> text.</p>
<p><strong>Keyboard Tag</strong> &lt;kbd&gt;</p>
<p>This scarcely known tag emulates <kbd>keyboard text</kbd>, which is usually styled like the <code>&lt;code&gt;</code> tag.</p>
<p><strong>Preformatted Tag</strong> &lt;pre&gt;</p>
<p>This tag styles large blocks of code.</p>
<pre>.post-title {
	margin: 0 0 5px;
	font-weight: bold;
	font-size: 38px;
	line-height: 1.2;
}</pre>
<p><strong>Quote Tag</strong> &lt;q&gt;</p>
<p><q>Developers, developers, developers&#8230;</q> &#8211;Steve Ballmer</p>
<p><strong>Strong Tag</strong> &lt;strong&gt;</p>
<p>This tag shows <strong>bold<strong> text.</strong></strong></p>
<p><strong>Subscript Tag</strong> &lt;sub&gt;</p>
<p>Getting our science styling on with H<sub>2</sub>O, which should push the &#8220;2&#8221; down.</p>
<p><strong>Superscript Tag</strong> &lt;sup&gt;</p>
<p>Still sticking with science and Isaac Newton&#8217;s E = MC<sup>2</sup>, which should lift the 2 up.</p>
<p><strong>Teletype Tag</strong> &lt;tt&gt;</p>
<p>This rarely used tag emulates <tt>teletype text</tt>, which is usually styled like the <code>&lt;code&gt;</code> tag.</p>
<p><strong>Variable Tag</strong> &lt;var&gt;</p>
<p>This allows you to denote <var>variables</var>.</p>
HTML;

	}

	protected static function get_images() {
		$image_url = plugins_url('/hunchfree-common-utils/includes/images/');
		return <<<HTML
<hr />
<h1>Images Unit Test</h1>
<p>Welcome to image alignment! The best way to demonstrate the ebb and flow of the various image positioning options is to nestle them snuggly among an ocean of words. Grab a paddle and let&#8217;s get started.</p>
<p>On the topic of alignment, it should be noted that users can choose from the options of <em>None</em>, <em>Left</em>, <em>Right, </em>and <em>Center</em>. In addition, they also get the options of <em>Thumbnail</em>, <em>Medium</em>, <em>Large</em> &amp; <em>Fullsize</em>.</p>
<p style="text-align:center;"><img class="size-full wp-image-906 aligncenter" title="Image Alignment 580x300" alt="Image Alignment 580x300" src="{$image_url}image-alignment-580x300.jpg" width="580" height="300" /></p>
<p>The image above happens to be <em><strong>centered</strong></em>.</p>
<p><strong><img class="size-full wp-image-904 alignleft" title="Image Alignment 150x150" alt="Image Alignment 150x150" src="{$image_url}image-alignment-150x150.jpg" width="150" height="150" /></strong>The rest of this paragraph is filler for the sake of seeing the text wrap around the 150&#215;150 image, which is <em><strong>left aligned</strong></em>. <strong></strong></p>
<p>As you can see the should be some space above, below, and to the right of the image. The text should not be creeping on the image. Creeping is just not right. Images need breathing room too. Let them speak like you words. Let them do their jobs without any hassle from the text. In about one more sentence here, we&#8217;ll see that the text moves from the right of the image down below the image in seamless transition. Again, letting the do it&#8217;s thang. Mission accomplished!</p>
<p>And now for a <em><strong>massively large image</strong></em>. It also has <em><strong>no alignment</strong></em>.</p>
<p><img class="alignnone  wp-image-907" title="Image Alignment 1200x400" alt="Image Alignment 1200x400" src="{$image_url}image-alignment-1200x4002.jpg" width="1200" height="400" /></p>
<p>The image above, though 1200px wide, should not overflow the content area. It should remain contained with no visible disruption to the flow of content.</p>
<p><img class="size-full wp-image-905 alignright" title="Image Alignment 300x200" alt="Image Alignment 300x200" src="{$image_url}image-alignment-300x200.jpg" width="300" height="200" /></p>
<p>And now we&#8217;re going to shift things to the <em><strong>right align</strong></em>. Again, there should be plenty of room above, below, and to the left of the image. Just look at him there&#8230; Hey guy! Way to rock that right side. I don&#8217;t care what the left aligned image says, you look great. Don&#8217;t let anyone else tell you differently.</p>
<p>In just a bit here, you should see the text start to wrap below the right aligned image and settle in nicely. There should still be plenty of room and everything should be sitting pretty. Yeah&#8230; Just like that. It never felt so good to be right.</p>
<p>And just when you thought we were done, we&#8217;re going to do them all over again with captions!</p>
<div id="attachment_906" style="width: 590px" class="wp-caption aligncenter"><img class="size-full wp-image-906  " title="Image Alignment 580x300" alt="Image Alignment 580x300" src="{$image_url}image-alignment-580x300.jpg" width="580" height="300" /><p class="wp-caption-text">Look at 580&#215;300 getting some <a title="Image Settings" href="http://en.support.wordpress.com/images/image-settings/">caption</a> love.</p></div>
<p>The image above happens to be <em><strong>centered</strong></em>. The caption also has a link in it, just to see if it does anything funky.</p>
<div id="attachment_904" style="width: 160px" class="wp-caption alignleft"><img class="size-full wp-image-904  " title="Image Alignment 150x150" alt="Image Alignment 150x150" src="{$image_url}image-alignment-150x150.jpg" width="150" height="150" /><p class="wp-caption-text">Itty-bitty caption.</p></div>
<p><strong></strong>The rest of this paragraph is filler for the sake of seeing the text wrap around the 150&#215;150 image, which is <em><strong>left aligned</strong></em>. <strong></strong></p>
<p>As you can see the should be some space above, below, and to the right of the image. The text should not be creeping on the image. Creeping is just not right. Images need breathing room too. Let them speak like you words. Let them do their jobs without any hassle from the text. In about one more sentence here, we&#8217;ll see that the text moves from the right of the image down below the image in seamless transition. Again, letting the do it&#8217;s thang. Mission accomplished!</p>
<p>And now for a <em><strong>massively large image</strong></em>. It also has <em><strong>no alignment</strong></em>.</p>
<div id="attachment_907" style="width: 1210px" class="wp-caption alignnone"><img class=" wp-image-907" title="Image Alignment 1200x400" alt="Image Alignment 1200x400" src="{$image_url}image-alignment-1200x4002.jpg" width="1200" height="400" /><p class="wp-caption-text">Massive image comment for your eyeballs.</p></div>
<p>The image above, though 1200px wide, should not overflow the content area. It should remain contained with no visible disruption to the flow of content.</p>
<div id="attachment_905" style="width: 310px" class="wp-caption alignright"><img class="size-full wp-image-905 " title="Image Alignment 300x200" alt="Image Alignment 300x200" src="{$image_url}image-alignment-300x200.jpg" width="300" height="200" /><p class="wp-caption-text">Feels good to be right all the time.</p></div>
<p>And now we&#8217;re going to shift things to the <em><strong>right align</strong></em>. Again, there should be plenty of room above, below, and to the left of the image. Just look at him there&#8230; Hey guy! Way to rock that right side. I don&#8217;t care what the left aligned image says, you look great. Don&#8217;t let anyone else tell you differently.</p>
<p>In just a bit here, you should see the text start to wrap below the right aligned image and settle in nicely. There should still be plenty of room and everything should be sitting pretty. Yeah&#8230; Just like that. It never felt so good to be right.</p>
<p>And that&#8217;s a wrap, yo! You survived the tumultuous waters of alignment. Image alignment achievement unlocked!</p>
HTML;

	}
}
