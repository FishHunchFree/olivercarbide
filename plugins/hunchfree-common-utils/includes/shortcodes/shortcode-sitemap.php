<?php
namespace Com\Hunchfree\Plugins\CommonUtils;

defined('\\ABSPATH') or die('Permission denied - class-theme-base.php');

abstract class Short_Code {
	protected $_notices;
	protected $_output = '';

	public function __construct( $code_name = '' ) {
		$this->_notices = array();
		add_shortcode( "{$code_name}", array( $this, 'do_process_short_code') );
	}

	abstract public function do_process_short_code( $attributes, $content = null );

	protected function reset() {
		$this->_notices = array();
		$this->_output = '';
	}

	protected function add_output($output = '') {
		$this->_output .= $output;
	}

	protected function get_output() {
		return $this->_output;
	}

	protected function add_content( $content = null ) {
		if ( !is_null( $content ) ) {
			$this->add_output( $content );
		}
		return $this;
	}

	protected function add_notices() {
		if ( 0 < count($this->_notices) ) {
			$this->add_output( "\r\n<!-- " . print_r( $this->_notices, true ) . "\r\n-->\r\n" );
		}
		return $this;
	}
}

class Site_Map extends Short_Code {

	protected static $_cache_refresh_minutes = 0;
	protected static $_search_placement = 0;
	protected static $_search_settings;
	protected static $_display_settings;

	protected static $_prefix = '';

	protected $_item_listing;

	protected $_post_info;
	protected $_post_parent_children_ids;		# array of parent ids containing array of child ids

	protected $_term_info;
	protected $_term_parent_children_ids;

	const SEARCH_NONE = 0;
	const SEARCH_ABOVE = 1;
	const SEARCH_BELOW = 2;

	public function __construct(
		$code_name = 'sitemap', $prefix, $cache_refresh_minutes, $search_placement, $search_settings, $display_settings
	) {
		self::$_prefix = $prefix;

		parent::__construct( $code_name );


		if ( 0 < (int)$cache_refresh_minutes ) {
			self::$_cache_refresh_minutes = $cache_refresh_minutes;
		}

		switch ( (int)$search_placement ) {
			case 0: # Do not show
				break;
			case 1: # Above sitemap
			case 2: # Below sitemap
				self::$_search_placement = (int)$search_placement;
				$defaults = $this->get_default_search_settings();
				foreach ( $defaults as $k => $v ) {
					if ( ! array_key_exists($k, $search_settings) ) {
						$search_settings[$k] = $v;
					}
				}
				self::$_search_settings = $search_settings;
				break;
			default:
				# Do not show - the default
				break;
		}

		if ( is_array($display_settings) ) {
			self::$_display_settings = $display_settings;
		} else {
			self::$_display_settings = array();
		}
	}

	protected function get_default_search_settings() {
		return array(
			'label_text' => 'Search',
			'placeholder_text' => 'Enter your search terms',
			'button_text' => 'Go'
		);
	}

	protected function reset() {
		parent::reset();
		$this->_item_listing = array();
		$this->_post_info = array();
		$this->_post_parent_children_ids = array();
		$this->_term_info = array();
		$this->_term_parent_children_ids = array();
		$this->_notices = array();
	}

	public function do_process_short_code( $attributes, $content = null ) {
		$a = shortcode_atts( array(
			'post_types' => false,
			'cache_minutes' => false,
			'search_placement' => false,
			'search_label' => false,
			'search_placeholder' => false,
			'search_button' => false
		), $attributes );

		$this->reset();

		$search_placement = $this->get_search_placement( $a['search_placement'] );
		if ( self::SEARCH_NONE != $search_placement ) {
			$label_text = $this->get_default_text_or_override('label_text', $a['search_label']);
			$placeholder_text = $this->get_default_text_or_override('placeholder_text', $a['search_placeholder'] );
			$button_text = $this->get_default_text_or_override('button_text', $a['search_button'] );
		} else {
			$label_text = $placeholder_text = $button_text = '';
		}

		$this->load_sitemap_components( $a['post_types'] );

		if ( 0 < count($this->_item_listing) || ! is_null( $content ) ) {

			$this->add_output('<div class="site_map_wrap">');

			if ( self::SEARCH_ABOVE == $search_placement ) {
				$this->add_search_box( $label_text, $placeholder_text, $button_text );
			}

			$this->add_sitemap_components()->add_content( $content );

			if ( self::SEARCH_BELOW == $search_placement ) {
				$this->add_search_box( $label_text, $placeholder_text, $button_text );
			}
			$this->add_output('</div>');

		}

		$this->add_notices();

		return $this->get_output();
	}

	protected function get_search_placement( $override = false ) {
		$choice = self::$_search_placement;
		if ( false !== $override ) {
			$override = (int)$override;
			switch ( $override ) {
				case self::SEARCH_NONE:
				case self::SEARCH_ABOVE:
				case self::SEARCH_BELOW:
					$choice = (int)$override;
					break;
				default:
					$this->_notices[] = "Unknown search placement: {$override}";
					break;
			}
		}
		return $choice;
	}

	protected function get_default_text_or_override( $field_name, $override ) {
		$choice = self::$_search_settings["{$field_name}"];
		if ( false !== $override ) {
			$choice = $override;
		}
		return $choice;
	}

	protected function load_sitemap_components( $override_item_types = false ) {

		$final_item_types = array();

		$post_type_args = array(
			'public' => true
		);
		$known_post_types = get_post_types( $post_type_args, 'names' );

		$tax_args = array(
			'public' => true
		);
		$known_taxonomies = get_taxonomies( $tax_args, 'names' );

		if ( false !== $override_item_types && is_string( $override_item_types ) ) {
			# User has specified a list of post types to display (comma separated)
			$item_types = array();
			$override_item_types = ltrim(rtrim($override_item_types));
			if ( ! empty( $override_item_types ) ) {
				if ( false === strpos( $override_item_types, ',' ) ) {
					$item_types = explode( ',', $override_item_types );
				} else {
					$item_types = array( $override_item_types );
				}
			}
			if ( 0 < count($item_types) ) {
				foreach ( $item_types as $item_type ) {
					$type_name = ltrim(rtrim($item_type));
					if ( in_array( $type_name, $known_post_types ) ) {
						$final_item_types[] = array(
							'type' => 'post',
							'name' => $type_name,
							'title' => '',
							'query_order' => 'ASC',
							'query_order_by' => 'title',
							'intro' => '',
							'link' => ''
						);
					} else if ( in_array( $type_name, $known_taxonomies ) ) {
						$final_item_types[] = array(
							'type' => 'taxonomy',
							'name' => $type_name,
							'title' => '',
							'query_order' => 'ASC',
							'query_order_by' => 'title',
							'intro' => '',
							'link' => ''
						);
					}
				}
			}
		}
		if ( 0 === count($final_item_types) ) {
			# Need to use the defaults
			$settings = self::$_display_settings;
			if ( is_array( $settings ) && 0 < count($settings ) ) {
				foreach ( $settings as $setting ) {
					if ( array_key_exists('use', $setting) && '1' == "{$setting['use']}" ) {
						if ( array_key_exists( 'type', $setting ) && !empty( $setting['type'] ) ) {
							if ( array_key_exists( 'name', $setting ) && !empty( $setting['name'] ) ) {
								$final_item_types[] = array(
									'type' => $setting['type'],
									'name' => $setting['name'],
									'title' => ( array_key_exists( 'title', $setting ) ) ? $setting['title'] : '',
									'query_order' => ( array_key_exists( 'query_order', $setting ) ) ? $setting['query_order'] : 'ASC',
									'query_order_by' => ( array_key_exists( 'query_order_by', $setting ) ) ? $setting['query_order_by'] : 'title',
									'intro' => ( array_key_exists( 'intro', $setting ) ) ? $setting['intro'] : '',
									'link' => ( array_key_exists( 'link', $setting ) ) ? $setting['link'] : ''
								);
							}
						}
					}
				}
			}
		}

		if ( 0 < count($final_item_types) ) {
			foreach ( $final_item_types as $data ) {
				switch ( $data['type'] ) {
					case 'post':
						$this->load_post_data( $data );
						break;
					case 'taxonomy':
						$this->load_taxonomy_data( $data );
						break;
					default:
						$this->_notices[] = "Unknown item type {$data['type']}";
						break;
				}
			}
		}
	}

	protected function add_search_box( $label_text, $placeholder_text, $button_text ) {

		if ( false !== $button_text && !empty($button_text) ) {
			$button_text = self::$_search_settings['button_text'];
		}
		if ( false !== $placeholder_text ) {
			if ( ! empty( $placeholder_text ) ) {
				$input_field = '<input type="text" name="s" id="search_now" value="" placeholder="' . $placeholder_text . '" />';
			} else {
				$input_field = '<input type="text" name="s" id="search_now" value="" />';
			}
		} else if ( !empty( self::$_search_settings['placeholder_text']) ) {
			$input_field = '<input type="text" name="s" id="search_now" value="" placeholder="' . self::$_search_settings['placeholder_text'] . '" />';
		} else {
			$input_field = '<input type="text" name="s" id="search_now" value="" />';
		}

		if ( false !== $label_text ) {
			if ( !empty( $label_text ) ) {
				$input_field = '<label for="search_now">' . $label_text . '</label>' . $input_field;
			}
		} else {
			if ( !empty( self::$_search_settings['label_text'] ) ) {
				$input_field = '<label for="search_now">' . self::$_search_settings['label_text'] . '</label>' . $input_field;
			}
		}

		$out = <<<HTML
<div class="sitemap-search">
	<form action="/" method="GET" />
		{$input_field}
		<input type="submit" name="sitemap-search-now" id="search_now" value="{$button_text}" />
	</div>
</div>
HTML;

		$this->add_output( $out );

		return $this;

	}

	protected function add_sitemap_components() {

		if ( is_array( $this->_item_listing ) && 0 < count( $this->_item_listing ) ) {
			foreach ( $this->_item_listing as $section ) {
				if ( array_key_exists('type', $section ) ) {
					switch ( "{$section['type']}" ) {
						case 'section_head':
							$title = ( array_key_exists('title', $section) )? $section['title'] : '';
							$intro = ( array_key_exists('intro', $section) )? $section['intro'] : '';
							$link = ( array_key_exists('link', $section) )? $section['link'] : '';
							# $this->_notices[] = "Doing head for:" . print_r($section, true);
							$this->add_section_header( $title, $intro, $link );
							break;
						case 'post_list':
							$post_type = ( array_key_exists('post_type', $section) )? $section['post_type'] : '';
							$id_list = ( array_key_exists('item_ids', $section ) )? $section['item_ids'] : '';
							$this->add_section_post_list( $post_type, $id_list );
							break;
						case 'term_list':
							$tax_name = ( array_key_exists('taxonomy', $section ) )? $section['taxonomy'] : '';
							$id_list = ( array_key_exists('item_ids', $section ) )? $section['item_ids'] : '';
							$this->add_section_term_list( $tax_name, $id_list );
							break;
						default:
							$this->_notices[] = "Unsupported section type {$section['type']}. Skipping.";
							break;
					}
				} else {
					$this->_notices[] = "Item listing missing type key. Not drawing.";
				}
			}
		}

		return $this;
	}

	protected function add_section_term_list( $tax_name, $id_list ) {
		if ( empty( $id_list ) ) {
			$this->_notices[] = "Section of terms with empty term list";
		} else if ( ! is_array( $id_list ) ) {
			$this->_notices[] = "Section of terms with non array term list. Expecting array of ids. Received " . gettype( $id_list );
		} else if ( 0 == count($id_list) ) {
			$this->_notices[] = "Section of terms with empty array as term list.";
		} else if ( ! array_key_exists("{$tax_name}", $this->_term_info) ) {
			$this->_notices[] = "Section of terms for {$tax_name}, but taxonomy not found in term_info";
		} else {
			$listings = array();
			foreach ( $id_list as $term_id ) {
				if ( ! array_key_exists( "{$term_id}", $this->_term_info["{$tax_name}"] ) ) {
					$this->_notices[] = "Term {$term_id} missing entry in term_info[{$tax_name}]";
				} else {
					$term_info = $this->_term_info["{$tax_name}"]["{$term_id}"];
					$child_info = $this->get_term_children_listing( $tax_name, $term_id );
					$listings[] = '<a href="' . $term_info['link'] . '">' . $term_info['title'] . '</a>' . $child_info;
				}
			}
			if ( 0 < count($listings) ) {
				$this->add_output( '<ul class="term_list tax_' . $tax_name . '"><li>' . implode('</li><li>', $listings) . '</li></ul>' );
			}
		}
		return $this;
	}

	protected function get_term_children_listing( $tax_name, $term_id ) {
		$out = '';

		if (
			array_key_exists("{$tax_name}", $this->_term_parent_children_ids)
			&& is_array( $this->_term_parent_children_ids["{$tax_name}"] )
			&& 0 < count( $this->_term_parent_children_ids["{$tax_name}"] )
		) {
			if ( array_key_exists("{$term_id}", $this->_term_parent_children_ids["{$tax_name}"] )
				&& is_array( $this->_term_parent_children_ids["{$tax_name}"]["{$term_id}"] )
				&& 0 < count( $this->_term_parent_children_ids["{$tax_name}"]["{$term_id}"] )
			) {
				$out .= '<ul>';
				$children = $this->_term_parent_children_ids["{$tax_name}"]["{$term_id}"];
				foreach ( $children as $child_id ) {
					if ( !array_key_exists( "{$child_id}", $this->_term_info["{$tax_name}"] ) ) {
						$this->_notices[] = "Failed to find child id {$child_id} in term_info[{$tax_name}]";
					} else {
						$child = $this->_term_info["{$tax_name}"]["{$child_id}"];
						$out .= '<li><a href="' . $child['link'] . '">' . $child['title'] . '</a>';
						if ( array_key_exists( $child_id, $this->_term_parent_children_ids["{$tax_name}"] ) ) {
							$out .= $this->get_term_children_listing( $tax_name, $child_id );
						}
						$out .= '</li>';
					}
				}
				$out .= '</ul>';
			}
		}

		return $out;
	}

	protected function add_section_post_list( $post_type, $id_list ) {
		if ( empty( $id_list ) ) {
			$this->_notices[] = "Section of posts with empty post list";
		} else if ( ! is_array( $id_list ) ) {
			$this->_notices[] = "Section of posts with non array post list. Expecting array of ids. Received " . gettype( $id_list );
		} else if ( 0 == count($id_list) ) {
			$this->_notices[] = "Section of posts with empty array as post list.";
		} else {
			$listings = array();
			foreach ( $id_list as $post_id ) {
				if ( ! array_key_exists( "{$post_id}", $this->_post_info ) ) {
					$this->_notices[] = "Post {$post_id} missing entry in post_info";
				} else {
					$post_info = $this->_post_info["{$post_id}"];
					$child_info = $this->get_post_children_listing( $post_id );
					$listings[] = '<a href="' . $post_info['link'] . '">' . $post_info['title'] . '</a>' . $child_info;
				}
			}
			if ( 0 < count($listings) ) {
				$this->add_output( '<ul class="post_list post_' . $post_type . '"><li>' . implode('</li><li>', $listings) . '</li></ul>' );
			}
		}
		return $this;
	}

	protected function get_post_children_listing( $post_id ) {
		$out = '';

		if (
			array_key_exists("{$post_id}", $this->_post_parent_children_ids)
			&& is_array( $this->_post_parent_children_ids["{$post_id}"] )
			&& 0 < count( $this->_post_parent_children_ids["{$post_id}"] )
		) {
			$out .= '<ul>';
			$children = $this->_post_parent_children_ids["{$post_id}"];
			foreach ( $children as $child_id ) {
				if ( ! array_key_exists( "{$child_id}", $this->_post_info ) ) {
					$this->_notices[] = "Failed to find child id {$child_id} in post info";
				} else {
					$child = $this->_post_info["{$child_id}"];
					$out .= '<li><a href="' . $child['link'] . '">' . $child['title'] . '</a>';
					if ( array_key_exists( $child_id, $this->_post_parent_children_ids ) ) {
						$out .= $this->get_post_children_listing( $child_id );
					}
					$out .= '</li>';
				}
			}
			$out .= '</ul>';
		}

		return $out;
	}

	protected function add_section_header( $title, $intro, $link ) {

		if ( empty($title) && empty($intro) ) {
			$this->_notices[] = "Section header without title or intro";
		} else {
			if ( !empty( $title ) ) {
				if ( !empty( $link ) ) {
					$title = '<h2><a href="' . $link .'">' . $title . '</a></h2>';
				} else {
					$title = '<h2>' . $title . '</h2>';
				}
			}
			if ( !empty( $intro ) ) {
				$intro = '<div class="intro">' . $intro . '</div>';
			}
			$out = <<<HTML
<div class="section head">
	{$title}
	{$intro}
</div>
HTML;

			$this->add_output( $out );
		}
		return $this;

	}

	protected function load_post_data( $data ) {
		$query_args = array(
			'post_type' => $data['name'],
			'posts_per_page' => -1,
			'order' => $data['query_order'],
			'orderby' => $data['query_order_by'],
			'meta_query' => array(
				array(
					'key' => '_' . self::$_prefix . 'sitemap_hide',
					'compare' => 'NOT EXISTS'
				)
			)
		);
		$results = new \WP_Query( $query_args );
		if ( $results->have_posts() ) {
			if ( ! empty( $data['title'] ) || !empty( $data['intro'] ) ) {
				$this->_item_listing[] = array(
					'type' => 'section_head',
					'title' => $data['title'],
					'intro' => $data['intro'],
					'link' => array_key_exists('link', $data)? $data['link'] : ''
				);
			}
			$parent_ids = array();
			while ( $results->have_posts() ) {
				$result = $results->next_post();
				$id = (int)$result->ID;
				$parent_id = (int)$result->post_parent;
				$this->_post_info["{$id}"] = array(
					'title' => $result->post_title,
					'link' => get_permalink( $result )
				);
				if ( 0 < $parent_id ) {
					if ( ! array_key_exists("{$parent_id}", $this->_post_parent_children_ids) ) {
						$this->_post_parent_children_ids["{$parent_id}"] = array( $id );
					} else {
						$this->_post_parent_children_ids["{$parent_id}"][] = $id;
					}
				} else {
					$parent_ids[] = $id;
				}
			}
			$this->_item_listing[] = array(
				'type' => 'post_list',
				'post_type' => $data['name'],
				'item_ids' => $parent_ids
			);
		}
		unset( $results );
	}

	protected function load_taxonomy_data( $data ) {
		$order = ( array_key_exists('query_order', $data) )? $data['query_order'] : 'ASC';
		$order_by = ( array_key_exists('query_order_by', $data) )? $data['query_order_by'] : 'name';
		if ( 'title' === "{$order_by}" ) {
			$order_by = 'name';
		}
		$query_args = array(
			'orderby' => $order_by,
			'order' => $order,
			'hide_empty' => true
		);
		$taxonomy_name = $data['name'];
		$results = get_terms( "{$taxonomy_name}", $query_args );
		if ( is_wp_error( $results ) ) {
			$this->_notices[] = "Failed taxonoy query for {$data['name']}";
		} else if ( is_array( $results ) && 0 < count($results) ) {
			# $this->_notices[] = "Found For {$taxonomy_name}:" . print_r($results, true);
			if ( ! empty( $data['title'] ) || !empty( $data['intro'] ) ) {
				$this->_item_listing[] = array(
					'type' => 'section_head',
					'title' => $data['title'],
					'intro' => $data['intro'],
					'link' => array_key_exists('link', $data)? $data['link'] : ''
				);
			}
			$section_parent_ids = array();
			foreach ( $results as $term ) {
				$this->_term_info["{$taxonomy_name}"]["{$term->term_id}"] = array(
					'title' => $term->name,
					'link' => get_term_link( $term )
				);
				$parent_id = (int)$term->parent;
				if ( 0 < $parent_id ) {
					if ( ! array_key_exists( $taxonomy_name, $this->_term_parent_children_ids ) ) {
						$this->_term_parent_children_ids["{$taxonomy_name}"] = array();
					}
					if ( ! array_key_exists("{$parent_id}", $this->_term_parent_children_ids["{$taxonomy_name}"]) ) {
						$this->_term_parent_children_ids["{$taxonomy_name}"]["{$parent_id}"] = array( (int)$term->term_id );
					} else {
						$this->_term_parent_children_ids["{$taxonomy_name}"]["{$parent_id}"][] = (int)$term->term_id;
					}
				} else {
					$section_parent_ids[] = $term->term_id;
				}
			}
			$this->_item_listing[] = array(
				'type' => 'term_list',
				'taxonomy' => $taxonomy_name,
				'item_ids' => $section_parent_ids
			);
		}
		unset($results);
	}
}
