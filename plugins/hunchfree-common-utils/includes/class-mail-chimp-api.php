<?php
namespace Com\Hunchfree\Plugins\CommonUtils;

defined('\\ABSPATH') or die('No script kiddies please!');

if ( !class_exists('Custom_Login') ) {
	class Mail_Chimp_Api {

		protected $_api_key = null;
		protected $_endpoint = null;

		protected $_errors;

		public function __construct($api_key) {
			$this->_errors = array();

			if ( !empty( $api_key ) ) {
				$this->_api_key = $api_key;

				$this->setup_endpoint();
			} else {
				$this->_errors[] = "Missing api key";
			}
		}

		protected function setup_endpoint() {
			if ( !empty( $this->_api_key ) ) {

				$data_center = 'us1';
				$dash_position = strpos( $this->_api_key, '-' );
				if ( false !== $dash_position ) {
					$data_center = substr( $this->_api_key, $dash_position + 1 );
				}
				$this->_endpoint = "https://{$data_center}.api.mailchimp.com/3.0/";
			}
		}

		public function get_errors() {
			return $this->_errors;
		}

		protected function process_request(
			$request_endpoint = '',
			$request_type = 'GET',
			$fields = array()
		) {
			$response = null;
			try {
				$valid_request_types = array( 'GET', 'POST', 'PATCH', 'DELETE', 'PUT' );
				$request_type = strtoupper( $request_type );
				if ( !in_array( $request_type, $valid_request_types ) ) {
					throw new \Exception( "Parameter 2 : Unknown request type [{$request_type}] : Should be one of " . implode( '|', $valid_request_types ), 10001 );
				} else {
					if ( !is_array( $fields ) ) {
						$this->_errors[] = "Fields not an array. Setting to empty array.";
						$fields = array();
					}
					$ch = curl_init();
					if ( $ch ) {
						$curl_headers = array();
						$use_post_fields = false;
						$require_post_fields = false;
						switch ( "{$request_type}" ) {
							case 'GET':
								curl_setopt( $ch, CURLOPT_HTTPGET, true );
								break;
							case 'POST':
								curl_setopt( $ch, CURLOPT_POST, true );
								$use_post_fields = true;
								$require_post_fields = true;
								break;
							case 'PATCH':
								# could use curl_setopt($ch, CURL_CUSTOMREQUEST, 'PATCH')
								# but some servers don't recognize patch so we use the method override header
								curl_setopt( $ch, CURLOPT_POST, true );
								$use_post_fields = true;
								$curl_headers[] = 'X-HTTP-Method-Override: PATCH';
								$require_post_fields = true;
								break;
							case 'DELETE':
								curl_setopt( $ch, CURLOPT_POST, true );
								$use_post_fields = true;
								$curl_headers[] = 'X-HTTP-Method-Override: DELETE';
								break;
							case 'PUT':
								curl_setopt( $ch, CURLOPT_POST, true );
								$use_post_fields = true;
								$curl_headers[] = 'X-HTTP-Method-Override: PUT';
								$require_post_fields = true;
								break;
							default:
								# This simply cannot happen. We prevented it above. Throwing an exception anywise
								unset( $ch );
								throw new \Exception( "Bad Request Type [{$request_type}] : Should be one of " . implode( '|', $valid_request_types ), 10001 );
								break;
						}
						if ( !empty( $this->_trigger_error ) ) {
							$curl_headers[] = "X-Trigger-Error: " . $this->_trigger_error;
						}
						if ( !$use_post_fields ) {
							# Need to tack our content onto the url
							$get_payload = ( 0 < count( $fields ) ) ? '?' . http_build_query( $fields ) : '';
							# request endpoint will look like lists/<list_id>/members/
							$full_request_uri = "{$this->_endpoint}{$request_endpoint}{$get_payload}";
							curl_setopt( $ch, CURLOPT_URL, $full_request_uri );
						} else {
							if ( $require_post_fields && 0 == count( $fields ) ) {
								unset( $ch );
								throw new \Exception( "{$request_type} requires at least one field. Parameter 3 : Fields : Empty array", 10001 );
							} else {
								# Set up items for post and add payload to post fields
								$curl_headers = array(
									'Content-Type: application/json'
								);
								$full_request_uri = "{$this->_endpoint}{$request_endpoint}";
								curl_setopt( $ch, CURLOPT_URL, $full_request_uri );
								if ( 0 < count( $fields ) ) {
									$json_data = json_encode( $fields );
									curl_setopt( $ch, CURLOPT_POSTFIELDS, $json_data );
								}
							}
						}
						curl_setopt( $ch, CURLOPT_HTTPHEADER, $curl_headers );
						curl_setopt( $ch, CURLOPT_USERAGENT, 'PHP-SMO/2.0' );
						curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
						curl_setopt( $ch, CURLOPT_TIMEOUT, 10 );
						curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
						curl_setopt( $ch, CURLOPT_USERPWD, "user:" . $this->_api_key );

						$curl_response = curl_exec( $ch );

						$http_code = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
						$http_code_type = substr( $http_code, 0, 1 );
						switch ( "{$http_code_type}" ) {
							case '1':
								# We should never receive this from Mailchimp
								# See See http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.1
								curl_close( $ch );
								$this->_errors[] = "HTTP 1xx Code {$http_code} received - This should not happen";
								$this->_errors[] = ". Request:" . $full_request_uri;
								$this->_errors[] = ". Method:" . $request_type;
								$this->_errors[] = ". Headers:" . print_r( $curl_headers, true );
								$this->_errors[] = ". Response:" . print_r( $curl_response, true );
								throw new \Exception( "Unexpected 1xx HTTP Status Code: {$http_code}. Use get_errors() for details.", 10001 );
								break;
							case '2':
								# OK status code
								# See http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.2
								if ( 'PATCH' == "{$request_type}" || 'DELETE' == "{$request_type}" ) {
									$response = true;
								} else {
									$response = json_decode( $curl_response );
								}
								break;
							case '3':
								# Redirect status code (very unexpected)
								# See http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.3
								curl_close( $ch );
								$error = json_decode( $curl_response );
								$this->_errors[] = "Redirect Response Received HTTP {$http_code} - This should not happen";
								$this->_errors[] = ". Request:" . $full_request_uri;
								$this->_errors[] = ". Method:" . $request_type;
								$this->_errors[] = ". Headers:" . print_r( $curl_headers, true );
								$this->_errors[] = ". Response:" . print_r( $error, true );
								throw new \Exception( "Unexpected 3xx HTTP Status Code: {$http_code}. Use get_errors() for details", 10001 );
								break;
							case '4':
								if ( 'GET' == "{$request_type}" && '404' == "{$http_code}" ) {
									# Requested item not found
									$response = null;
									# $this->_warnings[] = "404: " . $curl_response;
								} else {
									# Bad Request 4xx response : there is something wrong with our request
									# See http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.4
									# See http://kb.mailchimp.com/api/article/api-3-overview#Error-Handling
									# See http://kb.mailchimp.com/api/error-docs/#Error_Docs
									$information = json_decode( $curl_response );
									$this->_errors[] = "There appears to be a problem with the request made";
									$this->handle_mc_error( $information );
									$this->_errors[] = ". Request:" . $full_request_uri;
									$this->_errors[] = ". Method:" . $request_type;
									$this->_errors[] = ". Headers:" . print_r( $curl_headers, true );
									$this->_errors[] = ". Response:" . $curl_response;
									$response = null;
								}
								break;
							case '5':
								# Server error 5xx response : there is something wrong with the MC service currently
								# Tell user to check MC twitter channel and contact support if no known outage
								$information = json_decode( $response );
								$this->_errors[] = "There appears to be a problem with the Mail Chimp Service Currently";
								$this->_errors[] = "Check @MailChimpStatus on Twitter to see if they know about the issue. If not, contact support.";
								$this->handle_mc_error( $information );
								$this->_errors[] = ". Request:" . $full_request_uri;
								$this->_errors[] = ". Method:" . $request_type;
								$this->_errors[] = ". Headers:" . print_r( $curl_headers, true );
								$this->_errors[] = ". Response:" . $curl_response;
								$response = null;
								break;
							default:
								# Something really strange is going on
								curl_close( $ch );
								if ( $curl_response ) {
									$error = json_decode( $curl_response );
								} else {
									$error = 'Empty response received';
								}
								$this->_errors[] = "Unrecognized HTTP Status Code Received {$http_code}";
								$this->_errors[] = ". Request:" . $full_request_uri;
								$this->_errors[] = ". Method:" . $request_type;
								$this->_errors[] = ". Headers:" . print_r( $curl_headers, true );
								$this->_errors[] = ". Response:" . print_r( $error, true );
								throw new \Exception( "Unexpected HTTP Status Code: {$http_code}. Use get_errors() for details", 10001 );
								break;
						}
						curl_close( $ch );
					}
				}
			} catch ( \Exception $e ) {
				$response = null;
			}
			return $response;
		}

		protected function handle_mc_error($error) {
			$error_type = ( $error->type ) ? $error->type : 'Unknown';
			$error_title = ( $error->title ) ? $error->title : 'Unknown';
			$error_status = ( $error->status ) ? $error->status : 'Unknown';
			$error_detail = ( $error->detail ) ? $error->detail : 'Unknown';

			$this->_errors[] = ". Type: {$error_type} : {$error_title} : Status: {$error_status}";
			$this->_errors[] = ". MC Details: " . $error_detail;

		}

		# member id is md5 hash of lowercase version of email address
		public function list_get_member_by_id( $list_id = '', $member_id = '' ) {
			return $this->process_request( "lists/{$list_id}/members/{$member_id}", 'GET', array() );
		}

		public function list_get_member_by_email( $list_id = '', $member_email = '' ) {
			$member_id = md5( strtolower($member_email) );
			return $this->list_get_member_by_id( $list_id, $member_id );
		}

		public function list_member_add(
			$list_id, $email_address, $double_opt_in = true,
			$email_type = null, $merge_fields = null, $interests = null,
			$ip_signup = null, $timestamp_signup = null, $is_vip = null,
			$location = null, $last_note = null
		) {
			$added = false;
			$abort = false;

			$member = $this->list_get_member_by_email($list_id, $email_address);
			if ( is_object( $member ) ) {
				if ( property_exists($member, 'properties') ) {
					$member_data = $member->properties;
					if ( property_exists( $member_data, 'status' ) ) {
						$current_status = $member_data->status;
						$ignore_statuses = array('subscribed','cleaned','pending');
						if ( ! in_array( $current_status, $ignore_statuses ) ) {
							$this->list_member_update_by_email($list_id, $email_address, array('status' => 'subscribed'));
						} else {
							$added = true;
							$abort = true;
						}
					} else {
						$info = array(
							'Mail Chimp Member Missing properties->status',
							json_encode($member)
						);
						throw new \Exception('Bad Object' . print_r($info, true), 10001);
					}
				} else if ( property_exists( $member, 'status' ) ) {
					$current_status = $member->status;
					$ignore_statuses = array('subscribed','cleaned','pending');
					if ( ! in_array( $current_status, $ignore_statuses ) ) {
						$this->list_member_update_by_email($list_id, $email_address, array('status' => 'subscribed'));
					} else {
						$added = true;
						$abort = true;
					}
				} else {
					$info = array(
						'Mail Chimp Member Missing ->properties',
						json_encode($member)
					);
					throw new \Exception('Bad Object' . print_r($info, true), 10001);
				}
			}
			if ( false === $abort ) {
				$data = array(
					'email_address' => "{$email_address}",
					'list_id' => "{$list_id}"
				);
				$data['status'] = ( $double_opt_in ) ? 'pending' : 'subscribed';
				if ( ! is_null( $email_type ) && ! empty( $email_type ) ) {
					$allowed_types = $this->get_email_types();
					if ( array_key_exists( $email_type, $allowed_types ) ) {
						$data['email_type'] = $email_type;
					} else {
						$this->_errors[] = "Unknown email type {$email_type} for subscriber. Should be one of [" . implode( '|', array_keys($allowed_types) ) . "]";
					}
				}
				if ( is_array( $merge_fields ) && 0 < count( $merge_fields ) ) {
					$data['merge_fields'] = $merge_fields;
				}
				if ( is_array( $interests ) && 0 < count( $interests ) ) {
					$data['interests'] = $interests;
				}
				if ( ! is_null( $ip_signup ) && ! empty( $ip_signup ) ) {
					$data['ip_signup'] = $ip_signup;
				}
				if ( ! is_null( $timestamp_signup ) && ! empty( $timestamp_signup ) ) {
					$data['timestamp_signup'] = $timestamp_signup;
				}
				if ( is_bool( $is_vip ) ) {
					$data['vip'] = $is_vip;
				}
				if ( is_array( $location ) && 0 < count( $location ) ) {
					$data['location'] = $location;
				}
				if ( is_array( $last_note ) && 0 < count( $last_note ) ) {
					$data['last_note'] = $last_note;
				}
				$result = $this->process_request( "lists/{$list_id}/members/", 'POST', $data );
				if ( is_object( $result ) ) {
					$added = ( $result->id )? true : false;
					/*
					array(
						id,
						email_address,
						unique_email_id,
						email_type,
						status,
						merge_fields => array( field_name => field_value ),
						stats => array( avg_open_rate, avg_click_rate ),
						ip_signup => '',
						timestamp_signup => '',
						ip_opt => '',
						timestamp_opt => '',
						member_rating (int),
						last_changed,
						language,
						vip,
						email_client,
						location => array( latitude, longitude, gmtoff, dstoff, country_code, timezone ),
						list_id 'xxxxxx',
						_links => array(
							array( rel, href, method, *targetSchema, *schema ),
							[ self : this item ]
							[ parent : all members in this members list ]
							[ update : PATCH /lists/f36daccf90/members/b38670b22e834906db9c35f50bbf29cf
							[ delete : DELETE /lists/f36daccf90/members/b38670b22e834906db9c35f50bbf29cf
							[ activity: GET /lists/f36daccf90/members/b38670b22e834906db9c35f50bbf29cf/activity
							[ goals: GET /lists/f36daccf90/members/b38670b22e834906db9c35f50bbf29cf/goals
							[ notes: GET /lists/f36daccf90/members/b38670b22e834906db9c35f50bbf29cf/notes
						)
					*/
				} else {
					$this->_errors[] = "Bad result for list_member_add";
					$this->_errors[] = ". Received: " . print_r( $result, true );
				}
			}
			return $added;

		}

		protected function list_member_update_by_id(
			$list_id, $member_id, $changes = array()
		) {
			$result = null;

			$data = array();

			$status = ( array_key_exists('status', $changes) )? $changes['status'] : null;
			if ( !is_null( $status ) ) {
				$allowed_statuses = $this->get_allowed_statuses();
				if ( array_key_exists($status, $allowed_statuses) ) {
					$data['status'] = $status;
				}
			}

			$email_type = ( array_key_exists('email_type', $changes) )? $changes['email_type'] : null;
			if ( !is_null($email_type) && !empty($email_type) ) {
				$allowed_email_types = $this->get_email_types();
				if ( in_array($email_type, $allowed_email_types) ) {
					$data['email_type'] = $email_type;
				} else {
					$this->_errors[] = "Unknown email type [{$email_type}]. Should be one of (" . implode('|', $allowed_email_types) . ")";
				}
			}

			$merge_fields = ( array_key_exists('merge_fields', $changes) )? $changes['merge_fields'] : null;
			if ( is_array($merge_fields) && 0 < count($merge_fields) ) {
				$data['merge_fields'] = $merge_fields;
			}

			$interests = ( array_key_exists('interests', $changes) )? $changes['interests'] : null;
			if ( is_array( $interests ) && 0 < count( $interests ) ) {
				$data['interests'] = $interests;
			}

			$is_vip = ( array_key_exists('is_vip', $changes) )? $changes['is_vip'] : null;
			if ( is_bool( $is_vip ) ) {
				$data['is_vip'] = $is_vip;
			}

			$location = ( array_key_exists('location', $changes) )? $changes['location'] : null;
			if ( is_array( $location ) && 0 < count( $location ) ) {
				$data['location'] = $location;
			}

			if ( 0 < count($data) ) {
				$result = $this->process_request("lists/{$list_id}/members/{$member_id}", 'PATCH', $data );
			}
			return $result;
		}

		protected function list_member_update_by_email(
			$list_id, $member_email, $changes = array()
		) {
			$member_id = md5(strtolower($member_email) );
			return $this->list_member_update_by_id(
				$list_id, $member_id, $changes
			);
		}
		protected function get_email_types() {
			return array(
				'text' => 'Text Only',
				'html' => 'HTML'
			);
		}

		protected function get_allowed_statuses() {
			return array(
				'subscribed' => 'User currently subscribed to list',
				'unsubscribed' => 'User used to be subscribed to list',
				'cleaned' => 'Removed from list due to bounce',
				'pending' => 'Awaiting confirmation'
			);
		}

	}
}