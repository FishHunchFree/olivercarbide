<?php
namespace Com\Hunchfree\Plugins\CommonUtils;
/*
Plugin Name: Hunchfree Common Utilities
Description: Setup Bugherd, Google Analytics, Theme Unit Testing, Link Suppression, and Split Testing
Version: 1.0.2
Author: Tony Jennings @ HunchFree
Author URI: http://hunchfree.com/
*/
/*
Copyright 2015 HunchFree (email : hunchie@hunchfree.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
*/

defined('\\ABSPATH') or die('No script kiddies please!');

class Common_Utils_Exception extends \Exception {};

class Common_Utils {

	protected $_options_key = 'hf_cu_options';
	protected $_options = null;
	protected $_prefix = 'hf_cu_';
	protected $_base_name = '';
	protected $_plugin_file_path = '';
	protected $_plugin_data = null;

	protected $_mc_notices;

	const ANALYTICS_NO = 0;
	const ANALYTICS_NORMAL = 1;
	const ANALYTICS_TAG_MANAGER = 2;

	public function __construct( $plugin_file_path ) {
		$this->_base_name = basename( __FILE__ );
		$this->_plugin_file_path = $plugin_file_path;
		$this->_mc_notices = array();
	}

	public function do_common_init() {

		$use_list_add = $this->get_option('mailchimp_use', false);

		if ( $use_list_add && class_exists('WPCF7_ContactForm') ) {
			# wpcf7_before_send_mail
			#
			# $this->_mc_notices[] = "Properly detected WPCF7_ContactForm";
			add_action('wpcf7_mail_sent', array( $this, 'add_user_to_mailing_list') );
		} else {
			# $this->_mc_notices[] = "Did not find WPCF7";
		}
		# add_action('wp_footer', array( $this, 'add_notes_to_footer') );
	}

	public function add_notes_to_footer() {
		if ( is_array( $this->_mc_notices ) && 0 < count($this->_mc_notices) ) {
			echo "\r\n<!-- notes\r\n " . print_r($this->_mc_notices, true) . "\r\n-->\r\n";
		} else {
			echo "\r\n<!-- no notes -->\r\n";
		}
	}

	public function init() {
		$this->do_common_init();

		$use_split_testing = $this->get_option('split_test_use', false);
		if ( $use_split_testing ) {
			add_action('wp_head', array($this, 'maybe_add_experiment'), 1 );
		}
		$use_bh = $this->get_option('bugherd_use', false);
		if ( $use_bh ) {
			add_action('wp_footer', array( $this, 'add_bug_herd' ) );
		}
		$use_an = $this->get_option('analytics_use', self::ANALYTICS_NO);
		if ( false === $use_an ) {
			$use_an = self::ANALYTICS_NO;
		} else if ( true === $use_an ) {
			$use_an = self::ANALYTICS_NORMAL;
		}
		if ( self::ANALYTICS_NO !== $use_an ) {
			if ( is_bool( $use_an ) ) {
				$use_an = self::ANALYTICS_NORMAL;
			}
			switch ( "{$use_an}" ) {
				case '1':
					if ( $use_split_testing ) {
						add_action( 'wp_footer', array($this, 'add_analytics') );
					} else {
						add_action( 'wp_head', array($this, 'add_analytics'), 9 );
					}
					break;
				case '2':
					if ( $use_split_testing ) {
						add_action( 'wp_footer', array($this, 'add_analytics_tag_manager') );
					} else {
						add_action( 'wp_head', array($this, 'add_analytics_tag_manager'), 9 );
					}
					break;
				default:
					break;
			}
		}
		#if ( $use_an ) {
		#	if ( $use_split_testing ) {
		#		add_action( 'wp_footer', array($this, 'add_analytics') );
		#	} else {
		#		add_action( 'wp_head', array($this, 'add_analytics'), 9 );
		#	}
		#}
		$use_test = $this->get_option('unit_test_use', false);
		if ( $use_test ) {
			$this->add_unit_test_short_code();
		}
		$use_disable_links = $this->get_option('disable_links_use', false);
		if ( $use_disable_links ) {
			$max_role = $this->get_option('disable_links_maximum_role', '');
			if ( empty($max_role) || !current_user_can("{$max_role}") ) {
				add_action( 'wp_enqueue_scripts', array($this, 'add_jquery') );
				add_action( 'wp_footer', array($this, 'add_disable_links'), 95 );
			}
		}
		$use_custom_login = $this->get_option('login_use', false);
		if ( $use_custom_login ) {
			add_action('after_setup_theme', array( $this, 'maybe_adjust_login') );
		}

		$use_sitemap = $this->get_option('sitemap_use', false);
		if ( $use_sitemap ) {
			$this->add_sitemap_short_code();
		}

	}

	public function maybe_adjust_login() {
		try {
			global $pagenow;
			if ( is_string( $pagenow ) && in_array( $pagenow, array( 'wp-login.php' ) ) ) {
				if ( false === include_once $this->_plugin_file_path . 'includes/class-custom-login.php' ) {
					throw new Common_Utils_Exception( 'Failed to load class Custom_Login', 10001 );
				}
				$all_options = $this->get_options();
				$Custom_Login = new Custom_Login( $all_options );
				if ( is_a( $Custom_Login, __NAMESPACE__ . '\\Custom_Login' ) ) {
					add_action( 'login_head', array( $Custom_Login, 'do_alter_login_head' ) );
					add_filter( 'login_headertitle', array( $Custom_Login, 'override_login_header_title' ) );
					add_filter( 'login_headerurl', array( $Custom_Login, 'override_login_header_url' ) );
				}
			}
		} catch ( \Exception $e ) {
			error_log('Failed to locate ' . $this->_plugin_file_path . 'includes/class-custom-login.php' );
		}
	}

	public function add_jquery() {
		wp_enqueue_script('jquery');
	}

	protected function get_option( $key, $default_value = null ) {
		$current = $this->get_options();
		return array_key_exists( $key, $current )? $current["{$key}"] : $default_value;
	}

	protected function get_options() {
		if ( is_null( $this->_options ) ) {
			$defaults = $this->get_default_options();
			$this->_options = get_option( $this->_options_key, $defaults );
		}
		return $this->_options;
	}

	# Note on sitemap_display_settings:
	# - this should have one array for each post type or taxonomy to display
	# - each containing the following keys:
	# - - type (post|taxonomy)
	# - - name (post_type_name|taxonomy_name)
	# - - title: optional string title to show above the list of items
	# - - intro: optional short description (can use html) to show between title and item list
	# - - query_order: (ASC|DESC)
	# - - query_order_by: (title)
	protected function get_default_options() {

		return array(
			'analytics_use' => 0,
			'analytics_maximum_role' => '',
			'analytics_api_key' => '',
			'analytics_tag_manager_key' => '',
			'analytics_use_on_admin' => 0,

			'bugherd_use' => false,
			'bugherd_minimum_role' => '',
			'bugherd_api_key' => '',
			'bugherd_use_on_admin' => false,

			'unit_test_use' => false,

			'disable_links_use' => false,
			'disable_links_alert' => false,
			'disable_links_maximum_role' => '',

			'split_test_use' => false,
			'split_test_post_types' => array(),

			'login_use' => false,
			'login_head_title' => '',
			'login_head_link_url' => '',
			'login_image_url' => '',
			'login_image_h' => '',
			'login_image_w' => '',

			'mailchimp_use' => false,
			'mailchimp_api_key' => '',
			'mailchimp_list_id' => '',
			'mailchimp_email_field' => '',
			'mailchimp_double_opt_in' => true,
			'mailchimp_contact_form_ids' => array(),
			'mailchimp_extra_fields' => '',
			'mailchimp_merge_tags' => '',

			'sitemap_use' => false,
			'sitemap_cache_minutes' => 0,
			'sitemap_search_placement' => 0,
			'sitemap_search_settings' => array(
				'label_text' => 'Search',
				'placeholder_text' => '',
				'button_text' => 'Go'
			),
			'sitemap_display_settings' => array()
		);
	}

	public function maybe_add_experiment() {
		$allowed_post_types = $this->get_option('split_test_post_types', array());
		if ( is_array($allowed_post_types) && 0 < count($allowed_post_types) ) {
			/** @var \WP_Query $wp_query */
			global $wp_query;
			foreach ( $allowed_post_types as $post_type_name ) {
				if ( is_singular("{$post_type_name}") ) {
					$post_id = $wp_query->get_queried_object_id();
					$experiment = get_post_meta( $post_id, '_' . $this->_prefix . 'experiment_code', true);
					if ( !empty($experiment) ) {
						if ( substr($experiment, 0, 1) == '<' ) {
							echo $experiment;
						} else {
							echo <<<HTML
<!-- Google Analytics Content Experiment code -->
<script>function utmx_section(){}function utmx(){}(function(){var
k='{$experiment}',d=document,l=d.location,c=d.cookie;
if(l.search.indexOf('utm_expid='+k)>0)return;
function f(n){if(c){var i=c.indexOf(n+'=');if(i>-1){var j=c.
indexOf(';',i);return escape(c.substring(i+n.length+1,j<0?c.
length:j))}}}var x=f('__utmx'),xx=f('__utmxx'),h=l.hash;d.write(
'<sc'+'ript src="'+'http'+(l.protocol=='https:'?'s://ssl':
'://www')+'.google-analytics.com/ga_exp.js?'+'utmxkey='+k+
'&utmx='+(x?x:'')+'&utmxx='+(xx?xx:'')+'&utmxtime='+new Date().
valueOf()+(h?'&utmxhash='+escape(h.substr(1)):'')+
'" type="text/javascript" charset="utf-8"><\/sc'+'ript>')})();
</script><script>utmx('url','A/B');</script>
<!-- End of Google Analytics Content Experiment code -->

HTML;

						}
					}
				}
			}
		}
	}

	public function add_disable_links() {
		$use_alert = $this->get_option('disable_links_alert', false);
		if ( $use_alert ) {
			echo <<<HTML

<script>
jQuery(document).ready(function($) {
	function kill_click(e) {
		e.preventDefault();
		alert('This link is not yet ready for use');
	}
	$('a.no_click').on('click', kill_click);
	$('li.no_click a').on('click', kill_click);
});
</script>

HTML;

		} else {
			echo <<<HTML

<script>
jQuery(document).ready(function($) {
	$('a.no_click').on('click', function(e){
		e.preventDefault();
	});
	$('li.no_click a').on('click', function(e){
		e.preventDefault();
	});
});
</script>

HTML;

		}
	}

	public function add_bug_herd() {

		$api_key = $this->get_option('bugherd_api_key', '');
		if ( !empty($api_key) ) {
			$minimum_role = $this->get_option('bugherd_minimum_role', '');
			if ( empty($minimum_role) || current_user_can("{$minimum_role}") ) {
				echo <<<HTML
<script type='text/javascript'>
(function (d, t) {
var bh = d.createElement(t), s = d.getElementsByTagName(t)[0];
bh.type = 'text/javascript';
bh.src = '//www.bugherd.com/sidebarv2.js?apikey={$api_key}';
s.parentNode.insertBefore(bh, s);
})(document, 'script');
</script>
HTML;

			}
		} else {
			echo "\r\n<!-- bugherd api key not set - cannot enable -->\r\n";
		}
	}

	public function add_analytics() {
		$api_key = $this->get_option('analytics_api_key', '');
		if ( !empty($api_key) ) {
			$maximum_role = $this->get_option('analytics_maximum_role', '');
			if ( empty($maximum_role) || !current_user_can("{$maximum_role}") ) {
				/** @noinspection CommaExpressionJS */
				echo <<<HTML
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', '{$api_key}', 'auto');
  ga('send', 'pageview');

</script>
HTML;

			}
		} else {
			echo "\r\n<!-- analytics api key not set - cannot enable -->\r\n";
		}
	}

	public function add_analytics_tag_manager() {
		$api_key = $this->get_option('analytics_tag_manager_key', '');
		if ( !empty($api_key) ) {
			$maximum_role = $this->get_option('analytics_maximum_role', '');
			if ( empty($maximum_role) || !current_user_can("{$maximum_role}") ) {
				/** @noinspection CommaExpressionJS */
				echo <<<HTML

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id={$api_key}"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>
(function(w,d,s,l,i){
  w[l]=w[l]||[];
  w[l].push({
      'gtm.start':new Date().getTime(),
      event:'gtm.js'
  });
  var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),
    dl=l!='dataLayer'?'&l='+l:'';
    j.async=true;
    j.src= '//www.googletagmanager.com/gtm.js?id='+i+dl;
    f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','{$api_key}');
</script>
<!-- End Google Tag Manager -->

HTML;

			}
		} else {
			echo "\r\n<!-- analytics tag manager key not set - cannot enable -->\r\n";
		}
	}

	# Post type is WPCF7_ContactForm::post_type
	public function add_user_to_mailing_list( \WPCF7_ContactForm $contact_form = null) {
		try {
			if ( !is_null( $contact_form ) ) {
				$this->_mc_notices[] = 'Found form';
				$api_key = $this->get_option( 'mailchimp_api_key', '' );
				$list_id = $this->get_option( 'mailchimp_list_id', '' );
				$email_field_name = $this->get_option( 'mailchimp_email_field', '' );
				if ( !empty( $api_key ) && !empty( $list_id ) && !empty( $email_field_name ) ) {
					$this->_mc_notices[] = 'Found needed fields';
					$allowed_forms = $this->get_option( 'mailchimp_contact_form_ids', array() );
					if ( is_array( $allowed_forms ) && 0 < count($allowed_forms ) ) {
						$this->_mc_notices[] = 'Found forms';
						$current_form_id = $contact_form->id();
						if ( in_array($current_form_id, $allowed_forms ) ) {
							$this->_mc_notices[] = 'Can do this form';
							$submission = \WPCF7_Submission::get_instance();
							# $submission = $contact_form;
							if ( is_object( $submission) ) {
								$this->_mc_notices[] = 'Found submission';
								$posted_data = $submission->get_posted_data();
								if ( array_key_exists( $email_field_name, $posted_data ) ) {
									$this->_mc_notices[] = 'Found email field';
									$email_address = $posted_data["{$email_field_name}"];
									if ( !empty( $email_address ) ) {
										$this->_mc_notices[] = 'Found email address';
										$double_opt_in = $this->get_option( 'mailchimp_double_opt_in', true );
										$extra_fields = $this->get_option( 'mailchimp_extra_fields', '' );
										$merge_tags = $this->get_option( 'mailchimp_merge_tags', '' );

										$merge_tags = $this->prepare_merge_tags( $extra_fields, $merge_tags, $posted_data );
										if ( false === include_once $this->_plugin_file_path . 'includes/class-mail-chimp-api.php' ) {
											throw new Common_Utils_Exception( 'Failed to load class Mail_Chimp_Api', 10001 );
										}
										$this->_mc_notices[] = 'Initializing MC API';
										$api = new Mail_Chimp_Api( $api_key );
										$this->_mc_notices[] = 'Trying to add member';
										$ok = $api->list_member_add( $list_id, $email_address, $double_opt_in, null, $merge_tags );
										if ( ! $ok ) {
											$this->_mc_notices[] = "Failed to add list member";
											$this->_mc_notices[] = "List ID: {$list_id}";
											$this->_mc_notices[] = "Email: {$email_address}";
											$this->_mc_notices[] = "Merge Tags:" . ( (is_array($merge_tags) && 0 < count($merge_tags))? print_r($merge_tags, true) : 'None' );
											$attempt_errors = $api->get_errors();
											if ( is_array($attempt_errors) && 0 < count($attempt_errors) ) {
												$this->_mc_notices = array_merge( $this->_mc_notices, $attempt_errors );
											}
										} else {
											$this->_mc_notices[] = "Seems to have been added properly";
										}
									} else {
										$this->_mc_notices[] = "Empty email address";
									}
								} else {
									$this->_mc_notices[] = "Email field name {$email_field_name} not found in posted data: " . print_r($posted_data, true);
								}
							} else {
								$this->_mc_notices[] = "Submission is not an object. It is a " . gettype( $submission );
							}
						} else {
							$this->_mc_notices[] = "Not a form we worry about";
						}
					} else {
						$this->_mc_notices[] = "No allowed forms found";
					}
				} else {
					$this->_mc_notices[] = "Empty API Key, List ID, or Email Field Name";
				}
			} else {
				$this->_mc_notices[] = "Null contact form";
			}
		} catch ( \Exception $e ) {
			error_log( print_r( $this->_mc_notices, true ) );
			error_log( print_r( $e, true) );
			$this->handle_exception( $e, false, true );
		}
		#if ( 0 < count($this->_mc_notices) ) {
			#try {
			#	throw new Common_Utils_Exception("TESTING..." . print_r($this->_mc_notices, true), 10001);
			#} catch ( Common_Utils_Exception $e ) {
			#	$this->handle_exception( $e, false, true );
			#}
		#	error_log( print_r($this->_mc_notices, true) );
		#}
	}

	protected function prepare_merge_tags( $field_names, $merge_tags, $data ) {
		$tags = array();
		if ( !empty($field_names) && !empty( $merge_tags) ) {
			$field_names = explode(',', ltrim(rtrim($field_names)));
			$merge_tags = explode(',', ltrim(rtrim($merge_tags)));

			foreach ( $field_names as $k => $field_name ) {
				$field_name = ltrim(rtrim($field_name));
				if ( array_key_exists($k, $merge_tags) ) {
					# we ignore it if there are more merge tags than fields specified
					if ( array_key_exists( $field_name, $data ) ) {
						$value = $data[$field_name];
						$tag_list = ltrim( rtrim( $merge_tags[$k] ) );
						if ( false !== strpos( $tag_list, ' ' ) ) {
							# Multiple tags are supposed to be pulled out of this field
							$tag_list = explode( ' ', $tag_list );
							$tag_count = count( $tag_list );
							$value_list = ( false === strpos( $value, ' ' ) ) ? array($value) : explode(' ', $value);
							$value_count = count($value_list);
							if ( $tag_count === $value_count ) {
								$this->_mc_notices[] = "Need to pull tags from values";
								foreach ( $tag_list as $x => $tag_name ) {
									$this->_mc_notices[] = "X is {$x}, tag name is {$tag_name}";
									$tags["{$tag_name}"] = $value_list[$x];
								}
							} else if ( $tag_count > $value_count ) {
								$this->_mc_notices[] = "Need to pull {$tag_count} tags from {$value_count} values";
								# more tags specified than values found - just grab those we can
								foreach ( $tag_list as $x => $tag_name ) {
									if ( array_key_exists($x, $value_list ) ) {
										$tags["{$tag_name}"] = $value_list[$x];
									}
								}
							} else {
								$this->_mc_notices[] = "Need to pull {$tag_count} tags from {$value_count} values";

								# more values available than tags specified - how to deal with this
								# stick all extra values in the last merge tag
								$last_good_id = 0;
								foreach ( $value_list as $x => $item_value ) {
									if ( array_key_exists($x, $tag_list) ) {
										$last_good_id = $x;
										$tag_name = $tag_list[$x];
										$tags[$tag_name] = $item_value;
									} else {
										$tag_name = $tag_list[$last_good_id];
										$tags[$tag_name] .= ' ' . $item_value;
									}
								}
							}
						} else {
							$this->_mc_notices[] = "Pulling field {$field_name} as a single merge tag value";
							$tag_name = $merge_tags[$k];
							if ( is_array( $value ) ) {
								$value = reset($value);
							}
							$tags["{$tag_name}"] = $value;
						}
					} else {
						$this->_mc_notices[] = "Field name {$field_name} not found in data";
					}
				} else {
					$this->_mc_notices[] = "Key {$k} not found in merge tags list";
				}
			}
		} else {
			$this->_mc_notices[] = "No field names or merge tags";
		}
		if ( 0 == count($tags) ) {
			$tags = null;
		}
		return $tags;
	}

	public function add_unit_test_short_code() {
		try {
			if ( false === include_once $this->_plugin_file_path . 'includes/shortcodes/shortcode-unit-test.php' ) {
				throw new Common_Utils_Exception( 'Failed to load class Unit_Test', 10001 );
			} else if ( class_exists( __NAMESPACE__ . '\\Unit_Test') ) {
				Unit_Test::init();
			}

		} catch ( \Exception $e ) {
			# Do nothing - we just prevent things from dying here
		}
	}

	public function add_sitemap_short_code() {
		try {
			if ( false === include_once $this->_plugin_file_path . 'includes/shortcodes/shortcode-sitemap.php' ) {
				throw new Common_Utils_Exception( 'Failed to load sitemap shortcode', 10001 );
			} else if ( class_exists( __NAMESPACE__ . '\\Site_Map') ) {
				$prefix = $this->_prefix;
				$cache_refresh_minutes = $this->get_option('sitemap_cache_minutes', 0);
				$search_placement = $this->get_option('sitemap_search_placement', 0);
				$search_settings = $this->get_option('sitemap_search_settings', array());
				$display_settings = $this->get_option('sitemap_display_settings', array());
				new Site_Map( 'sitemap', $prefix, $cache_refresh_minutes, $search_placement, $search_settings, $display_settings );
			}
		} catch ( \Exception $e ) {
			# Do nothing
		}
	}

	protected function handle_exception( \Exception $e, $die_on_error = false, $write_to_error_log = false ) {
		if ( defined('\\WP_DEBUG') && \WP_DEBUG ) {
			$die_on_error = true;
			if ( defined('\\WP_DEBUG_LOG') && \WP_DEBUG_LOG ) {
				$write_to_error_log = true;
				if ( defined('\\WP_DEBUG_DISPLAY') && ! \WP_DEBUG_DISPLAY ) {
					$die_on_error = false;
				}
			}
		}
		$error_out = apply_filters( "{$this->_prefix}die_on_exception", $die_on_error, $e );
		$email_admin = apply_filters( "{$this->_prefix}mail_admin_exception", false, $e);
		$code = $e->getCode();
		$message = $e->getMessage();
		$trace = print_r( $e->getTrace(), true );
		$line = $e->getLine();
		$file = $e->getFile();
		if ( $error_out || $email_admin ) {

			$output = <<<HTML
<h2>HF Common Utilities Error #{$code}</h2>
<h3>Location: {$file}, line {$line}</h3>
<p>{$message}</p>
<pre>{$trace}</pre>
HTML;

			if ( $email_admin ) {
				$admin_email = apply_filters( $this->_prefix . 'mail_exceptions_to', get_option('admin_email', '') );
				if ( !empty($admin_email) ) {
					wp_mail( $admin_email, "HF Common Utilities Error #{$code}", $output );
				}
			}

			if ( $error_out ) {
				wp_die( $output, "HF Common Utilities Error #{$code}: {$message}" );
			}
		}
		if ( $write_to_error_log ) {
			error_log("HF Common Utilities Error {$code} ({$file}:{$line}): {$message}:trace: {$trace}");
		}
	}

	public function get_plugin_info() {
		if ( is_null( $this->_plugin_data ) ) {
			if ( ! function_exists('get_plugin_data' ) ) {
				throw new Common_Utils_Exception("Calling get_plugin_info too early!", 10001);
			} else {
				$this->_plugin_data = get_plugin_data( $this->_plugin_file_path . $this->_base_name, false, true );
			}
		}
		return $this->_plugin_data;
	}
}

try {

	$plugin_file_path = plugin_dir_path( __FILE__ );
	if ( is_admin() ) {
		if ( false === include_once $plugin_file_path . 'includes/class-common-admin-utils.php' ) {
			throw new Common_Utils_Exception( 'Failed to load class Common_Admin_Utils', 10001 );
		}
		$Utils = new Common_Admin_Utils(
			$plugin_file_path
		);
	} else {
		$Utils = new Common_Utils(
			$plugin_file_path
		);
	}
	add_action('plugins_loaded', array( $Utils, 'init') );

} catch ( \Exception $e ) {
	$die_on_error = true;
	$write_to_error_log = false;
	if ( defined('\\WP_DEBUG') && \WP_DEBUG ) {
		$die_on_error = true;
		if ( defined('\\WP_DEBUG_LOG') && \WP_DEBUG_LOG ) {
			$write_to_error_log = true;
			if ( defined('\\WP_DEBUG_DISPLAY') && ! \WP_DEBUG_DISPLAY ) {
				$die_on_error = false;
			}
		}
	}
	$error_out = apply_filters( 'hf_bh_die_on_exception', $die_on_error, $e );
	if ( $error_out ) {
		$code = $e->getCode();
		$message = $e->getMessage();
		$trace = print_r( $e->getTrace(), true );
		$line = $e->getLine();
		$file = $e->getFile();

		$output = <<<HTML
<h2>Common Utilities Plugin Error #{$code}</h2>
<h3>Location: {$file}, line {$line}</h3>
<p>{$message}</p>
<pre>{$trace}</pre>
HTML;

		if ( $write_to_error_log ) {
			error_log("Common Utilities Plugin Error {$code} ({$file}:{$line}): {$message}:trace: {$trace}");
		}

		if ( $error_out ) {
			wp_die( $output, "Common Utilities Plugin Error #{$code}: {$message}" );
		}
	}
}

