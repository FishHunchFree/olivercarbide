<?php
/*
Plugin Name: Rubberhog Tire Repair Products Display Shortcode
Description: Shortcode to easily display Rubberhog Tire Repair products by category on Product Pages.
Version: 1.0.0
Author: jordan@hunchfree.com
Author URI: http://hunchfree.com/
License: GPL 2 Compatible

------------------------------------------------------------------------
Copyright Hunchfree March 30 2017

*/
// create shortcode with parameters so that the user can define what's queried - default is to list all blog posts
add_shortcode( 'rubberhog-products', 'products_shortcode' );
function products_shortcode( $atts ) {
    ob_start();

extract( shortcode_atts( array(
        'category' => ''
    ), $atts ) );

$product_args = array ( 
	'posts_per_page' => -1,
	'post_type' => 'rh_products',
	'order' => 'ASC',
	'orderby' => 'date',
);

	if ( ! empty( $category ) ) {
        $product_args['category_name'] = $category;
    }

	/** @var \WP_Query $wp_query */
$product_query = new WP_Query( $product_args );

$product_list = '';

if ( $product_query->have_posts() ) {
	while( $product_query->have_posts() ) {
		$product_query->the_post();
		$id = get_the_ID();
        $post_type = get_post_type( $id );
		$link = get_permalink($id);
		$title = get_the_title();
		$product_info = get_field('product_info');
		# Set Up Tech Specs 
			$rows = get_field('product_specs');
			$product_specs_out = '';
if($rows)
{
	$product_specs_out .= '<div class="column large-4 skinny">
				<table>
					<thead>
						<tr>
							<th width="150">Part Number</th>
							<th width="150">Diameter</th>
							<th width="150">Bore</th>
							<th width="150">Coating</th>
						</tr>
					</thead>
					<tbody>';
									
	

	foreach($rows as $row)
	{

		$product_specs_out .= '<tr>
								<td>' . $row['part_number'] . '</td>
								<td>' . $row['diameter'] . '</td>
								<td>' . $row['bore'] . '</td>
								<td>' . $row['coating'] . '</td>
							</tr>';
	}

	$product_specs_out .= '</tbody>
						</table>
						</div>
						</div>';
}

		
		
		
        $post_image_data = wp_get_attachment_image_src( 'medium' );
		$post_thumbnail = '';
		if ( current_theme_supports('post-thumbnails') && has_post_thumbnail() ) {
			# There is a post thumbnail - grab that
			$post_thumb_id = get_post_thumbnail_id($id);
			$image_data = wp_get_attachment_image_src($post_thumb_id, 'full');
			$post_thumbnail = <<<HTML
<img id="attachment_{$post_thumb_id}" class="wp-image-{$post_thumb_id}" src="{$image_data[0]}" width="{$image_data[1]}" height="{$image_data[2]}" alt="K9 Officer Photo" />
HTML;
}

		
		$product_list .= <<<HTML
  		<section class="product-info">
		<div class="row">
			<div class="column medium-6 medium-push-3 large-4 large-push-0 skinny">
				{$post_thumbnail}
			</div>
			<div class="column large-4 skinny">
				<h2>{$title}</h2>
				<p>{$product_info}</p>
			</div>
			{$product_specs_out}
		</div>
	</section>
	<hr>
HTML;

	}
	
echo <<<HTML
			{$product_list}
HTML;
					
}

$myvariable = ob_get_clean();
        return $myvariable;      
}