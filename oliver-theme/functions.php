<?php

defined('\\ABSPATH') or die('Permission denied');

if ( ! is_admin() ) {
	function oliver_setup_styles_and_scripts() {
		wp_register_script('font-awesome', "https://use.fontawesome.com/53ac23b42e.js", array(), "4.6.1", 'all');
		wp_register_style('fonts', 'https://fonts.googleapis.com/css?family=Oswald:500|Source+Sans+Pro:300', array(), null, 'all');
		$assets_url = trailingslashit( get_stylesheet_directory_uri() ) . 'assets/';
		wp_register_style('swiper', $assets_url . 'css/swiper.min.css', array(), null, 'all');
		#$assets_url = trailingslashit( get_stylesheet_directory_uri() ) . 'assets/';
		#wp_register_script('jquery-js', $assets_url . 'js/vendor/jquery.js', array( 'jquery' ), null, true);
		#$assets_url = trailingslashit( get_stylesheet_directory_uri() ) . 'assets/';
		#wp_register_script('what-input', $assets_url . 'js/vendor/what-input.js', array( 'jquery' ), null, true);
		#$assets_url = trailingslashit( get_stylesheet_directory_uri() ) . 'assets/';
		wp_register_script('foundation-js', $assets_url . 'js/vendor/foundation.js', array( 'jquery' ), null, true);
		#wp_register_script('app', $assets_url . 'js/app.js', array( 'jquery' ), null, true);
		wp_register_script('modernizr', $assets_url . 'js/modernizr.custom.js', array( 'jquery' ), null, true);
		wp_register_script('jquery-js', $assets_url . 'js/vendor/jquery.js', array( 'jquery' ), null, true);
		wp_register_script('swiper-js', $assets_url . 'js/swiper.min.js', array( 'jquery' ), null, true);
		wp_register_script('classie', $assets_url . 'js/classie.js', array( 'jquery' ), null, true);
		wp_register_script('demo-1', $assets_url . 'js/demo1.js', array( 'jquery' ), null, true);



		
		wp_enqueue_script('font-awesome');
		wp_enqueue_style('fonts');
		wp_enqueue_script('swiper');
		#wp_enqueue_script('what-input');
		wp_enqueue_script('foundation-js');
		wp_enqueue_script('app');
		wp_enqueue_script('modernizr');
		wp_enqueue_script('jquery-js');
		wp_enqueue_script('swiper-js');
		wp_enqueue_script('classie');
		wp_enqueue_script('demo-1');


	}
	add_action('wp_enqueue_scripts', 'oliver_setup_styles_and_scripts' );

	function oliver_remove_excerpt_more( $more ) {
		return '';
	}
	add_filter('excerpt_more', 'oliver_remove_excerpt_more', 15 );
	
}

function special_nav_class ($classes, $item) {
    if (in_array('current-menu-item', $classes) ){
        $classes[] = 'is-the-page';
    }
    return $classes;
}

function register_my_menus() {
  register_nav_menus(
    array(
      'oliver_nav' => __( 'Oliver Menu' ),
      'kutzall_nav' => __( 'Kutzall Menu' ),
      'rst_nav' => __( 'Rubberhog Tire Repair Menu' ),
      'rgw_nav' => __( 'Rubberhog Roll Grinding Menu' ),
      'shieldzall_nav' => __( 'Shieldzall Menu' ),
      'mobile_nav' => __( 'Mobile Menu' ),
    )
  );
}
add_action( 'init', 'register_my_menus' );

remove_filter( 'the_content', 'wpautop' );
remove_filter( 'the_excerpt', 'wpautop' );


function cptui_register_my_cpts_kutzall() {

	/**
	 * Post Type: Kutzall.
	 */

	$labels = array(
		"name" => __( 'Kutzall', 'hf_found' ),
		"singular_name" => __( 'Kutzall', 'hf_found' ),
	);

	$args = array(
		"label" => __( 'Kutzall', 'hf_found' ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => true,
		"rewrite" => array( "slug" => "kutzall", "with_front" => true ),
		"query_var" => true,
		"supports" => array( "title", "editor", "thumbnail", "revisions", ),
	);

	register_post_type( "kutzall", $args );
}

add_action( 'init', 'cptui_register_my_cpts_kutzall' );
