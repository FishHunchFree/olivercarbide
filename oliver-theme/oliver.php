<?php
use Com\Hunchfree\Wp\Themes\Hffoundation as Hffoundation;
/*
	Template Name: Oliver Corp
*/

# This template uses row expanded instead of just row for drawing content

get_header();


try {

	$renderer = Hffoundation\Theme_Front_End::get_instance();
	if ( !is_object( $renderer ) ) {
		$this->_notices[] = "Failed to get renderer instance";
		throw new \Exception("Failed to get renderer instance", 10001);
	}

	# Set up our placable sidebars, if they are used
	$block_sb_side_one = '';
	$block_sb_side_two = '';
	$extra_main_wrapper_css = '';
	$shell_content_extra_css = '';
	$placeable_sidebar_use = (int)$renderer->get_best_mod('placeable_sidebars_use', 0);
	if ( 1 === $placeable_sidebar_use ) {

		# Placeable Sidebar Area - Side One : left of any content area
		$widgets = $renderer->get_sidebar_widgets("sb_content_side_one", "sb_content_side_one_css", 'small-12' );
		if ( !empty( $widgets ) ) {
			$sidebar_id = $renderer->get_last_used_sidebar_id();
			$block_sb_side_one = <<<HTML
<div id="sidebar-side-one" class="columns small-12 medium-4 large-3 sidebar-wrapper">
	<div class="row sidebar widget-wrapper sidebar-{$sidebar_id}">
		{$widgets}
	</div>
</div>

HTML;

		}

		# Placeable Sidebar Area - Side Two : Right of any content area
		$widgets = $renderer->get_sidebar_widgets("sb_content_side_two", "sb_content_side_two_css", 'small-12' );
		if ( !empty( $widgets ) ) {
			$sidebar_id = $renderer->get_last_used_sidebar_id();
			$block_sb_side_two = <<<HTML
<div id="sidebar-side-two" class="columns small-12 medium-4 large-3 sidebar-wrapper">
	<div class="row sidebar widget-wrapper sidebar-{$sidebar_id}">
		{$widgets}
	</div>
</div>

HTML;

		}

		# Based on known content areas, set up any extra content wrapper styles
		# the left and right sidebars use : small-12 medium-4 large-3
		if ( !empty( $block_sb_side_one ) ) {
			if ( !empty( $block_sb_side_two ) ) {
				$extra_main_wrapper_css = 'medium-4 large-6';
				$shell_content_extra_css = 'two-sidebars';
			} else {
				$extra_main_wrapper_css = 'medium-8 large-9';
				$shell_content_extra_css = 'one-sidebar';
			}
		} else if ( !empty( $block_sb_side_two ) ) {
			$extra_main_wrapper_css = 'medium-8 large-9';
			$shell_content_extra_css = 'one-sidebar';
		}
	}

	if ( !have_posts() ) {
		$this->_notices[] = "Attempting to draw singular without any posts!";
		throw new \Exception("Non 404, singular, without any posts!", 10001);

	} else {
		while ( have_posts() ) {
			# load the current post into the global space for easy access
			the_post();

			# grab access to the currently loaded post
			global $post;

			# get the post id, css classes for it, and post type
			$post_id = get_the_ID();
			$css_classes = implode(' ', get_post_class());
			$post_type = get_post_type( $post );

			# We are drawing the entire post.

			###
			### The Title Block
			### - you can set a hide_title custom variable on a post to hide the title
			### - alternately, you can use is_page() and similar functions to force no title display for specific post types

			$block_title = '';

			# block_main_title : a component of block_title
			$block_main_title = '';
			if ( post_type_supports( $post_type, 'title' ) ) {
				$page_title = '';
				$hide_title = get_post_meta( $post_id, 'hide_title', true);
				if ( '1' !== $hide_title ) {
					$page_title = get_the_title();
					if ( !empty( $page_title ) ) {
						$page_title = "<h1>{$page_title}</h1>";
					}
				}
				if ( !empty( $page_title ) ) {
					$block_main_title = <<<HTML
<div id="s-page-title" class="page-title">
<!-- {$page_title}> -->
</div>

HTML;

				}
			}

			# If any components of block_title are not empty, set up the block_title element
			if ( !empty( $block_main_title ) ) {
				$block_title = <<<HTML
<header id="s-page-title-wrap" class="page-title-wrap columns">
{$block_main_title}
</header>

HTML;


			}

			# Determine if there are sub pages and set up sub page navigation
			#global $pages, $page;
			global $pages;
			$block_subpage_nav = '';
			if ( is_array( $pages ) && 1 < count( $pages ) ) {
				$is_paged = true;
			} else if ( is_int($pages) && 1 < $pages ) {
				$is_paged = true;
			} else {
				$is_paged = false;
			}
			if ( $is_paged ) {
				# Notice that this is filtered via the Theme_Front_End class (override_wp_link_pages)
				$paged_nav = wp_link_pages(
					array(
						'echo' => 0,
						'before' => __('Pages:'),
						'after' => '',
						'separator' => '</li><li>',
						'nextpagelink' => '',
						'previouspagelink' => ''
					)
				);
				if ( !empty($paged_nav) ) {
					$block_subpage_nav = '<nav id="s-page-paged-nav" class="columns theme-navi navi-paged"><ul class="pagination">' . $paged_nav . '</ul></nav>';
				}
			}

			###
			### The Content Block
			###

			$block_content = '';

			# post_content - a component block_content
			$post_content = '';
			if ( post_type_supports( $post_type, 'editor' ) ) {
				remove_filter('the_content', 'wpautop');
				$post_content = get_the_content( 'Read More...' );
				$post_content = apply_filters( 'the_content', $post_content );
				$post_content = str_replace( ']]>', ']]&gt;', $post_content );
				add_filter('the_content', 'wpautop');
			}

			# featured image - a component of block_content
			# - if you want to do something different with the featured image, edit the below code.
			$featured_image = '';
			if ( current_theme_supports('post-thumbnails') && has_post_thumbnail() ) {
				if ( 'attachment' == "{$post_type}" || 'post' == "{$post_type}" ) {
					$post_thumbnail_id = get_post_thumbnail_id( $post_id );
					$post_image_data = wp_get_attachment_image_src( $post_thumbnail_id, 'large' );
					$attachment = get_post( $post_thumbnail_id );
					if ( is_object( $attachment ) ) {
						$thumbnail_alt = ( isset( $attachment->post_title ) && !empty( $attachment->post_title ) ) ? esc_attr( $attachment->post_title ) : esc_attr( get_the_title() );
					} else {
						$thumbnail_alt = esc_attr( get_the_title() );
					}
					$featured_image = <<<HTML
<div class="featured_image"><img src="{$post_image_data[0]}" id="attachment_{$post_thumbnail_id}" width="{$post_image_data[1]}" height="{$post_image_data[2]}" alt="{$thumbnail_alt}" /></div>
HTML;

				}
			}

			# If any component of block_content is not empty, set up the block_content area
			# - by default this draws the featured image before the content.
			# - if you want the feature image floated, add the float to the featured_image div.
			if ( !empty( $post_content ) || !empty( $featured_image ) ) {
				$block_content = <<<HTML
<div id="s-page-content" class="page-content">{$featured_image}{$post_content}</div>
HTML;

			}

			# Breadcrumbs
			# - this is an example of how to do breadcrumbs if they are wanted
			# - other available bars are author, date, and any taxonomy name
			$block_breadcrumbs = '';
			# - comment out the following 7 lines if you do not want breadcrumbs
			/*$wanted_bars = array('main');
			$crumb_bars = $renderer->get_nav_breadcrumbs( $wanted_bars );
			if ( is_array($crumb_bars) && 0 < count($crumb_bars) ) {
				if ( array_key_exists('main', $crumb_bars) ) {
					$block_breadcrumbs = '<div class="columns">' . $crumb_bars['main'] . '</div>';
				}
			}
            */
			# Next, Up, and Previous Navigation
			# - next			: next post of the same type
			# - up			: up to any available post type archive (or nothing is displayed)
			# - previous	: previous post of same type
			$block_paged_navigation = '';
			# comment out the next block of code if you do not want next, up, and previous navigation
			/*list( $up_one, $previous_item, $next_item ) = $renderer->get_nav_up_prev_next();
			if ( !empty($up_one) || !empty($previous_item) || !empty( $next_item ) ) {
				if ( empty( $up_one ) ) {
					$up_one = '&nbsp;';
				}
				if ( empty( $previous_item ) ) {
					$previous_item = '&nbsp;';
				}
				if ( empty( $next_item ) ) {
					$next_item = '&nbsp;';
				} 
				$block_paged_navigation = <<<HTML
<nav class="theme-navi next-previous row-expanded">
	<div class="columns small-12 medium-4 large-5 medium-text-left">{$previous_item}</div>
	<div class="columns small-12 medium-4 large-2 medium-text-center">{$up_one}</div>
	<div class="columns small-12 medium-4 large-5 medium-text-right">{$next_item}</div>
</nav>

HTML;

			} */

			# Comments
			# - rather than commenting out the following, use theme-configuration to remove comments post type support
			# - for any post types that you don't want to show comments for.
			$block_comments = '';
			if ( post_type_supports( $post_type, 'comments') ) {
				ob_start();
				comments_template('', true);
				$comments = ob_get_contents();
				ob_end_clean();
				if ( !empty( $comments ) ) {
					$block_comments = <<<HTML
<div id="comment-wrap" class="columns">
{$comments}
</div>

HTML;

				}
			}

			# Meta - author (or author linked)
			# - Many themes won't use these at all
			# - If you do need any of them, re-write the drawing code as wanted and then
			# - use them to build out the content you want in different places.
			$block_byline = '';
			$block_taxes = '';
			$use_meta = ( is_page() )? false : true;
			if ( $use_meta ) {
				# Non pages should include the following:
				# byline (under title) Posted by linked-author on Jan 25 2014
				# footer (above comments) Categorized <linked-cat>, Tags: <linked-tags>
				$meta_author = '';
				if ( post_type_supports( $post_type, 'author' ) ) {
					if ( !empty( $post->post_author ) ) {
						$author = get_userdata( $post->post_author );
						$author_url = get_author_posts_url( $post->post_author );
						$author_name = ( empty( $author->nickname ) )? $author->display_name : $author->nickname;
						$meta_author = <<<HTML
<span class="meta meta-author meta-author-{$author->ID}"><a href="{$author_url}">{$author_name}</a></span>
HTML;

					}
				}

				# Meta - published date
				$meta_published = '';
				$meta_published_date = get_the_date('M j Y');
				if ( !empty( $meta_published_date ) ) {
					$meta_published = <<<HTML
<span class="meta meta-date meta-published">{$meta_published_date}</span>
HTML;

				}

				if ( !empty($meta_published) || !empty( $meta_author) ) {
					if ( !empty($meta_author) ) {
						if ( !empty( $meta_published ) ) {
							$block_byline = '<div class="page-byline">Posted by ' . $meta_author . ' on ' . $meta_published . '</div>';
						} else {
							$block_byline = '<div class="page-byline">Posted by ' . $meta_author . '</div>';
						}
					} else {
						$block_byline = '<div class="page-byline">Posted on ' . $meta_published . '</div>';
					}
				}

				$category_list = '';
				$cats = get_the_terms( $post_id, 'category' );
				if ( is_array($cats) && 0 < count($cats) ) {
					$cat_links = array();
					foreach ( $cats as $cat ) {
						$cat_link = get_term_link( $cat );
						if ( !is_wp_error( $cat_link ) ) {
							$cat_links[] = '<a href="' . $cat_link . '" class="meta-tax">' . $cat->name . '</a>';
						}
					}
					if ( 0 < count($cat_links) ) {
						$cat_listing = implode(', ', $cat_links);
						$category_list = <<<HTML
<span class="meta-category"><span class="meta-title">Categorized</span> <span class="meta-taxes">{$cat_listing}</span></span>
HTML;
					}
				}

				$tag_list = '';
				$tags = get_the_terms( $post_id, 'post_tag' );
				if ( is_array($tags) && 0 < count($tags) ) {
					$tag_links = array();
					foreach ( $tags as $tag ) {
						$tag_link = get_term_link( $tag );
						if ( !is_wp_error( $tag_link ) ) {
							$tag_links[] = '<a href="' . $tag_link . '" class="meta-tax">' . $tag->name . '</a>';
						}
					}
					if ( 0 < count($tag_links) ) {
						$tag_listing = implode(', ', $tag_links);
						$tag_list = <<<HTML
<span class="meta-post_tag"><span class="meta-title">Tagged</span> <span class="meta-taxes">{$tag_listing}</span></span>
HTML;
					}
				}

				if ( !empty($category_list ) ) {
					if ( !empty( $tag_list ) ) {
						$block_taxes = '<div class="page-meta">' . $category_list . ' ' . $tag_list . '</div>';
					} else {
						$block_taxes = '<div class="page-meta">' . $category_list . '</div>';
					}
				} else if ( !empty( $tag_list ) ) {
					$block_taxes = '<div class="page-meta">' . $tag_list . '</div>';
				}

			}

			###
			### Sidebars are handled near the top of this file.
			###

			/**
			 * The following does all of the actual drawing
			 */
			echo <<<HTML
			<article id="post_{$post_id}" class="{$css_classes}">				
				{$block_content}
			</article>

HTML;

		}
	}
} catch ( \Exception $e ) {
	if ( WP_DEBUG || ( is_user_logged_in() && current_user_can('activate-plugins') ) ) {
		echo "<p>Exception Encountered:</p><pre>" . print_r($e, true) . "</pre>";
		if ( isset( $o_renderer ) && is_object( $o_renderer ) ) {
			$notices = $o_renderer->get_warnings();
			if ( 0 < count($notices) ) {
				echo '<div><h4>Notices:</h4><pre>' . print_r($notices, true) . '</pre></div>';
			}
		}
	}
}

get_footer();

/**
 * Draw out the contents of a 404 page
 */
function theme_draw_four_zero_four() {

	$title_bit = '<h1>404 Not Found</h1>';



	echo <<<HTML
<section class="header kutzall-head">
	<div class="row expanded top-content">
		<div class="row">
			<div class="column small-12 medium-10 medium-centered text-center">
				<h2>The Content You Are Looking For Could Not Be Found</h2>
          	</div>
		</div>
		<div class="row">
			<div class="columns small-12 text-right">
	    		<a href="/kutzall/about/"><button class="cta blue"><span>Return to Kutzall Home</span><i class="fa fa-long-arrow-right" aria-hidden="true"></i></button></a>
	    	</div>
		</div>
	</div>
</section>

HTML;

}