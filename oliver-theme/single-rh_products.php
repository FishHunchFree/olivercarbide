<?php
use Com\Hunchfree\Wp\Themes\Hffoundation as Hffoundation;

/**
 * Default template for drawing content when no other template is available.
 *
 * There are 3 distinct output types in this file:
 * 1) When a 404 type request is made
 * 2) When a singular post type is requested
 * 3) When anything else is requested (displaying lists of posts)
 *
 * This can be completely overridden by copying it to your child theme and editing it there.
 *
 * @see https://developer.wordpress.org/themes/basics/template-hierarchy/ Wordpress Template Hierarchy
 */

# Load the header.php file, which takes care of drawing the html header and visible header
# - note: header.php should load visible_header.php
get_header();

# Take care of drawing out everything between the header and the footer.
# - In case of an exception, draw it out.
# - If there are warnings and the user is a logged in admin, draw them
try {

	$renderer = Hffoundation\Theme_Front_End::get_instance();
	if ( !is_object( $renderer ) ) {
		$this->_notices[] = "Failed to get renderer instance";
		throw new \Exception("Failed to get renderer instance", 10001);
	}

	if ( is_404() ) {
		theme_draw_four_zero_four();
	} else {

		if ( is_singular() ) {
			theme_draw_singular(
				$renderer,
				$block_sb_side_one, $block_sb_side_two,
				$extra_main_wrapper_css, $shell_content_extra_css
			);
		} 
	}
} catch ( \Exception $e ) {
	if ( WP_DEBUG || ( is_user_logged_in() && current_user_can('activate-plugins') ) ) {
		echo "<p>Exception Encountered:</p><pre>" . print_r($e, true) . "</pre>";
		if ( isset( $o_renderer ) && is_object( $o_renderer ) ) {
			$notices = $o_renderer->get_warnings();
			if ( 0 < count($notices) ) {
				echo '<div><h4>Notices:</h4><pre>' . print_r($notices, true) . '</pre></div>';
			}
		}
	}
}

get_footer();

/**
 * Draw out the contents of a 404 page
 */
function theme_draw_four_zero_four() {

	$title_bit = '<h1>404 Not Found</h1>';

	echo <<<HTML
<div id="shell-content" class="row-expanded shell-content-singular">
	<div class="row">
		<div class="columns small-12">
			<article class="row article-wrap">
				<header class="columns page-title">
					<h1>{$title_bit}</h1>
				</header>
				<div class="columns content">
					<p>The content you were looking for was not found.</p>
				</div>
			</article>
		</div>
	</div>
</div>

HTML;

}

/**
 * Draw out the contents of a singular post type (posts, pages, custom post types)
 *
 * @param Hffoundation\Theme_Front_End $renderer	For utilities related to drawing out content
 * @param string $block_sb_side_one Content of any left sidebar
 * @param string $block_sb_side_two Content of any right sidebar
 * @param string $extra_main_wrapper_css Any css classes to stuff into the outer wrapper for sidebar placement
 * @param string $shell_content_extra_css Any css classes to stuff into the article for sidebar placement
 *
 * @throws Exception when a serious issue occurs
 *
 * @todo load theme test data and go through to ensure everything looks good by default
 * @todo paged_navigation using <!--nextpage--> tag
 * @todo for singular attachments, insert the attachment into the post content
 * @todo style comments
 * @todo let pingbacks and trackbacks show up even if comments are disabled
 */
function theme_draw_singular(
	Hffoundation\Theme_Front_End &$renderer,
	$block_sb_side_one = '', $block_sb_side_two = '',
	$extra_main_wrapper_css = '', $shell_content_extra_css = ''
) {

	if ( !have_posts() ) {
		$this->_notices[] = "Attempting to draw singular without any posts!";
		throw new \Exception("Non 404, singular, without any posts!", 10001);

	} else {
		while ( have_posts() ) {
			# load the current post into the global space for easy access
			the_post();

			# grab access to the currently loaded post
			global $post;

			# get the post id, css classes for it, and post type
			$post_id = get_the_ID();
			$css_classes = implode(' ', get_post_class());
			$post_type = get_post_type( $post );


			$block_content = '';
			# We are drawing the entire post.

			$post_title = get_the_title();
			$header_description = get_field('header_description');
			$hero_image = get_field('hero_image');
		
		
			if( have_rows('product_group') ) {
 				$product_group = '';
 
				while( have_rows('product_group') ) { 
					the_row();
					
					$product_group_name = get_sub_field('product_group_name');
					$extract_group = get_sub_field('product_group_photo');
							if( !empty($extract_group) ) {

								// vars
								$group_url = $extract_group['url'];
								$group_alt = $extract_group['alt'];
							}
					$product_group_photo = <<<HTML
					<img alt="{$group_alt}" src="{$group_url}"/>
HTML;
					
					
				
					$disclaimer = get_sub_field('disclaimer');
					
					if( have_rows('product_group_bullets') ) {
 						$group_bullets = '';
 					
						while( have_rows('product_group_bullets') ) { 
							the_row();
					
							$extract_bullets = get_sub_field('bullet');
							$group_bullets .= <<<HTML
								<li>{$extract_bullets}</li>
HTML;

						}
					}
					
					if( have_rows('flared_contour_wheel_table') ) {
 						$group_table = '';
 						$table_head = '';
 					
						while( have_rows('flared_contour_wheel_table') ) { 
							the_row();
					
							$product_number = get_sub_field('product_number');
							$diameter = get_sub_field('diameter');
							$bore = get_sub_field('bore');
							$coating = get_sub_field('coating');
							$workstation = get_sub_field('workstation');
							$application = get_sub_field('application');
							$extract_thumb = get_sub_field('product_thumbnail');
							if( !empty($extract_thumb) ) {

								// vars
								$thumb_url = $extract_thumb['url'];
								$thumb_alt = $extract_thumb['alt'];
							}
							$product_image = <<<HTML
								<ul class="enlarge">
                        		<li>
								<img alt="{$thumb_alt}" src="{$thumb_url}" class="thumb">
								<span> <!--span contains the popup image-->
								<img alt="{$thumb_alt}" src="{$thumb_url}">
								<br />Part No. {$product_number}
								</span>
								</li>
								</ul>
HTML;
							
							$table_head = <<<HTML
								<thead>
									<tr>
										<th width="50">Photo</th>
										<th>Part Number</th>
										<th>Diameter</th>
										<th>Bore</th>
										<th width="120px">Coating</th>
										<th style="background: #4c4c4c;">Workstation</th>
										<th style="background: #4c4c4c;">Application</th>
									</tr>
								</thead>
HTML;
							
							$group_table .= <<<HTML
								<tr>
									<td>{$product_image}</td>
									<td>{$product_number}</td>
									<td>{$diameter}</td>
									<td>{$bore}</td>
									<td>{$coating}</td>
									<td>{$workstation}</td>
									<td>{$application}</td>
								</tr>									
HTML;

						}
					} elseif ( have_rows('extreme_contour_wheel_table') ) {
 						$group_table = '';
 						$table_head = '';
 					
						while( have_rows('extreme_contour_wheel_table') ) { 
							the_row();
					
							$product_number = get_sub_field('product_number');
							$diameter = get_sub_field('diameter');
							$bore = get_sub_field('bore');
							$coating = get_sub_field('coating');
							$extract_thumb = get_sub_field('product_thumbnail');
							if( !empty($extract_thumb) ) {

								// vars
								$thumb_url = $extract_thumb['url'];
								$thumb_alt = $extract_thumb['alt'];
							}
							$product_image = <<<HTML
								<ul class="enlarge">
                        		<li>
								<img alt="{$thumb_alt}" src="{$thumb_url}" class="thumb">
								<span> <!--span contains the popup image-->
								<img alt="{$thumb_alt}" src="{$thumb_url}">
								<br />Part No. {$product_number}
								</span>
								</li>
								</ul>
HTML;
							
							$table_head = <<<HTML
								<thead>
									<tr>
										<th width="50">Photo</th>
										<th>Part Number</th>
										<th>Diameter</th>
										<th>Bore</th>
										<th width="150px">Coating</th>
									</tr>
								</thead>
HTML;
							
							$group_table .= <<<HTML
								<tr>
									<td>{$product_image}</td>
									<td>{$product_number}</td>
									<td>{$diameter}</td>
									<td>{$bore}</td>
									<td>{$coating}</td>
								</tr>									
HTML;


						}
					
					} elseif ( have_rows('polyplug') ) {
 						$group_table = '';
 						$table_head = '';
 					
						while( have_rows('polyplug') ) { 
							the_row();
					
							$product_number = get_sub_field('product_number');
							$diameter = get_sub_field('diameter');
							$bore = get_sub_field('bore');
							$extract_thumb = get_sub_field('product_thumbnail');
							if( !empty($extract_thumb) ) {

								// vars
								$thumb_url = $extract_thumb['url'];
								$thumb_alt = $extract_thumb['alt'];
							}
							$product_image = <<<HTML
								<ul class="enlarge">
                        		<li>
								<img alt="{$thumb_alt}" src="{$thumb_url}" class="thumb">
								<span> <!--span contains the popup image-->
								<img alt="{$thumb_alt}" src="{$thumb_url}">
								<br />Part No. {$product_number}
								</span>
								</li>
								</ul>
HTML;
							
							$table_head = <<<HTML
								<thead>
									<tr>
										<th width="50">Photo</th>
										<th>Part Number</th>
										<th>Diameter</th>
										<th>Bore</th>
									</tr>
								</thead>
								
HTML;
							
							
							$group_table .= <<<HTML
								<tr>
									<td>{$product_image}</td>
									<td>{$product_number}</td>
									<td>{$diameter}</td>
									<td>{$bore}</td>
								</tr>									
HTML;

						}
					} elseif ( have_rows('buzzout_table') ) {
 						$group_table = '';
 						$table_head = '';
 					
						while( have_rows('buzzout_table') ) { 
							the_row();
					
							$product_number = get_sub_field('product_number');
							$outer = get_sub_field('outer_diameter');
							$inner = get_sub_field('inner_diameter');
							$width = get_sub_field('width');
							$coating = get_sub_field('coating');
							$extract_thumb = get_sub_field('product_thumbnail');
							if( !empty($extract_thumb) ) {

								// vars
								$thumb_url = $extract_thumb['url'];
								$thumb_alt = $extract_thumb['alt'];
							}
							$product_image = <<<HTML
								<ul class="enlarge">
                        		<li>
								<img alt="{$thumb_alt}" src="{$thumb_url}" class="thumb">
								<span> <!--span contains the popup image-->
								<img alt="{$thumb_alt}" src="{$thumb_url}">
								<br />Part No. {$product_number}
								</span>
								</li>
								</ul>
HTML;
							
							$table_head = <<<HTML
								<thead>
									<tr>
										<th width="50">Photo</th>
										<th>Part Number</th>
										<th>O.D.</th>
										<th>Width</th>
										<th width="120px">Coating</th>
										<th>I.D.</th>
									</tr>
								</thead>
								
HTML;
							
							
							$group_table .= <<<HTML
								<tr>
									<td>{$product_image}</td>
									<td>{$product_number}</td>
									<td>{$outer}</td>
									<td>{$width}</td>
									<td>{$coating}</td>
									<td>{$inner}</td>
								</tr>									
HTML;


							

						}
					} elseif ( have_rows('sidewheel_table') ) {
 						$group_table = '';
 						$table_head = '';
 					
						while( have_rows('sidewheel_table') ) { 
							the_row();
					
							$product_number = get_sub_field('product_number');
							$diamter = get_sub_field('diameter');
							$description = get_sub_field('description');
							$inner = get_sub_field('inner_diameter');
							$coating = get_sub_field('coating');
							$application = get_sub_field('application');
							$extract_thumb = get_sub_field('product_thumbnail');
							if( !empty($extract_thumb) ) {

								// vars
								$thumb_url = $extract_thumb['url'];
								$thumb_alt = $extract_thumb['alt'];
							}
							$product_image = <<<HTML
								<ul class="enlarge">
                        		<li>
								<img alt="{$thumb_alt}" src="{$thumb_url}" class="thumb">
								<span> <!--span contains the popup image-->
								<img alt="{$thumb_alt}" src="{$thumb_url}">
								<br />Part No. {$product_number}
								</span>
								</li>
								</ul>
HTML;
							
							$table_head = <<<HTML
								<thead>
									<tr>
										<th width="50">Photo</th>
										<th>Part Number</th>
										<th>Diameter</th>
										<th>Description</th>
										<th>I.D.</th>
										<th width="150px">Coating</th>
										<th>Application</th>
									</tr>
								</thead>
								
HTML;
						
							$group_table .= <<<HTML
								<tr>
									<td>{$product_image}</td>
									<td>{$product_number}</td>
									<td>{$diamter}</td>
									<td>{$description}</td>
									<td>{$inner}</td>
									<td>{$coating}</td>
									<td>{$application}</td>
								</tr>									
HTML;


						}
					
					} elseif ( have_rows('tool_kit_table') ) {
 						$group_table = '';
 						$table_head = '';
 					
						while( have_rows('tool_kit_table') ) { 
							the_row();
					
							$product_number = get_sub_field('product_number');
							$diameter = get_sub_field('diameter');
							$shape = get_sub_field('shape');
							$coating = get_sub_field('coating');
							$extract_thumb = get_sub_field('product_thumbnail');
							if( !empty($extract_thumb) ) {

								// vars
								$thumb_url = $extract_thumb['url'];
								$thumb_alt = $extract_thumb['alt'];
							}
							$product_image = <<<HTML
								<ul class="enlarge">
                        		<li>
								<img alt="{$thumb_alt}" src="{$thumb_url}" class="thumb">
								<span> <!--span contains the popup image-->
								<img alt="{$thumb_alt}" src="{$thumb_url}">
								<br />Part No. {$product_number}
								</span>
								</li>
								</ul>
HTML;
							
							$table_head = <<<HTML
								<thead>
									<tr>
										<th width="50">Photo</th>
										<th>Part Number</th>
										<th>Shape</th>
										<th>Diameter</th>
										<th width="120px">Coating</th>
									</tr>
								</thead>
								
HTML;
						
							$group_table .= <<<HTML
								<tr>
									<td>{$product_image}</td>
									<td>{$product_number}</td>
									<td>{$shape}</td>
									<td>{$diameter}</td>
									<td>{$coating}</td>
								</tr>									
HTML;


						}
					} elseif ( have_rows('rotary_table') ) {
 						$group_table = '';
 						$table_head = '';
 					
						while( have_rows('rotary_table') ) { 
							the_row();
					
							$product_number = get_sub_field('product_number');
							$outer = get_sub_field('outer_diameter');
							$inner = get_sub_field('inner_diameter');
							$coating = get_sub_field('coating');
							$application = get_sub_field('application');
							$extract_thumb = get_sub_field('product_thumbnail');
							if( !empty($extract_thumb) ) {

								// vars
								$thumb_url = $extract_thumb['url'];
								$thumb_alt = $extract_thumb['alt'];
							}
							$product_image = <<<HTML
								<ul class="enlarge">
                        		<li>
								<img alt="{$thumb_alt}" src="{$thumb_url}" class="thumb">
								<span> <!--span contains the popup image-->
								<img alt="{$thumb_alt}" src="{$thumb_url}">
								<br />Part No. {$product_number}
								</span>
								</li>
								</ul>
HTML;
							
							$table_head = <<<HTML
								<thead>
									<tr>
										<th width="50">Photo</th>
										<th>Part Number</th>
										<th>O.D.</th>
										<th>I.D.</th>
										<th width="120px">Coating</th>
										<th>Application</th>
									</tr>
								</thead>
								
HTML;
							
							
							$group_table .= <<<HTML
								<tr>
									<td>{$product_image}</td>
									<td>{$product_number}</td>
									<td>{$outer}</td>
									<td>{$inner}</td>
									<td>{$coating}</td>
									<td>{$application}</td>
								</tr>									
HTML;

						}
					} elseif ( have_rows('adapter_table') ) {
 						$group_table = '';
 						$table_head = '';
 					
						while( have_rows('adapter_table') ) { 
							the_row();
					
							$product_number = get_sub_field('product_number');
							$diameter = get_sub_field('diameter');
							$shaft_length = get_sub_field('shaft_length');
							$length = get_sub_field('length');
							$extract_thumb = get_sub_field('product_thumbnail');
							if( !empty($extract_thumb) ) {

								// vars
								$thumb_url = $extract_thumb['url'];
								$thumb_alt = $extract_thumb['alt'];
							}
							$product_image = <<<HTML
								<ul class="enlarge">
                        		<li>
								<img alt="{$thumb_alt}" src="{$thumb_url}" class="thumb">
								<span> <!--span contains the popup image-->
								<img alt="{$thumb_alt}" src="{$thumb_url}">
								<br />Part No. {$product_number}
								</span>
								</li>
								</ul>
HTML;
							
							$table_head = <<<HTML
								<thead>
									<tr>
										<th width="50">Photo</th>
										<th>Part Number</th>
										<th>Diameter</th>
										<th>Shaft Length</th>
										<th>Length</th>
									</tr>
								</thead>
								
HTML;
							
							
							$group_table .= <<<HTML
								<tr>
									<td>{$product_image}</td>
									<td>{$product_number}</td>
									<td>{$diameter}</td>
									<td>{$shaft_length}</td>
									<td>{$length}</td>
								</tr>									
HTML;

						}
					} elseif ( have_rows('shaft_tools_table') ) {
 						$group_table = '';
 						$table_head = '';
 					
						while( have_rows('shaft_tools_table') ) { 
							the_row();
					
							$product_number = get_sub_field('product_number');
							$diameter = get_sub_field('diameter');
							$shape = get_sub_field('shape');
							$coating = get_sub_field('coating');
							$extract_thumb = get_sub_field('product_thumbnail');
							if( !empty($extract_thumb) ) {

								// vars
								$thumb_url = $extract_thumb['url'];
								$thumb_alt = $extract_thumb['alt'];
							}
							$product_image = <<<HTML
								<ul class="enlarge">
                        		<li>
								<img alt="{$thumb_alt}" src="{$thumb_url}" class="thumb">
								<span> <!--span contains the popup image-->
								<img alt="{$thumb_alt}" src="{$thumb_url}">
								<br />Part No. {$product_number}
								</span>
								</li>
								</ul>
HTML;
							
							$table_head = <<<HTML
								<thead>
									<tr>
										<th width="50">Photo</th>
										<th>Part Number</th>
										<th>Shape</th>
										<th>Diameter</th>
										<th width="120px">Coating</th>
									</tr>
								</thead>
								
HTML;
							
							
							$group_table .= <<<HTML
								<tr>
									<td>{$product_image}</td>
									<td>{$product_number}</td>
									<td>{$shape}</td>
									<td>{$diameter}</td>
									<td>{$coating}</td>
								</tr>									
HTML;

						}
					}
					
					
					$product_group .= <<<HTML
						<section class="products">
							<div class="row">
								<div class="columns small-12">
									<h2>{$product_group_name}</h2>
									<ul>{$group_bullets}</ul>
								</div>
							</div>
							<div class="row">
								<div class="columns small-12 medium-4">
									{$product_group_photo}
								</div>
								<div class="column small-12 medium-8">
									<div class="table-scroll">
										<table>
											{$table_head}
											<tbody>
												{$group_table}
											</tbody>
										</table>
										<p class="small-p">{$disclaimer}</p>
									</div>
								</div>
							</div>						
						</section>
						
HTML;
	
				}
			}
			
			
			/**
			 * The following does all of the actual drawing
			 */
			echo <<<HTML
			<section class="header {$hero_image}">
				<div class="row top-content">
					<div class="column medium-12">
						<h1>{$post_title}</h1>
						{$header_description}
					</div>
				</div>
			</section>
			{$product_group}
			<section class="grey-bg white-text">
				<div class="row">
					<div class="columns small-12 text-center">
						<h2>Products Downloads</h2>
					</div>
				</div>
				<div class="row" style="padding-top: 2rem;">
					<div class="columns small-12 medium-4 text-center">
						<a href="/wp-content/uploads/2017/05/Rubberhog_Passenger_Light_Truck_Commerical_Tire_Repair_Brochure.pdf" target="_blank"><img src="/wp-content/uploads/2017/05/rubberhog-catalogue-sidenav-light-truck.png" /><br/>
						<h5 class="yellow-text">Passenger/Light Truck & Commercial Tire Repair Brochure</h5></a>
					</div>
					<div class="columns small-12 medium-4 text-center">
						<a href="/wp-content/uploads/2017/04/Oliver_Brochure.pdf" target="_blank"><img src="/wp-content/uploads/2017/05/rubberhog-catalogue-sidenav-retread.png" /><br/>
						<h5 class="yellow-text">Retreading & Major Tire Repair Brochure</h5></a>
					</div>
					<div class="columns small-12 medium-4 text-center">
						<a href="/wp-content/uploads/2017/04/Extreme_Flared_Contour_Wheels.pdf" target="_blank"><img src="/wp-content/uploads/2017/05/rubberhog-catalogue-sidenav-contour-wheel.png" /><br/>
						<h5 class="yellow-text">Extreme Contour Wheel Bulletin</h5></a>
					</div>
				</div>
			</section>
			<section class="yellow-bg white-text">
				<div class="row">
					<div class="columns small-12 text-center">
						<h2 class="grey-text">Apply to Become a Distributor or Locate One!</h2>
					</div>
				</div>
				<div class="row">
					<div class="columns small-12 medium-6 text-center">
						<i class="fa fa-truck fa-5x" aria-hidden="true"></i><br/>
						<a href="/rh-tire-repair/become-distributor/"><button class="cta black"><span>Start Selling</span><i class="fa fa-long-arrow-right" aria-hidden="true"></i></button></a>
					</div>
					<div class="columns small-12 medium-6 text-center">
						<i class="fa fa-globe fa-5x" aria-hidden="true"></i><br/>
						<a href="/rh-tire-repair/locate-distributor/"><button class="cta black"><span>Find a Distributor</span><i class="fa fa-long-arrow-right" aria-hidden="true"></i></button></a>
					</div>
				</div>
			</section>
HTML;
		}
	}
}