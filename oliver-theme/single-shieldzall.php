<?php
use Com\Hunchfree\Wp\Themes\Hffoundation as Hffoundation;

/**
 * Default template for drawing content when no other template is available.
 *
 * There are 3 distinct output types in this file:
 * 1) When a 404 type request is made
 * 2) When a singular post type is requested
 * 3) When anything else is requested (displaying lists of posts)
 *
 * This can be completely overridden by copying it to your child theme and editing it there.
 *
 * @see https://developer.wordpress.org/themes/basics/template-hierarchy/ Wordpress Template Hierarchy
 */

# Load the header.php file, which takes care of drawing the html header and visible header
# - note: header.php should load visible_header.php
get_header();

# Take care of drawing out everything between the header and the footer.
# - In case of an exception, draw it out.
# - If there are warnings and the user is a logged in admin, draw them
try {

	$renderer = Hffoundation\Theme_Front_End::get_instance();
	if ( !is_object( $renderer ) ) {
		$this->_notices[] = "Failed to get renderer instance";
		throw new \Exception("Failed to get renderer instance", 10001);
	}

	# Set up our placable sidebars, if they are used
	$block_sb_side_one = '';
	$block_sb_side_two = '';
	$extra_main_wrapper_css = '';
	$shell_content_extra_css = '';
	$placeable_sidebar_use = (int)$renderer->get_best_mod('placeable_sidebars_use', 0);
	if ( 1 === $placeable_sidebar_use ) {

		# Placeable Sidebar Area - Side One : left of any content area
		$widgets = $renderer->get_sidebar_widgets("sb_content_side_one", "sb_content_side_one_css", 'small-12' );
		if ( !empty( $widgets ) ) {
			$sidebar_id = $renderer->get_last_used_sidebar_id();
			$block_sb_side_one = <<<HTML
<div id="sidebar-side-one" class="columns small-12 medium-4 large-3 sidebar-wrapper">
	<div class="row sidebar widget-wrapper sidebar-{$sidebar_id}">
		{$widgets}
	</div>
</div>

HTML;

		}

		# Placeable Sidebar Area - Side Two : Right of any content area
		$widgets = $renderer->get_sidebar_widgets("sb_content_side_two", "sb_content_side_two_css", 'small-12' );
		if ( !empty( $widgets ) ) {
			$sidebar_id = $renderer->get_last_used_sidebar_id();
			$block_sb_side_two = <<<HTML
<div id="sidebar-side-two" class="columns small-12 medium-4 large-3 sidebar-wrapper">
	<div class="row sidebar widget-wrapper sidebar-{$sidebar_id}">
		{$widgets}
	</div>
</div>

HTML;

		}

		# Based on known content areas, set up any extra content wrapper styles
		# the left and right sidebars use : small-12 medium-4 large-3
		if ( !empty( $block_sb_side_one ) ) {
			if ( !empty( $block_sb_side_two ) ) {
				$extra_main_wrapper_css = 'medium-4 large-6';
				$shell_content_extra_css = 'two-sidebars';
			} else {
				$extra_main_wrapper_css = 'medium-8 large-9';
				$shell_content_extra_css = 'one-sidebar';
			}
		} else if ( !empty( $block_sb_side_two ) ) {
			$extra_main_wrapper_css = 'medium-8 large-9';
			$shell_content_extra_css = 'one-sidebar';
		}
	}

	if ( is_404() ) {
		theme_draw_four_zero_four();
	} else {

		if ( is_singular() ) {
			theme_draw_rh_roll_grinding(
				$renderer,
				$block_sb_side_one, $block_sb_side_two,
				$extra_main_wrapper_css, $shell_content_extra_css
			);
		} 
	}
} catch ( \Exception $e ) {
	if ( WP_DEBUG || ( is_user_logged_in() && current_user_can('activate-plugins') ) ) {
		echo "<p>Exception Encountered:</p><pre>" . print_r($e, true) . "</pre>";
		if ( isset( $o_renderer ) && is_object( $o_renderer ) ) {
			$notices = $o_renderer->get_warnings();
			if ( 0 < count($notices) ) {
				echo '<div><h4>Notices:</h4><pre>' . print_r($notices, true) . '</pre></div>';
			}
		}
	}
}

get_footer();

/**
 * Draw out the contents of a 404 page
 */
function theme_draw_four_zero_four() {

	$title_bit = '<h1>404 Not Found</h1>';



	echo <<<HTML
<div id="shell-content" class="row-expanded shell-content-singular">
	<div class="row">
		<div class="columns small-12">
			<article class="row article-wrap">
				<header class="columns page-title">
					<h1>{$title_bit}</h1>
				</header>
				<div class="columns content">
					<p>The content you were looking for was not found.</p>
				</div>
			</article>
		</div>
	</div>
</div>

HTML;

}

/**
 * Draw out the contents of a singular post type (posts, pages, custom post types)
 *
 * @param Hffoundation\Theme_Front_End $renderer	For utilities related to drawing out content
 * @param string $block_sb_side_one Content of any left sidebar
 * @param string $block_sb_side_two Content of any right sidebar
 * @param string $extra_main_wrapper_css Any css classes to stuff into the outer wrapper for sidebar placement
 * @param string $shell_content_extra_css Any css classes to stuff into the article for sidebar placement
 *
 * @throws Exception when a serious issue occurs
 *
 * @todo load theme test data and go through to ensure everything looks good by default
 * @todo paged_navigation using <!--nextpage--> tag
 * @todo for singular attachments, insert the attachment into the post content
 * @todo style comments
 * @todo let pingbacks and trackbacks show up even if comments are disabled
 */
function theme_draw_rh_roll_grinding(
	Hffoundation\Theme_Front_End &$renderer,
	$block_sb_side_one = '', $block_sb_side_two = '',
	$extra_main_wrapper_css = '', $shell_content_extra_css = ''
) {

	if ( !have_posts() ) {
		$this->_notices[] = "Attempting to draw singular without any posts!";
		throw new \Exception("Non 404, singular, without any posts!", 10001);

	} else {
		while ( have_posts() ) {
			# load the current post into the global space for easy access
			the_post();

			# grab access to the currently loaded post
			global $post;

			# get the post id, css classes for it, and post type
			$post_id = get_the_ID();
			$css_classes = implode(' ', get_post_class());
			$post_type = get_post_type( $post );

			# We are drawing the entire post.


			###
			### The Content Block
			###

			$block_content = '';

			# post_content - a component block_content
			$post_content = '';
			if ( post_type_supports( $post_type, 'editor' ) ) {

				$post_content = get_the_content( 'Read More...' );
				$post_content = apply_filters( 'the_content', $post_content );
				$post_content = str_replace( ']]>', ']]&gt;', $post_content );

			}

			

			# If any component of block_content is not empty, set up the block_content area
			# - by default this draws the featured image before the content.
			# - if you want the feature image floated, add the float to the featured_image div.
			if ( !empty( $post_content ) || !empty( $featured_image ) ) {
				$block_content = <<<HTML
{$post_content}
HTML;

			}

			###
			### Sidebars are handled near the top of this file.
			###

			/**
			 * The following does all of the actual drawing
			 */
			echo <<<HTML
{$block_content}


HTML;

		}
	}
}

