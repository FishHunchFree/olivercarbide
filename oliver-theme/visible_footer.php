<?php
use Com\Hunchfree\Wp\Themes\Hffoundation as Hffoundation;

defined('\\ABSPATH') or die('Permission denied - header.php');

$year = date('Y');

# Draw the main navigation menu in the footer
# Small readers without javascript will jump here when clicking the menu icon
# Hidden on larger screens that should support the normal nav menu
locate_template('walkers/menus/main-nav-menu-walker.php', true);

$menu_args = array(
	'container' => '',
	'menu_class' => 'vertical menu',
	'items_wrap' => '%3$s',
	'depth' => 2,
	'theme_location' => 'main_header',
	'echo' => 0,
	'fallback_cb' => false
);
if ( class_exists( '\\Com\\Hunchfree\\Wp\\Themes\\Hffoundation\\Main_Nav_Menu_Walker') ) {
	$menu_args['walker'] = new Hffoundation\Main_Nav_Menu_Walker();
}

$menu_args = apply_filters( 'pwf_adjust_theme_main_nav_arguments', $menu_args );

$top_nav = wp_nav_menu( $menu_args );
if ( empty( $top_nav ) ) {
	$top_nav = <<<HTML
	<li>
		<a href="#">Set</a>
		<ul class="vertical menu">
			<li>
				<a href="#">Sub Item One</a>
				<ul class="vertical menu">
					<li><a href="#">Sub Sub One</a></li>
					<li><a href="#">Sub Sub One</a></li>
					<li><a href="#">Sub Sub One</a></li>
				</ul>
			</li>
		</ul>
	</li>
	<li><a href="#">Up</a></li>
	<li><a href="#">Main</a></li>
	<li><a href="#">Nav</a></li>

HTML;

}

/** @var Hffoundation\Theme_Controller_Front_End $Renderer */
$Renderer = Hffoundation\Theme_Front_End::get_instance();

# Admin visible notices and exceptions should be drawn at the very bottom of the footer
$admin_notices = '';
if ( is_user_logged_in() && current_user_can('switch_themes') ) {
	$exceptions = $Renderer->get_exceptions();
	if ( 0 < count($exceptions) ) {
		$notice_list = '';
		foreach ( $exceptions as $e ) {
			$notice_list .= "\r\n" . '<div class="columns"><pre>' . print_r($e, true) . '</pre></div>';
		}
	} else {
		$notice_list = '<div class="columns">No Exceptions</div>';
	}
	$warnings = $Renderer->get_warnings();
	if ( 0 < count($warnings) ) {
		$warning_list = "\r\n" . '<div class="columns"><h6>Warnings:</h6><pre>' . print_r($warnings, true) . '</pre></div>';
	} else {
		$warning_list = '<div class="columns">No Warnings</div>';
	}
	$admin_notices = <<<HTML
<div class="row-expanded exceptions" style="background-color: #EFEFEF; padding: 1em 0;">
	<div class="row">
		<div class="columns">
			<p><b>Admin Notices:</b> Only logged in administrators see this content.</p>
		</div>
		{$notice_list}
		{$warning_list}
	</div>
</div>
HTML;

}

/**
 * Add your footer into the below footer section as wanted.
 *
 * It should be contained in one or more rows drawn after the row that contains the {$top_nav} variable.
 */

echo <<<HTML
<section class="footer">
		<div class="footer-top">
	 		<div class="row">
				<div class="row medium-up-5" style="padding: 0 20px;">
					<div class="column">
						<h5>Oliver</h5>
						<ul>
							<li><a href="/">About</a></li>
							<li><a href="/custom-services/">Custom Services</a></li>
							<li><a href="/contact/">Contact</a></li>
						</ul>
					</div>
					<div class="column">
						<h5>Kutzall</h5>
						<ul>
							<li><a href="/kutzall/about/">About</a></li>
							<li><a target="_blank" href="https://kutzall.com/">Shop</a></li>
							<li><a href="/kutzall/become-distributor/">Become a Distributor</a></li>
							<li><a target="_blank" href="https://kutzall.com/pages/contact-us">Contact</a></li>
						</ul>
					</div>
					<div class="column">
						<h5>RH Tire Repair</h5>
						<ul>
							<li><a href="/rh-tire-repair/about/">About</a></li>
							<li><a href="/rh-tire-repair/products/">Products</a></li>
							<li><a href="/rh-tire-repair/locate-distributor/">Locate a Distributor</a></li>
							<li><a href="/rh-tire-repair/become-distributor/">Become a Distributor</a></li>
							<li><a href="/rh-tire-repair/contact/">Contact</a></li>
						</ul>
					</div>
					<div class="column">
						<h5>RH Roll Grinding</h5>
						<ul>
							<li><a href="/rh-roll-grinding/about/">About</a></li>
							<li><a href="/rh-roll-grinding/coatings/">Coatings</a></li>
							<li><a href="/rh-roll-grinding/design-center/">Design Center</a></li>
							<li><a href="/rh-roll-grinding/wheel-neglect/">Wheel Neglect</a></li>
							<li><a href="/rh-roll-grinding/contact/">Contact</a></li>
						</ul>
					</div>
					<div class="column">
						<h5>Shieldzall</h5>
						<ul>
							<li><a href="/shieldzall/about/">About</a></li>
							<li><a href="/shieldzall/contact-us/">Contact</a></li>
						</ul>
					</div>
				</div>
	 		</div>
	 	</div>
	 	<div class="footer-bottom row expanded">
	 		<div class="row vertical-out">
	 			<div class="columns small-12 large-4 text-center vertical-in">
	 				<img src="/wp-content/uploads/2017/03/Made-in-USA.png" alt="Made in the USA" width="150px">
	 			</div>
	 			<div class="columns small-12 large-4 text-center vertical-in">
	 				<p>©{$year} Oliver Carbide Products</p>
	 			</div>
	 			<div class="columns small-12 large-4 text-center vertical-in" style="padding: 1.5rem 0 1rem 0;">
	 				<a href="https://hunchfree.com/" target="_blank"><img src="/wp-content/uploads/2017/03/Powered-by-Hunch-Free.png" alt="Powered by Hunch Free"></a>
	 			</div>
	 		</div>
	 	</div>
	</section>
    <script type='text/javascript'>
	(function (d, t) {
	var bh = d.createElement(t), s = d.getElementsByTagName(t)[0];
	bh.type = 'text/javascript';
	bh.src = 'https://www.bugherd.com/sidebarv2.js?apikey=g9xxriaxuhosm35ydne3aa';
	s.parentNode.insertBefore(bh, s);
	})(document, 'script');
	</script>
	<script>
	jQuery(document).ready(function($) {   
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        slidesPerView: 1,
        paginationClickable: true,
        spaceBetween: 30,
        loop: true,
        autoplay: 3000,
        autoplayDisableOnInteraction: false   
    });
    }
    </script>
  
{$admin_notices}
HTML;

/*<footer id="shell_foot">
	<div class="row-expanded altnav" id="alt-top-nav">
		<div class="row">
			<div class="columns">
				<ul class="vertical menu">
				{$top_nav}
				</ul>
			</div>
		</div>
	</div>
	{$admin_notices}
</footer>*/